---
title: Reference Documentation
prevSection: mechanical
nextSection: appendix-a
---

# {{ page.title }}

[1] Xilinx, Inc., "Zynq UltraScale+ MPSoC," 2017. [Online]. Available:
[https://www.xilinx.com/products/silicon-devices/soc/zynq-ultrascale-mpsoc.html](https://www.xilinx.com/products/silicon-devices/soc/zynq-ultrascale-mpsoc.html). [Accessed 19 April
2017].
{: id="ref1" }

[2] Analog Devices, Inc., "AD9361 Datasheet and Product Info," 2017. [Online]. Available:
[http://www.analog.com/en/products/rf-microwave/integrated-transceivers-transmitters-
receivers/wideband-transceivers-ic/ad9361.html#product-overview](http://www.analog.com/en/products/rf-microwave/integrated-transceivers-transmitters-
receivers/wideband-transceivers-ic/ad9361.html#product-overview). [Accessed 19 April 2017].
{: id="ref2" }