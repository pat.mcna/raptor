---
title: Appendix B - Assembly Drawings
prevSection: appendix-a
nextSection: appendix-c
sectionNumber: B
---

# {{ page.title }}
{: .noHeadingNumber }

![Raptor Assembly Drawing - Front](assets/imgs/raptor-assembly-front-drawing.png)
{: .text-center data-caption='Raptor Assembly Drawing - Front' }

![Raptor Assembly Drawing - Back](assets/imgs/raptor-assembly-back-drawing.png)
{: .text-center data-caption='Raptor Assembly Drawing - Back' }
