---
title: Appendix C - Board Revision
prevSection: appendix-b
<!-- nextSection: appendix-d -->
sectionNumber: C
---

# {{ page.title }}
{: .noHeadingNumber }

## Revision

| Revision                     | Date   | Description              |
| ---------------------------- | ------     | ----------              |
| Rev A                        | 1/2017     |   Engineering Prototype |
| Rev B                        | 7/2017     |   Initial Production Boards                     |
