---
title: Appendix A - Statement of Volatility
prevSection: mechanical
nextSection: appendix-b
sectionNumber: A
---

# {{ page.title }}
{: .noHeadingNumber }

## Types of Memory
{: id="types-of-memory" }

The Raptor contains the following volatile and non-volatile memory:

|Memory             | Type         | Capacity                                | Function                                | User-Accessible Storage |
|------------------ | ------------ | --------------------------------------- | --------------------------------------- | ----------------------- |
|MicroSD Flash Card | Non-volatile | Varies, typically 8 GBytes\\(1 device)  | File system for embedded microprocessor | Yes                     |
|QSPI Flash         | Non-volatile |   125 MBytes                             |                                         |                         |
|MPSoC Memory         | Volatile     | 4 GBytes DDR4 attached to processing system |                                         |                         |
|MPSoC OCM RAM        | Volatile     | 256 KBytes                                  |                                         |                         |
|EEPROM             | Non-volatile |  32 Kbytes   | Static system configuration storage     | No                      |
{: .table .table-condensed .table-striped data-caption='Raptor Memories' }

Volatile memories do not retain their data after the system power has been
removed. Non-volatile memories do retain their data after the system power has
been removed.


## Write-Protection Controls
{: id="write-protection-controls" }

The non-volatile memories on the Raptor board can be write-protected, rendering them read-only
memories. When the memories are write-protected, no data can be stored to them. The write protect
controls are configured with the SW1 DIP switch (see Figure A.2).

![Write-Protection Controls](assets/imgs/write-protection-controls.png)
{: .text-center data-caption='Write-Protection Controls' }

The EEPROM is write-protected when position 3 of the DIP switch is ON. The
EEPROM is write- enabled when postion 3 of the DIP switch is OFF. This switch
should only be toggled when the unit is powered off.

The microSD flash card is write-protected when position 4 of the DIP switch is
ON. When position 4 of the DIP switch is OFF, the microSD FLASH card is write-
enabled.

Because the microSD flash card contains the file-system for the embedded
microprocessor, care must be taken to mount the file system as read-only
before write-protecting the flash card.

To write-protect the file-system:

1. Log-in to the system as root.
2. Issue the command: mount –o remount,ro /
3. Issue the command: shutdown
4. Power-off the system once shutdown has completed.
5. Change the DIP switch to the write-protected position.
6. Power-on the system.

To write-enable the system:

1. Power down the system.
2. Change the DIP switch to the write-enabled position.
3. Power-on the system.

A signal can also be applied to the write-protect header to control the write-
protect state of the microSD flash card remotely. Consult the factory for
details on using this signal.

To verify the microSD flash card is write-protected, issue the following mount
command to attempt to mount the microSD flash card as read-write.

<code>mount -o remount,rw /</code>

The following error message will be returned:

<code>mount: cannot remount /dev/root read-write, is write-protected</code>
