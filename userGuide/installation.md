---
title: Installation
prevSection: introduction
nextSection: electrical-subsystems
sectionNumber: 2
---

# {{ page.title }}

## Raptor Developer Kit Contents
{: id="kit-contents" }

The Raptor Developer Kit contains the following items:

* Raptor board (with heatsinks and standoffs)
* MicroSD Card
* Power Supply
* USB 3.0 Cable
* USB 2.0 Cable
* JTAG Adapter Cable for Xilinx USB 2.0
* Quick Start Guide and Limited Warranty

<!-- **Note:** The custom cable for TBD is not included. You can order the cable TBD -->

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

## ESD Precautions
{: id="esd-precautions" }

The Raptor SDR is an electrostatic-sensitive device. Before you install the
Raptor board, observe precautions for handling electrostatic-sensitive devices
to avoid electrostatic discharge (ESD) damage to the equipment. ESD is a
discharge of stored static electricity that can damage equipment and impair
electrical circuitry. It occurs when electronic components are improperly
handled and can result in complete or intermittent failures.

Before you begin making connections to the Raptor board, make sure power to
the Raptor board is off to avoid possible permanent damage to the equipment.
{: .alert .alert-warning .esd-warning}

Take care to avoid static damage to any components of your system. To minimize
the possibility of causing electrostatic damage, observe the following:

* Be sure you are grounded before installing the Raptor board. Use a ground
  strap.
* Make sure the power source for the Raptor board is connected through a
  grounded power receptacle.


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

## Thermal Considerations
{: id="thermal-considerations" }

***WARNING:*** Once power has been applied, the Raptor SDR requires mounting
in a chassis with integrated heat management components or air to flow across
the attached heat sinks of the circuit card assembly. Failure to provide
sufficient air flow could lead to device damage or failure and void any
warranty.
{: .alert .alert-danger}



* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

## Connections
{: id="connections" }

***IMPORTANT:*** All devices must be powered off before connecting/disconnecting.

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

### Signal Connections
{: id="signal-connections" }

Connect RF input and output connectors, reference signals (i.e., 10 MHz clock,
1 PPS, IRIG-B-DC, etc.), network cable, and serial console. Not all
connections are required for system operation.

Use care when attaching cables. Do not over-tighten the SMA connectors.

Prior to connecting input sources to the Raptor SDR, verify that the sources
are set to the appropriate signal levels. Failure to do so could result in
damage to the Raptor SDR. <!-- For detailed signal requirements, please refer to
TBD. -->

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


### Power
{: id="power" }
Follow these steps to power up the Raptor SDR.

1. Connect mezzanine or other peripheral device before powering on the system.\\
   **Note:** Failure to remove power before connecting external devices can result
   in damage to both the Raptor SDR and attached devices.
2. Apply DC power (9 VDC to 16 VDC) to the Raptor SDR.
