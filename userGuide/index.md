---
title: Table of Contents
nextSection: introduction
---

--------------
![Raptor &trade; SDR](assets/imgs/raptorLogo.png)
{: .text-center }

# User Guide
--------------

Version 1.0
{{ site.pubDate | date: site.dateFormat }}
{: .text-center }

<div class='alert alert-info' markdown='1'>
## Notice of Disclaimer

The information disclosed to you hereunder (the “Materials”) is provided solely for the use of Rincon
Research Corporation products. To the maximum extent permitted by applicable law: (1) Materials are
made available "AS IS" and with all faults, Rincon hereby DISCLAIMS ALL WARRANTIES AND
CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED TO
WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR ANY
PARTICULAR PURPOSE; and (2) Rincon shall not be liable (whether in contract or tort, including
negligence, or under any other theory of liability) for any loss or damage of any kind or nature related to,
arising under, or in connection with, the Materials (including your use of the Materials), including for any
direct, indirect, special, incidental, or consequential loss or damage (including loss of data, profits,
goodwill, or any type of loss or damage suffered as a result of any action brought by a third party) even if
such damage or loss was reasonably foreseeable or Rincon had been advised of the possibility of the
same. Rincon assumes no obligation to correct any errors contained in the Materials or to notify you of
updates to the Materials or to product specifications. You may not reproduce, modify, distribute, or
publicly display the Materials without prior written consent. Certain products are subject to the terms and
conditions of Rincon’s limited warranty. Rincon products are not designed or intended to be fail-safe or for use in
any application requiring fail-safe performance; you assume sole risk and liability for use of Rincon
products in such critical applications.\\
\\
&copy; Copyright 2016–2018 Rincon Research Corporation.\\
\\
Raptor and the Raptor logo are trademarks of Rincon Research Corporation. All other trademarks are the
property of their respective owners.
</div>

## Revision History
{: .text-center }

| Version | Date | Comments |
|:-----:|:--------------|:----------------|
|  1.0  |  {{ site.pubDate | date: site.dateFormat }} | Initial Release |
|  1.1  |  {{ site.pubDate | date: site.dateFormat }} | Naming convention change. Added more detail on dimensions|
{: style="margin: 1em auto;" border="1"}

## Table of Contents

<!-- 0. Revision History -->
<div id='toc' markdown='1'>
1. [Introduction]({{ site.baseurl }}{% link introduction.md %})                 
   1. [Raptor Board]({{ site.baseurl }}{% link introduction.md %}#raptor-board)
      1. [Features]({{ site.baseurl }}{% link introduction.md %}#features)      
      2. [Specifications]({{ site.baseurl }}{% link introduction.md %}#specs)   
      3. [Block Diagram]({{ site.baseurl }}{% link introduction.md %}#block-diagram)
      4. [Device Callouts]({{ site.baseurl }}{% link introduction.md %}#device-callouts)
   2. [Expansion Board]({{site.baseurl  }}{% link introduction.md %}#expansion-board)
      1. [Expansion Block Diagram]({{site.baseurl  }}{% link introduction.md %}#block-expansion-diagram)
2. [Installation]({{ site.baseurl }}{% link installation.md %})
   1. [Raptor Developer Kit Contents]({{ site.baseurl }}{% link installation.md %}#kit-contents)
   2. [ESD Precautions]({{ site.baseurl }}{% link installation.md %}#esd-precautions)
   3. [Thermal Considerations]({{ site.baseurl }}{% link installation.md %}#thermal-considerations)
   4. [Connections]({{ site.baseurl }}{% link installation.md %}#connections)   
      1. [Signal Connections]({{ site.baseurl }}{% link installation.md %}#signal-connections)
      2. [Power]({{ site.baseurl }}{% link installation.md %}#power)
3. [Electrical Subsystems]({{ site.baseurl }}{% link electrical-subsystems.md %})
   1. [Xilinx Zynq Ultrascale+ MPSOC]({{ site.baseurl }}{% link electrical-subsystems.md %}#xilinx)
   2. [Analog Devices AD9361 Transceiver]({{ site.baseurl }}{% link electrical-subsystems.md %}#analog-devices)
   3. [Analog Filters]({{ site.baseurl }}{% link electrical-subsystems.md %}#analog-filters)
      1. [Transmit Path]({{ site.baseurl }}{% link electrical-subsystems.md %}#transmit-path)
         1. [TX1]({{ site.baseurl }}{% link electrical-subsystems.md %}#transmit-path)
         2. [TX2]({{ site.baseurl }}{% link electrical-subsystems.md %}#transmit-path)
      2. [Receive Path]({{ site.baseurl }}{% link electrical-subsystems.md %}#receive-path)
         1. [RX1]({{ site.baseurl }}{% link electrical-subsystems.md %}#receive-path)
         2. [RX2]({{ site.baseurl }}{% link electrical-subsystems.md %}#receive-path)
   4. [Memory]({{ site.baseurl }}{% link electrical-subsystems.md %}#memory)
      1. [DDR4]({{ site.baseurl }}{% link electrical-subsystems.md %}#ddr4)
      2. [On-chip Memory]({{ site.baseurl }}{% link electrical-subsystems.md %}#on-chip-memory)
      3. [Quad SPI Flash]({{ site.baseurl }}{% link electrical-subsystems.md %}#quad-spi-flash)
      4. [EEPROM]({{ site.baseurl }}{% link electrical-subsystems.md %}#eeprom)
      5. [Micro SD]({{ site.baseurl }}{% link electrical-subsystems.md %}#micro-sd)
         1. [Interface]({{ site.baseurl }}{% link electrical-subsystems.md %}#micro-sd-interface)
         2. [Installing and removing the microSD Card]({{ site.baseurl }}{% link electrical-subsystems.md %}#micro-sd-install)
   5. [USB 2.0/3.0 Host Device]({{ site.baseurl }}{% link electrical-subsystems.md %}#usb-host-device)
      1. [USB 2.0/3.0 Host Device Port 0]({{ site.baseurl }}{% link electrical-subsystems.md %}#usb-host-device-port-0)
      2. [USB 2.0/3.0 Host Device Port 1]({{ site.baseurl }}{% link electrical-subsystems.md %}#usb-host-device-port-1)
   6. [I/O]({{ site.baseurl }}{% link electrical-subsystems.md %}#user-io)
      1. [LEDs]({{ site.baseurl }}{% link electrical-subsystems.md %}#user-leds)
   7. [Expansion Header]({{ site.baseurl }}{% link electrical-subsystems.md %}#expansion-header)
   8. [Reset Sources]({{ site.baseurl }}{% link electrical-subsystems.md %}#power-interface-reset)
      1. [ZynqMP Power‐on Reset]({{ site.baseurl }}{% link electrical-subsystems.md %}#power-interface-reset-zynqmp)
      2. [AD9361 Reset]({{ site.baseurl }}{% link electrical-subsystems.md %}#ad9361-reset)
   9. [RF Interfaces]({{ site.baseurl }}{% link electrical-subsystems.md %}#rf-interfaces)
4. [Software]({{ site.baseurl }}{% link software.md %})
   1. [Prerequisites]({{ site.baseurl }}{% link software.md %}#prerequisites)
   2. [Getting Started]({{ site.baseurl }}{% link software.md %}#getting-started)
      1. [Preparing Booting Media]({{ site.baseurl }}{% link software.md %}#preparing-boot-media)
         1. [INITRAMFS Boot (Default RootFS for PetaLinux)]({{ site.baseurl }}{% link software.md %}#initramfs)
         2. [SD Card Extended File System Boot]({{ site.baseurl }}{% link software.md %}#sd-card-boot)
      2. [Installing Raptor BSP]({{ site.baseurl }}{% link software.md %}#installing-raptor-bsp)
      3. [Arch Linux Distribution]({{ site.baseurl }}{% link software.md %}#arch-linux-distribution)
      4. [Raptor Bring-up]({{ site.baseurl }}{% link software.md %}#raptor-bring-up)
   3. [Operating System Options]({{ site.baseurl }}{% link software.md %}#os-options)
   4. [Libiio and IIO Daemon]({{ site.baseurl }}{% link software.md %}#libiio-and-iio-daemon)
   5. [Source Code and Reference Designs]({{ site.baseurl }}{% link software.md %}#source-code)
      1. [Analog Devices AD9361 RF Transceiver]({{ site.baseurl }}{% link software.md %}#ad-and-rf-transceiver)
5. [Performance]({{ site.baseurl }}{% link performance.md %})
   1. [RF Performance Measurements]({{ site.baseurl }}{% link performance.md %}#rf-measurements)
   2. [Environmental]({{ site.baseurl }}{% link performance.md %}#environmental)
   3. [Error Vector Magnitude]({{ site.baseurl }}{% link performance.md %}#error-vector-magnitude)
6. [Mechanical]({{ site.baseurl }}{% link mechanical.md %})
   1. [Mounting]({{ site.baseurl }}{% link mechanical.md %}#mounting)
7. [Reference Documentation]({{ site.baseurl }}{% link reference.md %})
{: .sections }

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

1. [Appendix A - Statement of Volatility]({{ site.baseurl }}{% link appendix-a.md %})
   1. [Types of Memory]({{ site.baseurl }}{% link appendix-a.md %}#types-of-memory)
   2. [Write-Protection Controls]({{ site.baseurl }}{% link appendix-a.md %}#write-protection-controls)
2. [Appendix B - Assembly Drawings]({{ site.baseurl }}{% link appendix-b.md %})
3. [Appendix C - Board Revision]({{ site.baseurl }}{% link appendix-c.md %})
<!-- 4. [Appendix D - TBD]({{ site.baseurl }}{% link appendix-d.md %}) -->
{: .appendices }


</div>
