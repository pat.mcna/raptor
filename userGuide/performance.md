---
title: Performance
prevSection: software
nextSection: mechanical
sectionNumber: 5
---

# {{ page.title }}

## RF Performance Measurements
{: id="rf-measurements" }

Following are the results of the Raptor board frequency response:

![Receive Path Port A](assets/imgs/rx_porta_freq_response.png)
{: .text-center data-caption='Receive Path Port A' }

![Receive Path Port B](assets/imgs/rx_portb_freq_response.png)
{: .text-center data-caption='Receive Path Port B' }

![Receive Path Port C](assets/imgs/rx_portc_freq_response.png)
{: .text-center data-caption='Receive Path Port C' }
