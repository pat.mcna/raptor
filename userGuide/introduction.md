---
title: Introduction
prevSection: index
nextSection: installation
sectionNumber: 1
---

# {{ page.title }}

Raptor&trade; SDR Development Kit combines state-of-the-art capabilities with a
flexible design, resulting in a compact, efficient solution for multiple mission
requirements.

![Raptor Front View](assets/imgs/raptor-front-view.png)
{: .text-center data-caption='Raptor Base Board Front View' }
## Raptor Board
###  Features
{: id="features" }

* Zynq UltraScale+ XCZU9EG-1FFVC900E Devices
* Analog Devices AD9361 2x2 MIMO RF transceiver (70 MHz to 6 GHz)
* Wideband LNA and selectable filter bank per receiver
* Bypassable 2.4 GHz 29 dBm power amplifier per transmitter
* External timing inputs (i.e., 10MHz, 1-PPS) supported
* 4GB 64-bit DDR4-2400 attached to Processor Subsystem (PS)
* MicroSD memory card interface
* 128MB QSPI flash memory (dual parallel)
* Dual USB 3.0 interfaces with USB 2.0 OTG compatibility
* USB 2.0 UART (i.e., console) interface
* Expansion connector for optional I/O mezzanine
* Flexible 9V to 16V supply input
* Compact form factor (5 x 2.675 in)
* Compatible with ADI (FMCOMMS) drivers and reference designs
* BSP, drivers, and COTS tool support included with purchase


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

###  Specifications
{: id="specs" }

#### Specifications
{: .text-center .noHeadingNumber .incrementFigureCounter }

<div class='productInfo'>
   <div class='container' id='techSpecs'>
   {% for category in site.data.specs %}
      <h4 class='noHeadingNumber'>{{ category.categoryName }}</h4>
      {% for item in category.items %}
      <div class='specRow'>
         <div class='specHeading'>{{ item.name }}</div>
         <div class='specValue'>{{ item.value }}</div>
      </div>     
      {% endfor %}   
   {% endfor %}
   </div>

</div>
<div class='alert alert-info'>
   For a more comprehensive list of specifications, refer to the <a href="https://www.rincon.com/shop/board-level/raptor/"> Raptor web page</a>.
</div>

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

###  Block Diagram
{: id="block-diagram" }

![Raptor Board Block Diagram](assets/imgs/raptor-sdr-block-diagram.png)
{: .text-center data-caption='Raptor Base Board Block Diagram' }

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

###  Device Callouts
{: id="device-callouts" }

![Annotated Board—Front](assets/imgs/annotated-board-front.png)
{: .text-center data-caption='Annotated Board—Front' }

![Annotated Board—Back](assets/imgs/annotated-board-back.png)
{: .text-center data-caption='Annotated Board—Back' }

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
## Expansion Board
{: id="expansion-board" }

The Raptor Expansion board adds more functionality to the main Raptor board. This includes M.2 SATA , DisplayPort, 1G Ethernet, Firefly, Dual SFP+, IRIG B, I2C, SPI, UART, 16x GPIO and an 8-channel configurable ADC/DAC/GPIO.

![Raptor Expansion Front View](assets/imgs/RaptorExpansion.png)
{: .text-center data-caption='Raptor Expansion Board Front View' }

![Raptor Expansion Rear View](assets/imgs/RaptorExpansionrear.png)
{: .text-center data-caption='Raptor Expansion Board Rear View' }

![Raptor with Expansion View](assets/imgs/RaptorwithExpansion.png)
{: .text-center data-caption='Raptor with Expansion View' }

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

### Block Diagram
{: id="block-expansion-diagram" }

![Expansion Board Block Diagram](assets/imgs/expansion-board-block-diagram.png)

{: .text-center data-caption='Expansion Board Block Diagram' }

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
