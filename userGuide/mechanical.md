---
title: Mechanical
prevSection: performance
nextSection: appendix-a
sectionNumber: 6
---

# {{ page.title }}

## Mounting
{: id="mounting" }


Following are the dimensions of the Raptor board:

![Overall Board Dimensions](assets/imgs/RAPTOR_Overall_Board_Dimensions.png)
{: .text-center data-caption='Overall Board Dimensions' }

![Connector Locations](assets/imgs/RAPTOR_Connector_Locations_in_inches.png)
{: .text-center data-caption='Connector Locations' }

![Rear Connection Locations](assets/imgs/RAPTOR_Rear_Connector_Locations_in_inches.png)
{: .text-center data-caption='Rear Connection Locations' }
