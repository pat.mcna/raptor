Raptor is the next-generation software-defined radio platform from Rincon
Research. Raptor combines state-of-the-art capabilities with a flexible
design, resulting in a compact, efficient solution for multiple mission
requirements.

* [Product Information](http://www.rincon.com/products/board-level/raptor/)
* [User Guide](http://raptor.rincon.com/)
* [Issue Tracker](https://gitlab.com/rinconresearch/raptor/issues)

## Prerequisites


The Raptor SDR includes a PetaLinux board support package (BSP) that can be
used for immediate development. The BSP includes pre-built images that can be
transferred to the SD card to boot the board.

To prepare the environment on a host computer:

1. Install PetaLinux Tools. Please refer to the PetaLinux Tools Installation
   Steps from the Xilinx website.
2. Download the Raptor Project:

  <code>git clone https://gitlab.com/rinconresearch/raptor.git<code>

3. Set up the PetaLinux Working Environment. Please refer to the PetaLinux
   documentation for the steps to set up the environment.  Use the Petalinux version that corresponds with the bsp version in the Raptor project to be used.





## Getting Started

### Preparing Booting Media


Choose one of the options to prepare the booting media:

* Single partition (see 4.2.1.1 INITRAMFS Boot (Default RootFS for PetaLinux)
  Option)
* Two partitions (see 4.2.1.2 SD Card Extended File System Boot Option)

#### INITRAMFS Boot (Default RootFS for PetaLinux)


The INITRAMFS boot, which is the default booting option, only requires one
partition on the SD card.

To prepare the SD card and create a FAT32 partition:


<ol>
   <li>
      Insert the SD card into an SD card reader in the host computer, and run the
      lsblk command to identify the device.<br />Following is the output of the lsblk
      command, where sdb is the SD card:<br />
   <pre>
   NAME                     MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
   sda                        8:0    0 931.5G  0 disk
   ├─sda1                     8:1    0   501M  0 part /boot
   ├─sda2                     8:2    0   200M  0 part /boot/efi
   └─sda3                     8:3    0 930.8G  0 part  
   sdb                        8:16   1   7.4G  0 disk
   └─sdb1                     8:17   1   7.4G  0 part /run/media/jgm/3632-6239
   </pre>
      <div class='alert alert-info'>
         There are many tools to format and partition media. These instructions use the
         parted tool to perform the task.
      </div>
   </li>
   <li>
      Exchange **sdb** with the name of your block device in the following command:<br />
      <code>sudo parted /dev/sdb</code>
   </li>
   <li>
      At this point the parted utility is running. Create a partition table by entering the
      following command:<br />
      <code>(parted) mklabel msdos</code>
   </li>
   <li>
      Create one partition on the SD card:<br />
      <code>(parted) mkpart primary fat32 1MiB 100%</code>
   </li>
   <li>
      Set boot flag:<br />
      <code>(parted) set 1 boot on</code>
   </li>
   <li>
      Exit parted utility:<br />
      <code>(parted) quit</code>
   </li>
   <li>
      Verify that the partition was created successfully by performing an **lsblk** command.
      Verify that the new partition is shown in the output.
   </li>
   <li>
      Format device as a FAT32:<br />
      <code>sudo mkfs.vfat /dev/sdb1</code>
   </li>
</ol>

#### SD Card Extended File System Boot


The SD card extended (ext) file system boot requires two partitions. The first
partition should be at least 40 MB in size, be formatted as a FAT32 file
system, and be named *boot*. The second partition, which will store the root
filesystem, should be formatted as an ext4 file system, be named *rootfs*, and
can take up the remaining space of the SD card.  The second partition enables users to build on the default Petalinux file system.

To prepare the SD card for ext boot:

<ol>
   <li>
      Perform steps 1 through 3 of the previous section.
   </li>
   <li>
      Create fat32 partition with at least 50MB of space:<br />
      <code>(parted) mkpart primary fat32 1MiB 50MiB</code>
   </li>
   <li>
      Set boot flag:<br />
      <code>(parted) set 1 boot on</code>
   </li>
   <li>
      Create ext4 partition wih the rest of the space available in the SD card:<br />
      <code>(parted) mkpart primary ext4 50MiB 100%</code>
   </li>
   <li>
      Verify that the partition was created successfully by performing an
      **lsblk** command. Verify that the new partition is shown in the output.
   </li>
   <li>
      Format devices as a FAT32 and EXT4:<br />
      <code>sudo mkfs.vfat /dev/sdb1</code><br />
      <code>sudo mkfs.ext4 /dev/sdb2</code>
   </li>
</ol>

### Installing Raptor BSP


The Raptor Board Support Package (BSP) contains all the libraries and drivers
needed to build the Raptor SDR project. The Raptor BSP contains the software
stack as well as the hardware design.

To install the Raptor BSP:

<ol>
   <li>
      Change to the directory under which you want the project to be created:<br />
      <code>cd /home/user</code>
   </li>
   <li>
      Perform the *Petalinux-create* command to install the BSP:<br />
      <code>petalinux-create –t project –s &lt;path-to-bsp&gt;</code>
   </li>
</ol>

The Raptor BSP is now installed in your desired location. The BSP contains a
pre-built folder that contains the Linux images needed to boot the system
immediately; those images are located at *<plnx‑proj‑root>/pre‑built/*. Move
*BOOT.bin* and *image.ub* to the FAT32 partition of the SD card. At this point the
SD card is ready to be installed in the board.

The Raptor project is configured with a default set of drivers and libraries.
However, the PetaLinux tool can be used to add any library, driver, or
application code. Refer to the PetaLinux documentation for more details on how
to modify and rebuild a PetaLinux project.




### Raptor Bring-up


To power up your Raptor board:

<ol>
   <li>
      Move *BOOT.bin* and *image.ub* from the pre-built folder to the *boot*
      partition of the SD card.
   </li>
   <li>
      Remove the SD card from host computer and insert it into the Raptor board.
   </li>
   <li>
      Connect the micro USB cable to the USB/UART port of the Raptor board.
   </li>
   <li>
      Using terminal emulator such as putty, open a terminal and connect to the
      UART device that is assigned to the Raptor board.  Default baud rate is 115200 8N1.
   </li>
   <li>
      Power up the Raptor board.
   </li>
   <li>
      Following is an example of the messages that will be displayed in the
      console by the board:<br />
<pre>
Xilinx Zynq MP First Stage Boot Loader
Release 2016.2   Dec  9 2016  -  13:29:49
NOTICE:  ATF running on XCZU9EG/silicon v1/RTL5.1 at 0xfffe5000
NOTICE:  BL31: Secure code at 0x60000000
NOTICE:  BL31: Non secure code at 0x8000000
NOTICE:  BL31: v1.2(release):1058495
NOTICE:  BL31: Built : 07:24:40, Jun 10 2016
U-Boot 2016.01 (Jun 10 2016 - 07:24:48 +0530) Xilinx ZynqMP ZCU102
I2C:   ready
DRAM:  4 GiB
Enabling Caches...
EL Level:       EL2
MMC:   sdhci@ff170000: 0
Using default environment
In:    serial@ff000000
Out:   serial@ff000000
Err:   serial@ff000000
Bootmode: SD_MODE1
SCSI:  SATA link 0 timeout.
SATA link 1 timeout.
AHCI 0001.0301 32 slots 2 ports 6 Gbps 0x3 impl SATA mode
flags: 64bit ncq pm clo only pmp fbss pio slum part ccc apst
scanning bus for devices...
Found 0 device(s).
Net:   ZYNQ GEM: ff0e0000, phyaddr 12, interface rgmii-id

Warning: ethernet@ff0e0000 (eth0) using random MAC address - 02:71:fd:43:66:69
eth0: ethernet@ff0e0000
Hit any key to stop autoboot:  0
switch to partitions #0, OK
mmc0 is current device
Device: sdhci@ff170000
Manufacturer ID: 3
OEM: 5344
Name: SL08G
Tran Speed: 50000000
Rd Block Len: 512
SD version 3.0
High Capacity: Yes
Capacity: 7.4 GiB
Bus Width: 4-bit
Erase Group Size: 512 Bytes
reading uEnv.txt
380 bytes read in 13 ms (28.3 KiB/s)
Loaded environment from uEnv.txt
Importing environment from SD ...
reading system.dtb
35193 bytes read in 34 ms (1010.7 KiB/s)
reading Image
11489792 bytes read in 768 ms (14.3 MiB/s)
   Flattened Device Tree blob at 04000000
   Booting using the fdt blob at 0x4000000
   Loading Device Tree to 000000000fff4000, end 000000000ffff978 ... OK

Starting kernel ...
........
[    4.913267] Freeing unused kernel memory: 456K (ffffff8008a76000 - ffffff8008ae8000)
Mount failed for selinuxfs on /sys/fs/selinux:  No such file or directory
[ OK ]ting up X socket directories...        g EVIOCSKEYCODE: Invalid argumentt
[ OK ]Starting IIO Daemon iiod        
Last login: Thu Sep 29 17:49:19 UTC 2016 on ttyPS0
Welcome to Linaro 14.04 (GNU/Linux 4.6.0-08559-ga64b9bc aarch64)
 * Documentation:  wiki.analog.org ez.analog.com
2 packages can be updated.
0 updates are security updates.

New release '16.04.1 LTS' available.
Run 'do-release-upgrade' to upgrade to it.

root@raptor:~#
</pre>
   </li>
   <li>
      At this point, a check needs to be done to make sure all the IIO devices
      have been created. The **ii_info** command can be used to check the devices.<br />
<pre>
root@raptor:~# iio_info | grep iio:device
    iio:device3: cf-ad9361-lpc (buffer capable)
    iio:device1: ams
    iio:device2: cf-ad9361-dds-core-lpc (buffer capable)
    iio:device0: ad9361-phy

</pre>
   </li>
</ol>

Now your Raptor board is ready to be connected to any IIO system object backend.

## Operating System Options


The Raptor SDR features the ARM flagship Cortex-A53 64-bit quad-core processor
capable of running a great variety of software options, including Linux, RTOS,
and bare metal, to mention a few. The complete list of supported software
options for the Zynq Ultrascale+ is [here](https://www.xilinx.com/products/design-tools/software-zone/embedded-computing.html#os).


## Libiio and IIO Daemon


The Raptor BSP comes with the IIO Daemon (IIOD) server that enables you to
transmit and receive RF signals out of the box. Following is the list of
network backends that can be used to stream data to.

1. [MATLAB and Simulink](https://wiki.analog.com/resources/tools-software/linux-software/libiio/clients/matlab_simulink)
2. [Visual Analog](https://wiki.analog.com/resources/tools-software/linux-software/libiio/clients/visual_analog)
3. [IIO Oscilloscope](https://wiki.analog.com/resources/tools-software/linux-software/iio_oscilloscope)
4. [GNU Radio](https://wiki.analog.com/resources/tools-software/linux-software/gnuradio)
5. [GNU Radio Raptor Blocks](https://gitlab.com/rinconresearch/gr-raptor/)

## Source Code and Reference Designs


Analog Devices provide a set of drivers, libraries and reference designs for the AD9361 part. The Raptor SDR includes them in the BSP that is included with the board.

### Analog Devices AD9361 RF Transceiver


Analog Devices provides complete drivers for the AD9361 for both bare metal/No-OS and operating systems (Linux). The AD9361 drivers can be found at:

[Linux](https://wiki.analog.com/resources/tools-software/linux-drivers/iio-transceiver/ad9361?doc=AD9361_Reference_Manual_UG-570.pdf) - wiki page and device driver customization.

[No-OS](https://wiki.analog.com/resources/eval/user-guides/ad-fmcomms2-ebz/software/baremetal?doc=AD9361_Reference_Manual_UG-570.pdf) - wiki page.

[FPGA](https://wiki.analog.com/resources/eval/user-guides/ad-fmcomms2-ebz/reference_hdl) - HDL Reference Design.

<div class='alert alert-info'>
   The Raptor SDR BSP already has the Analog Devices drivers and hardware design included.
</div>
