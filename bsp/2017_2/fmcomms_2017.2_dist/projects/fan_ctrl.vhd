--=============================================================================
--|
--| :COPYRIGHT: 2017, RINCON RESEARCH CORPORATION, ALL RIGHTS RESERVED.
--| :LICENSE: THIS CONFIDENTIAL AND PROPRIETARY SOFTWARE MAY BE USED ONLY AS
--|           AUTHORIZED BY A LICENSING AGREEMENT FROM 
--|           RINCON RESEARCH CORPORATION.
--|
--| :AUTHOR: LM
--| :DATE: 2017-03-10
--|
--|DESCRIPTION
--|*********** 
--|  
--|
--|
--|ASSUMPTIONS
--|***********
--|
--|
--|POSSIBLE ENHANCEMENTS
--|*********************
--|
--|
--=============================================================================

--{{{ LIBRARIES/PACKAGES
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
--}}}

--#############################################################################

--{{{ ENTITY DECLARATION

entity fan_ctrl is

  port (
    clk          : in  std_logic;
    rst          : in  std_logic;
    -----------------------------------------------------------------------
    -- FAN CONTROL
    -----------------------------------------------------------------------
--    PAD_FAN_PWM     : out std_logic;
    PAD_FAN_TACH    : in  std_logic;
    fan_status_out  : out std_logic_vector(31 downto 0);
    fan_control_in  : in  std_logic_vector(31 downto 0)
    );

end entity fan_ctrl;

--}}}

--#############################################################################

architecture rtl of fan_ctrl is

  --=========================================================================
  --{{{  GLOBAL SIGNALS AND CONSTANTS
  --=========================================================================
  constant TIMER_TC_TACH : natural := 26;
  constant TIMER_TC_PWM  : natural := 5;
  signal sys_rst_n                          : std_logic;
  signal sys_clk                            : std_logic;
  signal tach_d1, tach_d2, tach_cnt_rst     : std_logic;
  signal tach_cnt, timer                    : unsigned(31 downto 0);
  signal pwm_cnt_rst, pwm_toggle            : std_logic;
  signal pwm_cnt                            : unsigned(7 downto 0);
  signal pwm_cnten, fan_tach, fan_pwm       : std_logic;
  signal tach_cnt_tc, pwm_cnten_tc          : std_logic;
  signal tach_cnt_tc_d, pwm_cnten_tc_d      : std_logic;
  signal tach_rate                          : std_logic_vector(31 downto 0);
begin

  -- FAN CONTROL 
  fan_tach_ibuf : ibuf port map (O => fan_tach, I => PAD_FAN_TACH);
--  fan_pwm_obuf  : obuf port map (I => fan_pwm,  O => PAD_FAN_PWM);
  fan_status_out <= tach_rate;

  --=========================================================================
  --  Generate a counter to determine the frequency of the fan
  --  tachometer input  This count is latched whenever the terminal
  --  the tachometer will pulse twice per revolution
  --=========================================================================
  tach_counter_proc : process ( clk, rst ) is
  begin

    if ( rst = '1' ) then
      tach_d1 <= '1';
      tach_d2 <= '1';
    elsif ( clk'event and clk = '1' ) then
      tach_d1 <= fan_tach;
      tach_d2 <= tach_d1;
    end if;

    if ( clk'event and clk = '1' ) then
      if ( tach_cnt_rst = '1' ) then
        tach_cnt <= (others => '0');
      elsif ( tach_d2 = '0' and tach_d1 = '1' ) then
        tach_cnt <= tach_cnt + 1;
      end if;
    end if;

  end process tach_counter_proc;


  --=========================================================================
  --  Generate a counter to create a PWM signal for the fan  The period
  --  is specified as the number of 640 ns cycles to count before the
  --  waveform repeats  The duty cycle is specified as the count value
  --  where the PWM signal should transition High-to-Low  Setting the
  --  duty cycle to the period count sets the PWM to be a constant high
  --  signal
  --=========================================================================
  pwm_cnt_rst <= '1' when ( pwm_cnt = unsigned(fan_control_in(15 downto 8))) else '0';
  pwm_toggle  <= '1' when ( pwm_cnt = unsigned(fan_control_in(23 downto 16))) else '0';

  pwm_counter_proc : process ( clk, rst ) is
  begin

    if ( rst = '1' ) then

      pwm_cnt <= (others => '0');
      fan_pwm <= '1';

    elsif ( clk'event and clk = '1' ) then

      -- The PWM counter and control signal should only be modified when
      -- the timing interval has elapsed => ie, 'pwm_cnten' asserted)
      if ( pwm_cnten = '1' ) then

        if ( pwm_cnt_rst = '1' ) then
          pwm_cnt <= (others => '0');
          fan_pwm <= '1';
        else

          -- Increment the PWM period counter and toggle the PWM signal
          -- if the duty cycle point has been reached
          pwm_cnt <= pwm_cnt + 1;
          if ( pwm_toggle = '1' ) then
            fan_pwm <= '0';
          end if;

        end if;

      end if;

    end if;

  end process pwm_counter_proc;
--=========================================================================
  --  Generate a timing counter to measure a constant period  This
  --  counter wraps naturally, and the most significant bit is used
  --  as a level-type terminal count flag => ie, a low-to-high or
  --  high-to-low change represents a terminal count)  This will
  --  aid in crossing the terminal count across clock domains
  --=========================================================================
  tach_cnt_tc  <= timer(TIMER_TC_TACH);
  pwm_cnten_tc <= timer(TIMER_TC_PWM);

  timer_proc : process ( clk, rst ) is
  begin

    if ( rst = '1' ) then

      timer          <= (others => '0');

      tach_cnt_tc_d  <= '0';
      tach_cnt_rst   <= '1';
      tach_rate      <= (others => '0');

      pwm_cnten_tc_d <= '0';
      pwm_cnten      <= '0';

    elsif ( clk'event and clk = '1' ) then

      timer <= timer + 1;


      -- Latch the fan tachometer frequency count when the counter reset
      -- signal is asserted
      tach_cnt_tc_d <= tach_cnt_tc;
      tach_cnt_rst  <= tach_cnt_tc_d xor tach_cnt_tc;

      if ( tach_cnt_rst = '1' ) then
        tach_rate <= std_logic_vector(tach_cnt);
      end if;

      -- Generate a single-cycle wide pulse to enable the PWM period counter

      pwm_cnten_tc_d <= pwm_cnten_tc;
      pwm_cnten      <= pwm_cnten_tc_d xor pwm_cnten_tc;

    end if;

  end process timer_proc;


  --}}}

end architecture rtl;

--#############################################################################
