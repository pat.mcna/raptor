
################################################################
# This is a generated script based on design: system
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2016.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source system_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xczu9eg-ffvc900-1-i-es1
}


# CHANGE DESIGN NAME HERE
set design_name system

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports

  # Create ports
  set enable [ create_bd_port -dir O enable ]
  set gpio_i [ create_bd_port -dir I -from 94 -to 0 gpio_i ]
  set gpio_o [ create_bd_port -dir O -from 94 -to 0 gpio_o ]
  set ps_intr_00 [ create_bd_port -dir I -type intr ps_intr_00 ]
  set ps_intr_01 [ create_bd_port -dir I -type intr ps_intr_01 ]
  set ps_intr_02 [ create_bd_port -dir I -type intr ps_intr_02 ]
  set ps_intr_03 [ create_bd_port -dir I -type intr ps_intr_03 ]
  set ps_intr_04 [ create_bd_port -dir I -type intr ps_intr_04 ]
  set ps_intr_05 [ create_bd_port -dir I -type intr ps_intr_05 ]
  set ps_intr_06 [ create_bd_port -dir I -type intr ps_intr_06 ]
  set ps_intr_07 [ create_bd_port -dir I -type intr ps_intr_07 ]
  set ps_intr_08 [ create_bd_port -dir I -type intr ps_intr_08 ]
  set ps_intr_09 [ create_bd_port -dir I -type intr ps_intr_09 ]
  set ps_intr_10 [ create_bd_port -dir I -type intr ps_intr_10 ]
  set ps_intr_11 [ create_bd_port -dir I -type intr ps_intr_11 ]
  set ps_intr_14 [ create_bd_port -dir I -type intr ps_intr_14 ]
  set ps_intr_15 [ create_bd_port -dir I -type intr ps_intr_15 ]
  set rx_clk_in_n [ create_bd_port -dir I rx_clk_in_n ]
  set rx_clk_in_p [ create_bd_port -dir I rx_clk_in_p ]
  set rx_data_in_n [ create_bd_port -dir I -from 5 -to 0 rx_data_in_n ]
  set rx_data_in_p [ create_bd_port -dir I -from 5 -to 0 rx_data_in_p ]
  set rx_frame_in_n [ create_bd_port -dir I rx_frame_in_n ]
  set rx_frame_in_p [ create_bd_port -dir I rx_frame_in_p ]
  set tdd_sync_i [ create_bd_port -dir I tdd_sync_i ]
  set tdd_sync_o [ create_bd_port -dir O tdd_sync_o ]
  set tdd_sync_t [ create_bd_port -dir O tdd_sync_t ]
  set tx_clk_out_n [ create_bd_port -dir O tx_clk_out_n ]
  set tx_clk_out_p [ create_bd_port -dir O tx_clk_out_p ]
  set tx_data_out_n [ create_bd_port -dir O -from 5 -to 0 tx_data_out_n ]
  set tx_data_out_p [ create_bd_port -dir O -from 5 -to 0 tx_data_out_p ]
  set tx_frame_out_n [ create_bd_port -dir O tx_frame_out_n ]
  set tx_frame_out_p [ create_bd_port -dir O tx_frame_out_p ]
  set txnrx [ create_bd_port -dir O txnrx ]
  set up_enable [ create_bd_port -dir I up_enable ]
  set up_txnrx [ create_bd_port -dir I up_txnrx ]

  # Create instance: axi_ad9361, and set properties
  set axi_ad9361 [ create_bd_cell -type ip -vlnv analog.com:user:axi_ad9361:1.0 axi_ad9361 ]
  set_property -dict [ list \
CONFIG.DEVICE_TYPE {2} \
CONFIG.ID {0} \
 ] $axi_ad9361

  # Create instance: axi_ad9361_adc_dma, and set properties
  set axi_ad9361_adc_dma [ create_bd_cell -type ip -vlnv analog.com:user:axi_dmac:1.0 axi_ad9361_adc_dma ]
  set_property -dict [ list \
CONFIG.AXI_SLICE_DEST {false} \
CONFIG.AXI_SLICE_SRC {false} \
CONFIG.CYCLIC {false} \
CONFIG.DMA_2D_TRANSFER {false} \
CONFIG.DMA_DATA_WIDTH_SRC {64} \
CONFIG.DMA_TYPE_DEST {0} \
CONFIG.DMA_TYPE_SRC {2} \
CONFIG.SYNC_TRANSFER_START {true} \
 ] $axi_ad9361_adc_dma

  # Create instance: axi_ad9361_dac_dma, and set properties
  set axi_ad9361_dac_dma [ create_bd_cell -type ip -vlnv analog.com:user:axi_dmac:1.0 axi_ad9361_dac_dma ]
  set_property -dict [ list \
CONFIG.AXI_SLICE_DEST {true} \
CONFIG.AXI_SLICE_SRC {false} \
CONFIG.CYCLIC {true} \
CONFIG.DMA_2D_TRANSFER {false} \
CONFIG.DMA_DATA_WIDTH_DEST {64} \
CONFIG.DMA_TYPE_DEST {2} \
CONFIG.DMA_TYPE_SRC {0} \
 ] $axi_ad9361_dac_dma

  # Create instance: axi_cpu_interconnect, and set properties
  set axi_cpu_interconnect [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_cpu_interconnect ]
  set_property -dict [ list \
CONFIG.NUM_MI {4} \
 ] $axi_cpu_interconnect

  # Create instance: axi_hp1_interconnect, and set properties
  set axi_hp1_interconnect [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_hp1_interconnect ]
  set_property -dict [ list \
CONFIG.NUM_MI {1} \
CONFIG.NUM_SI {1} \
 ] $axi_hp1_interconnect

  # Create instance: axi_hp2_interconnect, and set properties
  set axi_hp2_interconnect [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_hp2_interconnect ]
  set_property -dict [ list \
CONFIG.NUM_MI {1} \
CONFIG.NUM_SI {1} \
 ] $axi_hp2_interconnect

  # Create instance: clkdiv, and set properties
  set clkdiv [ create_bd_cell -type ip -vlnv analog.com:user:util_clkdiv:1.0 clkdiv ]
  set_property -dict [ list \
CONFIG.SIM_DEVICE {ULTRASCALE} \
 ] $clkdiv

  # Create instance: clkdiv_reset, and set properties
  set clkdiv_reset [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 clkdiv_reset ]

  # Create instance: clkdiv_sel_logic, and set properties
  set clkdiv_sel_logic [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_reduced_logic:2.0 clkdiv_sel_logic ]
  set_property -dict [ list \
CONFIG.C_SIZE {2} \
 ] $clkdiv_sel_logic

  # Create instance: concat_logic, and set properties
  set concat_logic [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 concat_logic ]
  set_property -dict [ list \
CONFIG.NUM_PORTS {2} \
 ] $concat_logic

  # Create instance: dac_fifo, and set properties
  set dac_fifo [ create_bd_cell -type ip -vlnv analog.com:user:util_rfifo:1.0 dac_fifo ]
  set_property -dict [ list \
CONFIG.DIN_ADDRESS_WIDTH {4} \
CONFIG.DIN_DATA_WIDTH {16} \
CONFIG.DOUT_DATA_WIDTH {16} \
 ] $dac_fifo

  # Create instance: sys_concat_intc_0, and set properties
  set sys_concat_intc_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 sys_concat_intc_0 ]
  set_property -dict [ list \
CONFIG.NUM_PORTS {8} \
 ] $sys_concat_intc_0

  # Create instance: sys_concat_intc_1, and set properties
  set sys_concat_intc_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 sys_concat_intc_1 ]
  set_property -dict [ list \
CONFIG.NUM_PORTS {8} \
 ] $sys_concat_intc_1

  # Create instance: sys_ps8, and set properties
  set sys_ps8 [ create_bd_cell -type ip -vlnv xilinx.com:ip:zynq_ultra_ps_e:1.2 sys_ps8 ]
  set_property -dict [ list \
CONFIG.PSU_BANK_0_IO_STANDARD {LVCMOS33} \
CONFIG.PSU_BANK_1_IO_STANDARD {LVCMOS33} \
CONFIG.PSU_BANK_2_IO_STANDARD {LVCMOS33} \
CONFIG.PSU_DDR_RAM_HIGHADDR {0xFFFFFFFF} \
CONFIG.PSU_DDR_RAM_HIGHADDR_OFFSET {0x80000000} \
CONFIG.PSU_MIO_0_DIRECTION {out} \
CONFIG.PSU_MIO_0_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_0_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_0_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_0_SLEW {slow} \
CONFIG.PSU_MIO_10_DIRECTION {inout} \
CONFIG.PSU_MIO_10_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_10_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_10_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_10_SLEW {slow} \
CONFIG.PSU_MIO_11_DIRECTION {inout} \
CONFIG.PSU_MIO_11_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_11_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_11_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_11_SLEW {slow} \
CONFIG.PSU_MIO_12_DIRECTION {out} \
CONFIG.PSU_MIO_12_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_12_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_12_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_12_SLEW {slow} \
CONFIG.PSU_MIO_13_DIRECTION {inout} \
CONFIG.PSU_MIO_13_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_13_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_13_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_13_SLEW {slow} \
CONFIG.PSU_MIO_14_DIRECTION {in} \
CONFIG.PSU_MIO_14_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_14_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_14_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_14_SLEW {slow} \
CONFIG.PSU_MIO_15_DIRECTION {out} \
CONFIG.PSU_MIO_15_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_15_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_15_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_15_SLEW {slow} \
CONFIG.PSU_MIO_16_DIRECTION {inout} \
CONFIG.PSU_MIO_16_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_16_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_16_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_16_SLEW {slow} \
CONFIG.PSU_MIO_17_DIRECTION {inout} \
CONFIG.PSU_MIO_17_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_17_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_17_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_17_SLEW {slow} \
CONFIG.PSU_MIO_18_DIRECTION {inout} \
CONFIG.PSU_MIO_18_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_18_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_18_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_18_SLEW {slow} \
CONFIG.PSU_MIO_19_DIRECTION {out} \
CONFIG.PSU_MIO_19_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_19_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_19_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_19_SLEW {slow} \
CONFIG.PSU_MIO_1_DIRECTION {inout} \
CONFIG.PSU_MIO_1_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_1_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_1_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_1_SLEW {slow} \
CONFIG.PSU_MIO_20_DIRECTION {out} \
CONFIG.PSU_MIO_20_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_20_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_20_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_20_SLEW {slow} \
CONFIG.PSU_MIO_21_DIRECTION {inout} \
CONFIG.PSU_MIO_21_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_21_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_21_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_21_SLEW {slow} \
CONFIG.PSU_MIO_22_DIRECTION {inout} \
CONFIG.PSU_MIO_22_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_22_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_22_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_22_SLEW {slow} \
CONFIG.PSU_MIO_23_DIRECTION {inout} \
CONFIG.PSU_MIO_23_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_23_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_23_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_23_SLEW {slow} \
CONFIG.PSU_MIO_24_DIRECTION {out} \
CONFIG.PSU_MIO_24_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_24_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_24_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_24_SLEW {slow} \
CONFIG.PSU_MIO_25_DIRECTION {in} \
CONFIG.PSU_MIO_25_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_25_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_25_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_25_SLEW {slow} \
CONFIG.PSU_MIO_26_DIRECTION {inout} \
CONFIG.PSU_MIO_26_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_26_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_26_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_26_SLEW {slow} \
CONFIG.PSU_MIO_27_DIRECTION {inout} \
CONFIG.PSU_MIO_27_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_27_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_27_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_27_SLEW {slow} \
CONFIG.PSU_MIO_28_DIRECTION {inout} \
CONFIG.PSU_MIO_28_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_28_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_28_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_28_SLEW {slow} \
CONFIG.PSU_MIO_29_DIRECTION {inout} \
CONFIG.PSU_MIO_29_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_29_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_29_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_29_SLEW {slow} \
CONFIG.PSU_MIO_2_DIRECTION {inout} \
CONFIG.PSU_MIO_2_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_2_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_2_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_2_SLEW {slow} \
CONFIG.PSU_MIO_30_DIRECTION {inout} \
CONFIG.PSU_MIO_30_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_30_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_30_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_30_SLEW {slow} \
CONFIG.PSU_MIO_31_DIRECTION {out} \
CONFIG.PSU_MIO_31_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_31_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_31_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_31_SLEW {slow} \
CONFIG.PSU_MIO_32_DIRECTION {inout} \
CONFIG.PSU_MIO_32_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_32_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_32_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_32_SLEW {slow} \
CONFIG.PSU_MIO_33_DIRECTION {inout} \
CONFIG.PSU_MIO_33_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_33_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_33_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_33_SLEW {slow} \
CONFIG.PSU_MIO_34_DIRECTION {inout} \
CONFIG.PSU_MIO_34_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_34_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_34_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_34_SLEW {slow} \
CONFIG.PSU_MIO_35_DIRECTION {inout} \
CONFIG.PSU_MIO_35_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_35_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_35_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_35_SLEW {slow} \
CONFIG.PSU_MIO_36_DIRECTION {inout} \
CONFIG.PSU_MIO_36_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_36_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_36_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_36_SLEW {slow} \
CONFIG.PSU_MIO_37_DIRECTION {inout} \
CONFIG.PSU_MIO_37_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_37_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_37_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_37_SLEW {slow} \
CONFIG.PSU_MIO_38_DIRECTION {inout} \
CONFIG.PSU_MIO_38_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_38_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_38_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_38_SLEW {slow} \
CONFIG.PSU_MIO_39_DIRECTION {inout} \
CONFIG.PSU_MIO_39_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_39_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_39_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_39_SLEW {slow} \
CONFIG.PSU_MIO_3_DIRECTION {inout} \
CONFIG.PSU_MIO_3_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_3_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_3_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_3_SLEW {slow} \
CONFIG.PSU_MIO_40_DIRECTION {inout} \
CONFIG.PSU_MIO_40_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_40_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_40_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_40_SLEW {slow} \
CONFIG.PSU_MIO_41_DIRECTION {inout} \
CONFIG.PSU_MIO_41_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_41_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_41_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_41_SLEW {slow} \
CONFIG.PSU_MIO_42_DIRECTION {inout} \
CONFIG.PSU_MIO_42_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_42_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_42_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_42_SLEW {slow} \
CONFIG.PSU_MIO_43_DIRECTION {out} \
CONFIG.PSU_MIO_43_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_43_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_43_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_43_SLEW {slow} \
CONFIG.PSU_MIO_44_DIRECTION {in} \
CONFIG.PSU_MIO_44_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_44_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_44_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_44_SLEW {slow} \
CONFIG.PSU_MIO_45_DIRECTION {in} \
CONFIG.PSU_MIO_45_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_45_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_45_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_45_SLEW {slow} \
CONFIG.PSU_MIO_46_DIRECTION {inout} \
CONFIG.PSU_MIO_46_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_46_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_46_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_46_SLEW {slow} \
CONFIG.PSU_MIO_47_DIRECTION {inout} \
CONFIG.PSU_MIO_47_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_47_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_47_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_47_SLEW {slow} \
CONFIG.PSU_MIO_48_DIRECTION {inout} \
CONFIG.PSU_MIO_48_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_48_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_48_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_48_SLEW {slow} \
CONFIG.PSU_MIO_49_DIRECTION {inout} \
CONFIG.PSU_MIO_49_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_49_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_49_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_49_SLEW {slow} \
CONFIG.PSU_MIO_4_DIRECTION {inout} \
CONFIG.PSU_MIO_4_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_4_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_4_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_4_SLEW {slow} \
CONFIG.PSU_MIO_50_DIRECTION {inout} \
CONFIG.PSU_MIO_50_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_50_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_50_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_50_SLEW {slow} \
CONFIG.PSU_MIO_51_DIRECTION {out} \
CONFIG.PSU_MIO_51_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_51_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_51_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_51_SLEW {slow} \
CONFIG.PSU_MIO_52_DIRECTION {in} \
CONFIG.PSU_MIO_52_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_52_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_52_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_52_SLEW {slow} \
CONFIG.PSU_MIO_53_DIRECTION {in} \
CONFIG.PSU_MIO_53_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_53_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_53_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_53_SLEW {slow} \
CONFIG.PSU_MIO_54_DIRECTION {inout} \
CONFIG.PSU_MIO_54_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_54_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_54_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_54_SLEW {slow} \
CONFIG.PSU_MIO_55_DIRECTION {in} \
CONFIG.PSU_MIO_55_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_55_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_55_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_55_SLEW {slow} \
CONFIG.PSU_MIO_56_DIRECTION {inout} \
CONFIG.PSU_MIO_56_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_56_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_56_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_56_SLEW {slow} \
CONFIG.PSU_MIO_57_DIRECTION {inout} \
CONFIG.PSU_MIO_57_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_57_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_57_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_57_SLEW {slow} \
CONFIG.PSU_MIO_58_DIRECTION {out} \
CONFIG.PSU_MIO_58_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_58_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_58_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_58_SLEW {slow} \
CONFIG.PSU_MIO_59_DIRECTION {inout} \
CONFIG.PSU_MIO_59_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_59_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_59_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_59_SLEW {slow} \
CONFIG.PSU_MIO_5_DIRECTION {out} \
CONFIG.PSU_MIO_5_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_5_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_5_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_5_SLEW {slow} \
CONFIG.PSU_MIO_60_DIRECTION {inout} \
CONFIG.PSU_MIO_60_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_60_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_60_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_60_SLEW {slow} \
CONFIG.PSU_MIO_61_DIRECTION {inout} \
CONFIG.PSU_MIO_61_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_61_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_61_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_61_SLEW {slow} \
CONFIG.PSU_MIO_62_DIRECTION {inout} \
CONFIG.PSU_MIO_62_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_62_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_62_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_62_SLEW {slow} \
CONFIG.PSU_MIO_63_DIRECTION {inout} \
CONFIG.PSU_MIO_63_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_63_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_63_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_63_SLEW {slow} \
CONFIG.PSU_MIO_64_DIRECTION {in} \
CONFIG.PSU_MIO_64_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_64_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_64_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_64_SLEW {slow} \
CONFIG.PSU_MIO_65_DIRECTION {in} \
CONFIG.PSU_MIO_65_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_65_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_65_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_65_SLEW {slow} \
CONFIG.PSU_MIO_66_DIRECTION {inout} \
CONFIG.PSU_MIO_66_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_66_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_66_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_66_SLEW {slow} \
CONFIG.PSU_MIO_67_DIRECTION {in} \
CONFIG.PSU_MIO_67_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_67_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_67_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_67_SLEW {slow} \
CONFIG.PSU_MIO_68_DIRECTION {inout} \
CONFIG.PSU_MIO_68_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_68_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_68_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_68_SLEW {slow} \
CONFIG.PSU_MIO_69_DIRECTION {inout} \
CONFIG.PSU_MIO_69_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_69_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_69_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_69_SLEW {slow} \
CONFIG.PSU_MIO_6_DIRECTION {out} \
CONFIG.PSU_MIO_6_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_6_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_6_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_6_SLEW {slow} \
CONFIG.PSU_MIO_70_DIRECTION {out} \
CONFIG.PSU_MIO_70_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_70_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_70_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_70_SLEW {slow} \
CONFIG.PSU_MIO_71_DIRECTION {inout} \
CONFIG.PSU_MIO_71_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_71_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_71_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_71_SLEW {slow} \
CONFIG.PSU_MIO_72_DIRECTION {inout} \
CONFIG.PSU_MIO_72_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_72_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_72_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_72_SLEW {slow} \
CONFIG.PSU_MIO_73_DIRECTION {inout} \
CONFIG.PSU_MIO_73_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_73_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_73_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_73_SLEW {slow} \
CONFIG.PSU_MIO_74_DIRECTION {inout} \
CONFIG.PSU_MIO_74_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_74_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_74_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_74_SLEW {slow} \
CONFIG.PSU_MIO_75_DIRECTION {inout} \
CONFIG.PSU_MIO_75_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_75_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_75_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_75_SLEW {slow} \
CONFIG.PSU_MIO_76_DIRECTION {out} \
CONFIG.PSU_MIO_76_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_76_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_76_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_76_SLEW {slow} \
CONFIG.PSU_MIO_77_DIRECTION {inout} \
CONFIG.PSU_MIO_77_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_77_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_77_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_77_SLEW {slow} \
CONFIG.PSU_MIO_7_DIRECTION {out} \
CONFIG.PSU_MIO_7_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_7_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_7_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_7_SLEW {slow} \
CONFIG.PSU_MIO_8_DIRECTION {inout} \
CONFIG.PSU_MIO_8_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_8_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_8_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_8_SLEW {slow} \
CONFIG.PSU_MIO_9_DIRECTION {inout} \
CONFIG.PSU_MIO_9_DRIVE_STRENGTH {12} \
CONFIG.PSU_MIO_9_INPUT_TYPE {schmitt} \
CONFIG.PSU_MIO_9_PULLUPDOWN {pullup} \
CONFIG.PSU_MIO_9_SLEW {slow} \
CONFIG.PSU_MIO_TREE_PERIPHERALS {Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash##Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash##UART 0#UART 0#I2C 1#I2C 1#SPI 1#SPI 1#SPI 1#SPI 1#SPI 1#SPI 1#UART 1#UART 1###################SD 1#SD 1#SD 1#SD 1#SD 1#SD 1#SD 1#SD 1#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 1#USB 1#USB 1#USB 1#USB 1#USB 1#USB 1#USB 1#USB 1#USB 1#USB 1#USB 1##} \
CONFIG.PSU_MIO_TREE_SIGNALS {sclk_out#so_mo1#mo2#mo3#si_mi0#n_ss_out##n_ss_out_upper#mo_upper[0]#mo_upper[1]#mo_upper[2]#mo_upper[3]#sclk_out_upper##rxd#txd#scl_out#sda_out#so#n_ss_out[2]#n_ss_out[1]#n_ss_out[0]#sclk_out#si#txd#rxd###################sdio1_wp#sdio1_cd_n#sdio1_data_out[0]#sdio1_data_out[1]#sdio1_data_out[2]#sdio1_data_out[3]#sdio1_cmd_out#sdio1_clk_out#ulpi_clk_in#ulpi_dir#ulpi_tx_data[2]#ulpi_nxt#ulpi_tx_data[0]#ulpi_tx_data[1]#ulpi_stp#ulpi_tx_data[3]#ulpi_tx_data[4]#ulpi_tx_data[5]#ulpi_tx_data[6]#ulpi_tx_data[7]#ulpi_clk_in#ulpi_dir#ulpi_tx_data[2]#ulpi_nxt#ulpi_tx_data[0]#ulpi_tx_data[1]#ulpi_stp#ulpi_tx_data[3]#ulpi_tx_data[4]#ulpi_tx_data[5]#ulpi_tx_data[6]#ulpi_tx_data[7]##} \
CONFIG.PSU_PACKAGE_DDR_BOARD_DELAY3 {0.100} \
CONFIG.PSU_PRESET_BANK0_VOLTAGE {<Select>} \
CONFIG.PSU_PRESET_BANK1_VOLTAGE {<Select>} \
CONFIG.PSU_UIPARAM_GENERATE_SUMMARY {<Select>} \
CONFIG.PSU__ACPU0__POWER__ON {1} \
CONFIG.PSU__ACPU1__POWER__ON {1} \
CONFIG.PSU__ACPU2__POWER__ON {1} \
CONFIG.PSU__ACPU3__POWER__ON {1} \
CONFIG.PSU__ACT_DDR_FREQ_MHZ {1} \
CONFIG.PSU__ADMA_COHERENCY {1} \
CONFIG.PSU__AUX_REF_CLK__FREQMHZ {33.333} \
CONFIG.PSU__CAN0_LOOP_CAN1__ENABLE {0} \
CONFIG.PSU__CAN0__GRP_CLK__ENABLE {0} \
CONFIG.PSU__CAN0__GRP_CLK__IO {<Select>} \
CONFIG.PSU__CAN0__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__CAN0__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__CAN1__GRP_CLK__ENABLE {0} \
CONFIG.PSU__CAN1__GRP_CLK__IO {<Select>} \
CONFIG.PSU__CAN1__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__CAN1__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__CPU_CPU_6X4X_MAX_RANGE {1} \
CONFIG.PSU__CPU_R5__PERIPHERAL__ENABLE {1} \
CONFIG.PSU__CRF_APB__ACPU_CTRL__ACT_FREQMHZ {1099.989014} \
CONFIG.PSU__CRF_APB__ACPU_CTRL__DIVISOR0 {1} \
CONFIG.PSU__CRF_APB__ACPU_CTRL__FREQMHZ {1200} \
CONFIG.PSU__CRF_APB__ACPU_CTRL__SRCSEL {APLL} \
CONFIG.PSU__CRF_APB__ACPU__ENABLE {1} \
CONFIG.PSU__CRF_APB__AFI0_REF_CTRL__ACT_FREQMHZ {667} \
CONFIG.PSU__CRF_APB__AFI0_REF_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRF_APB__AFI0_REF_CTRL__FREQMHZ {667} \
CONFIG.PSU__CRF_APB__AFI0_REF_CTRL__SRCSEL {DPLL} \
CONFIG.PSU__CRF_APB__AFI0_REF__ENABLE {0} \
CONFIG.PSU__CRF_APB__AFI1_REF_CTRL__ACT_FREQMHZ {667} \
CONFIG.PSU__CRF_APB__AFI1_REF_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRF_APB__AFI1_REF_CTRL__FREQMHZ {667} \
CONFIG.PSU__CRF_APB__AFI1_REF_CTRL__SRCSEL {DPLL} \
CONFIG.PSU__CRF_APB__AFI1_REF__ENABLE {0} \
CONFIG.PSU__CRF_APB__AFI2_REF_CTRL__ACT_FREQMHZ {667} \
CONFIG.PSU__CRF_APB__AFI2_REF_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRF_APB__AFI2_REF_CTRL__FREQMHZ {667} \
CONFIG.PSU__CRF_APB__AFI2_REF_CTRL__SRCSEL {DPLL} \
CONFIG.PSU__CRF_APB__AFI2_REF__ENABLE {0} \
CONFIG.PSU__CRF_APB__AFI3_REF_CTRL__ACT_FREQMHZ {667} \
CONFIG.PSU__CRF_APB__AFI3_REF_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRF_APB__AFI3_REF_CTRL__FREQMHZ {667} \
CONFIG.PSU__CRF_APB__AFI3_REF_CTRL__SRCSEL {DPLL} \
CONFIG.PSU__CRF_APB__AFI3_REF__ENABLE {0} \
CONFIG.PSU__CRF_APB__AFI4_REF_CTRL__ACT_FREQMHZ {667} \
CONFIG.PSU__CRF_APB__AFI4_REF_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRF_APB__AFI4_REF_CTRL__FREQMHZ {667} \
CONFIG.PSU__CRF_APB__AFI4_REF_CTRL__SRCSEL {DPLL} \
CONFIG.PSU__CRF_APB__AFI4_REF__ENABLE {0} \
CONFIG.PSU__CRF_APB__AFI5_REF_CTRL__ACT_FREQMHZ {667} \
CONFIG.PSU__CRF_APB__AFI5_REF_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRF_APB__AFI5_REF_CTRL__FREQMHZ {667} \
CONFIG.PSU__CRF_APB__AFI5_REF_CTRL__SRCSEL {DPLL} \
CONFIG.PSU__CRF_APB__AFI5_REF__ENABLE {0} \
CONFIG.PSU__CRF_APB__APLL_CTRL__DIV2 {1} \
CONFIG.PSU__CRF_APB__APLL_CTRL__FBDIV {66} \
CONFIG.PSU__CRF_APB__APLL_CTRL__FRACDATA {0.000} \
CONFIG.PSU__CRF_APB__APLL_CTRL__FRACFREQ {27.138} \
CONFIG.PSU__CRF_APB__APLL_CTRL__SRCSEL {PSS_REF_CLK} \
CONFIG.PSU__CRF_APB__APLL_FRAC_CFG__ENABLED {0} \
CONFIG.PSU__CRF_APB__APLL_TO_LPD_CTRL__DIVISOR0 {3} \
CONFIG.PSU__CRF_APB__APLL_TO_LPD_CTRL__SRCSEL {APLL} \
CONFIG.PSU__CRF_APB__APM_CTRL__ACT_FREQMHZ {1} \
CONFIG.PSU__CRF_APB__APM_CTRL__DIVISOR0 {1} \
CONFIG.PSU__CRF_APB__APM_CTRL__FREQMHZ {1} \
CONFIG.PSU__CRF_APB__APM_CTRL__SRCSEL {<Select>} \
CONFIG.PSU__CRF_APB__DBG_FPD_CTRL__ACT_FREQMHZ {249.997498} \
CONFIG.PSU__CRF_APB__DBG_FPD_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRF_APB__DBG_FPD_CTRL__FREQMHZ {250} \
CONFIG.PSU__CRF_APB__DBG_FPD_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRF_APB__DBG_FPD__ENABLE {1} \
CONFIG.PSU__CRF_APB__DBG_TRACE_CTRL__ACT_FREQMHZ {249.997498} \
CONFIG.PSU__CRF_APB__DBG_TRACE_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRF_APB__DBG_TRACE_CTRL__FREQMHZ {250} \
CONFIG.PSU__CRF_APB__DBG_TRACE_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRF_APB__DBG_TRACE__ENABLE {1} \
CONFIG.PSU__CRF_APB__DBG_TSTMP_CTRL__ACT_FREQMHZ {249.997498} \
CONFIG.PSU__CRF_APB__DBG_TSTMP_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRF_APB__DBG_TSTMP_CTRL__FREQMHZ {250} \
CONFIG.PSU__CRF_APB__DBG_TSTMP_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRF_APB__DBG_TSTMP__ENABLE {1} \
CONFIG.PSU__CRF_APB__DDR_CTRL__ACT_FREQMHZ {533.328003} \
CONFIG.PSU__CRF_APB__DDR_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRF_APB__DDR_CTRL__FREQMHZ {1200} \
CONFIG.PSU__CRF_APB__DDR_CTRL__SRCSEL {DPLL} \
CONFIG.PSU__CRF_APB__DFT125_REF_CTRL__DIVISOR0 {10} \
CONFIG.PSU__CRF_APB__DFT125_REF_CTRL__SRCSEL {APLL} \
CONFIG.PSU__CRF_APB__DFT250_REF_CTRL__DIVISOR0 {5} \
CONFIG.PSU__CRF_APB__DFT250_REF_CTRL__SRCSEL {APLL} \
CONFIG.PSU__CRF_APB__DFT270_REF_CTRL__DIVISOR0 {5} \
CONFIG.PSU__CRF_APB__DFT270_REF_CTRL__SRCSEL {APLL} \
CONFIG.PSU__CRF_APB__DFT300_REF_CTRL__DIVISOR0 {4} \
CONFIG.PSU__CRF_APB__DFT300_REF_CTRL__SRCSEL {DPLL} \
CONFIG.PSU__CRF_APB__DPDMA_REF_CTRL__ACT_FREQMHZ {549.994507} \
CONFIG.PSU__CRF_APB__DPDMA_REF_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRF_APB__DPDMA_REF_CTRL__FREQMHZ {600} \
CONFIG.PSU__CRF_APB__DPDMA_REF_CTRL__SRCSEL {APLL} \
CONFIG.PSU__CRF_APB__DPDMA__ENABLE {1} \
CONFIG.PSU__CRF_APB__DPLL_CTRL__DIV2 {1} \
CONFIG.PSU__CRF_APB__DPLL_CTRL__FBDIV {64} \
CONFIG.PSU__CRF_APB__DPLL_CTRL__FRACDATA {0.000} \
CONFIG.PSU__CRF_APB__DPLL_CTRL__FRACFREQ {27.138} \
CONFIG.PSU__CRF_APB__DPLL_CTRL__SRCSEL {PSS_REF_CLK} \
CONFIG.PSU__CRF_APB__DPLL_FRAC_CFG__ENABLED {0} \
CONFIG.PSU__CRF_APB__DPLL_TO_LPD_CTRL__DIVISOR0 {3} \
CONFIG.PSU__CRF_APB__DPLL_TO_LPD_CTRL__SRCSEL {DPLL} \
CONFIG.PSU__CRF_APB__DP_AUDIO_REF_CTRL__ACT_FREQMHZ {25.000} \
CONFIG.PSU__CRF_APB__DP_AUDIO_REF_CTRL__DIVISOR0 {39} \
CONFIG.PSU__CRF_APB__DP_AUDIO_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRF_APB__DP_AUDIO_REF_CTRL__FREQMHZ {25} \
CONFIG.PSU__CRF_APB__DP_AUDIO_REF_CTRL__SRCSEL {VPLL} \
CONFIG.PSU__CRF_APB__DP_STC_REF_CTRL__ACT_FREQMHZ {27.000} \
CONFIG.PSU__CRF_APB__DP_STC_REF_CTRL__DIVISOR0 {17} \
CONFIG.PSU__CRF_APB__DP_STC_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRF_APB__DP_STC_REF_CTRL__FREQMHZ {27} \
CONFIG.PSU__CRF_APB__DP_STC_REF_CTRL__SRCSEL {RPLL} \
CONFIG.PSU__CRF_APB__DP_VIDEO_REF_CTRL__ACT_FREQMHZ {269.997} \
CONFIG.PSU__CRF_APB__DP_VIDEO_REF_CTRL__DIVISOR0 {3} \
CONFIG.PSU__CRF_APB__DP_VIDEO_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRF_APB__DP_VIDEO_REF_CTRL__FREQMHZ {320} \
CONFIG.PSU__CRF_APB__DP_VIDEO_REF_CTRL__SRCSEL {RPLL} \
CONFIG.PSU__CRF_APB__GDMA_REF_CTRL__ACT_FREQMHZ {549.994507} \
CONFIG.PSU__CRF_APB__GDMA_REF_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRF_APB__GDMA_REF_CTRL__FREQMHZ {600} \
CONFIG.PSU__CRF_APB__GDMA_REF_CTRL__SRCSEL {APLL} \
CONFIG.PSU__CRF_APB__GDMA__ENABLE {1} \
CONFIG.PSU__CRF_APB__GPU_REF_CTRL__ACT_FREQMHZ {499.994995} \
CONFIG.PSU__CRF_APB__GPU_REF_CTRL__DIVISOR0 {1} \
CONFIG.PSU__CRF_APB__GPU_REF_CTRL__FREQMHZ {500} \
CONFIG.PSU__CRF_APB__GPU_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRF_APB__GPU__ENABLE {1} \
CONFIG.PSU__CRF_APB__GTGREF0_REF_CTRL__ACT_FREQMHZ {124.998749} \
CONFIG.PSU__CRF_APB__GTGREF0_REF_CTRL__DIVISOR0 {4} \
CONFIG.PSU__CRF_APB__GTGREF0_REF_CTRL__FREQMHZ {125} \
CONFIG.PSU__CRF_APB__GTGREF0_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRF_APB__GTGREF0__ENABLE {1} \
CONFIG.PSU__CRF_APB__PCIE_REF_CTRL__ACT_FREQMHZ {249.997} \
CONFIG.PSU__CRF_APB__PCIE_REF_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRF_APB__PCIE_REF_CTRL__FREQMHZ {250} \
CONFIG.PSU__CRF_APB__PCIE_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRF_APB__SATA_REF_CTRL__ACT_FREQMHZ {249.997} \
CONFIG.PSU__CRF_APB__SATA_REF_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRF_APB__SATA_REF_CTRL__FREQMHZ {250} \
CONFIG.PSU__CRF_APB__SATA_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRF_APB__TOPSW_LSBUS_CTRL__ACT_FREQMHZ {99.998999} \
CONFIG.PSU__CRF_APB__TOPSW_LSBUS_CTRL__DIVISOR0 {5} \
CONFIG.PSU__CRF_APB__TOPSW_LSBUS_CTRL__FREQMHZ {100} \
CONFIG.PSU__CRF_APB__TOPSW_LSBUS_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRF_APB__TOPSW_LSBUS__ENABLE {1} \
CONFIG.PSU__CRF_APB__TOPSW_MAIN_CTRL__ACT_FREQMHZ {479.228546} \
CONFIG.PSU__CRF_APB__TOPSW_MAIN_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRF_APB__TOPSW_MAIN_CTRL__FREQMHZ {533.33} \
CONFIG.PSU__CRF_APB__TOPSW_MAIN_CTRL__SRCSEL {VPLL} \
CONFIG.PSU__CRF_APB__TOPSW_MAIN__ENABLE {1} \
CONFIG.PSU__CRF_APB__VPLL_CTRL__DIV2 {1} \
CONFIG.PSU__CRF_APB__VPLL_CTRL__FBDIV {57} \
CONFIG.PSU__CRF_APB__VPLL_CTRL__FRACDATA {0.508} \
CONFIG.PSU__CRF_APB__VPLL_CTRL__FRACFREQ {27.138} \
CONFIG.PSU__CRF_APB__VPLL_CTRL__SRCSEL {PSS_REF_CLK} \
CONFIG.PSU__CRF_APB__VPLL_FRAC_CFG__ENABLED {1} \
CONFIG.PSU__CRF_APB__VPLL_TO_LPD_CTRL__DIVISOR0 {3} \
CONFIG.PSU__CRF_APB__VPLL_TO_LPD_CTRL__SRCSEL {VPLL} \
CONFIG.PSU__CRL_APB__ADMA_REF_CTRL__ACT_FREQMHZ {499.994995} \
CONFIG.PSU__CRL_APB__ADMA_REF_CTRL__DIVISOR0 {3} \
CONFIG.PSU__CRL_APB__ADMA_REF_CTRL__FREQMHZ {500} \
CONFIG.PSU__CRL_APB__ADMA_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__ADMA__ENABLE {1} \
CONFIG.PSU__CRL_APB__AFI6_REF_CTRL__ACT_FREQMHZ {499.995} \
CONFIG.PSU__CRL_APB__AFI6_REF_CTRL__DIVISOR0 {2} \
CONFIG.PSU__CRL_APB__AFI6_REF_CTRL__FREQMHZ {500} \
CONFIG.PSU__CRL_APB__AFI6_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__AFI6__ENABLE {0} \
CONFIG.PSU__CRL_APB__AMS_REF_CTRL__ACT_FREQMHZ {50.000} \
CONFIG.PSU__CRL_APB__AMS_REF_CTRL__DIVISOR0 {20} \
CONFIG.PSU__CRL_APB__AMS_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__AMS_REF_CTRL__FREQMHZ {50} \
CONFIG.PSU__CRL_APB__AMS_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__AMS__ENABLE {0} \
CONFIG.PSU__CRL_APB__CAN0_REF_CTRL__ACT_FREQMHZ {99.999} \
CONFIG.PSU__CRL_APB__CAN0_REF_CTRL__DIVISOR0 {15} \
CONFIG.PSU__CRL_APB__CAN0_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__CAN0_REF_CTRL__FREQMHZ {100} \
CONFIG.PSU__CRL_APB__CAN0_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__CAN1_REF_CTRL__ACT_FREQMHZ {99.999} \
CONFIG.PSU__CRL_APB__CAN1_REF_CTRL__DIVISOR0 {15} \
CONFIG.PSU__CRL_APB__CAN1_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__CAN1_REF_CTRL__FREQMHZ {100} \
CONFIG.PSU__CRL_APB__CAN1_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__CPU_R5_CTRL__ACT_FREQMHZ {499.994995} \
CONFIG.PSU__CRL_APB__CPU_R5_CTRL__DIVISOR0 {3} \
CONFIG.PSU__CRL_APB__CPU_R5_CTRL__FREQMHZ {500} \
CONFIG.PSU__CRL_APB__CPU_R5_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__CSU_PLL_CTRL__ACT_FREQMHZ {399.996012} \
CONFIG.PSU__CRL_APB__CSU_PLL_CTRL__DIVISOR0 {3} \
CONFIG.PSU__CRL_APB__CSU_PLL_CTRL__FREQMHZ {500} \
CONFIG.PSU__CRL_APB__CSU_PLL_CTRL__SRCSEL {RPLL} \
CONFIG.PSU__CRL_APB__DBG_LPD_CTRL__ACT_FREQMHZ {249.997498} \
CONFIG.PSU__CRL_APB__DBG_LPD_CTRL__DIVISOR0 {6} \
CONFIG.PSU__CRL_APB__DBG_LPD_CTRL__FREQMHZ {250} \
CONFIG.PSU__CRL_APB__DBG_LPD_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__DBG_LPD__ENABLE {1} \
CONFIG.PSU__CRL_APB__DEBUG_R5_ATCLK_CTRL__ACT_FREQMHZ {1000} \
CONFIG.PSU__CRL_APB__DEBUG_R5_ATCLK_CTRL__DIVISOR0 {6} \
CONFIG.PSU__CRL_APB__DEBUG_R5_ATCLK_CTRL__FREQMHZ {1000} \
CONFIG.PSU__CRL_APB__DEBUG_R5_ATCLK_CTRL__SRCSEL {RPLL} \
CONFIG.PSU__CRL_APB__DLL_REF_CTRL__ACT_FREQMHZ {1500} \
CONFIG.PSU__CRL_APB__DLL_REF_CTRL__FREQMHZ {1500} \
CONFIG.PSU__CRL_APB__DLL_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__GEM0_REF_CTRL__ACT_FREQMHZ {124.999} \
CONFIG.PSU__CRL_APB__GEM0_REF_CTRL__DIVISOR0 {12} \
CONFIG.PSU__CRL_APB__GEM0_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__GEM0_REF_CTRL__FREQMHZ {125} \
CONFIG.PSU__CRL_APB__GEM0_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__GEM1_REF_CTRL__ACT_FREQMHZ {124.999} \
CONFIG.PSU__CRL_APB__GEM1_REF_CTRL__DIVISOR0 {12} \
CONFIG.PSU__CRL_APB__GEM1_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__GEM1_REF_CTRL__FREQMHZ {125} \
CONFIG.PSU__CRL_APB__GEM1_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__GEM2_REF_CTRL__ACT_FREQMHZ {124.999} \
CONFIG.PSU__CRL_APB__GEM2_REF_CTRL__DIVISOR0 {12} \
CONFIG.PSU__CRL_APB__GEM2_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__GEM2_REF_CTRL__FREQMHZ {125} \
CONFIG.PSU__CRL_APB__GEM2_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__GEM3_REF_CTRL__ACT_FREQMHZ {124.999} \
CONFIG.PSU__CRL_APB__GEM3_REF_CTRL__DIVISOR0 {12} \
CONFIG.PSU__CRL_APB__GEM3_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__GEM3_REF_CTRL__FREQMHZ {125} \
CONFIG.PSU__CRL_APB__GEM3_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__GEM_TSU_REF_CTRL__ACT_FREQMHZ {216.664} \
CONFIG.PSU__CRL_APB__GEM_TSU_REF_CTRL__DIVISOR0 {3} \
CONFIG.PSU__CRL_APB__GEM_TSU_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__GEM_TSU_REF_CTRL__FREQMHZ {250} \
CONFIG.PSU__CRL_APB__GEM_TSU_REF_CTRL__SRCSEL {RPLL} \
CONFIG.PSU__CRL_APB__I2C0_REF_CTRL__ACT_FREQMHZ {99.999} \
CONFIG.PSU__CRL_APB__I2C0_REF_CTRL__DIVISOR0 {15} \
CONFIG.PSU__CRL_APB__I2C0_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__I2C0_REF_CTRL__FREQMHZ {100} \
CONFIG.PSU__CRL_APB__I2C0_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__I2C1_REF_CTRL__ACT_FREQMHZ {99.998999} \
CONFIG.PSU__CRL_APB__I2C1_REF_CTRL__DIVISOR0 {15} \
CONFIG.PSU__CRL_APB__I2C1_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__I2C1_REF_CTRL__FREQMHZ {100} \
CONFIG.PSU__CRL_APB__I2C1_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__IOPLL_CTRL__DIV2 {0} \
CONFIG.PSU__CRL_APB__IOPLL_CTRL__FBDIV {45} \
CONFIG.PSU__CRL_APB__IOPLL_CTRL__FRACDATA {0.000} \
CONFIG.PSU__CRL_APB__IOPLL_CTRL__FRACFREQ {27.138} \
CONFIG.PSU__CRL_APB__IOPLL_CTRL__SRCSEL {PSS_REF_CLK} \
CONFIG.PSU__CRL_APB__IOPLL_FRAC_CFG__ENABLED {0} \
CONFIG.PSU__CRL_APB__IOPLL_TO_FPD_CTRL__DIVISOR0 {3} \
CONFIG.PSU__CRL_APB__IOPLL_TO_FPD_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__IOU_SWITCH_CTRL__ACT_FREQMHZ {249.997498} \
CONFIG.PSU__CRL_APB__IOU_SWITCH_CTRL__DIVISOR0 {6} \
CONFIG.PSU__CRL_APB__IOU_SWITCH_CTRL__FREQMHZ {267} \
CONFIG.PSU__CRL_APB__IOU_SWITCH_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__IOU_SWITCH__ENABLE {1} \
CONFIG.PSU__CRL_APB__LPD_LSBUS_CTRL__ACT_FREQMHZ {99.998999} \
CONFIG.PSU__CRL_APB__LPD_LSBUS_CTRL__DIVISOR0 {15} \
CONFIG.PSU__CRL_APB__LPD_LSBUS_CTRL__FREQMHZ {100} \
CONFIG.PSU__CRL_APB__LPD_LSBUS_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__LPD_LSBUS__ENABLE {1} \
CONFIG.PSU__CRL_APB__LPD_SWITCH_CTRL__ACT_FREQMHZ {499.994995} \
CONFIG.PSU__CRL_APB__LPD_SWITCH_CTRL__DIVISOR0 {3} \
CONFIG.PSU__CRL_APB__LPD_SWITCH_CTRL__FREQMHZ {500} \
CONFIG.PSU__CRL_APB__LPD_SWITCH_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__LPD_SWITCH__ENABLE {1} \
CONFIG.PSU__CRL_APB__NAND_REF_CTRL__ACT_FREQMHZ {99.999} \
CONFIG.PSU__CRL_APB__NAND_REF_CTRL__DIVISOR0 {15} \
CONFIG.PSU__CRL_APB__NAND_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__NAND_REF_CTRL__FREQMHZ {100} \
CONFIG.PSU__CRL_APB__NAND_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__OCM_MAIN_CTRL__ACT_FREQMHZ {500} \
CONFIG.PSU__CRL_APB__OCM_MAIN_CTRL__DIVISOR0 {3} \
CONFIG.PSU__CRL_APB__OCM_MAIN_CTRL__FREQMHZ {500} \
CONFIG.PSU__CRL_APB__OCM_MAIN_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__OCM_MAIN__ENABLE {1} \
CONFIG.PSU__CRL_APB__PCAP_CTRL__ACT_FREQMHZ {199.998006} \
CONFIG.PSU__CRL_APB__PCAP_CTRL__DIVISOR0 {6} \
CONFIG.PSU__CRL_APB__PCAP_CTRL__FREQMHZ {250} \
CONFIG.PSU__CRL_APB__PCAP_CTRL__SRCSEL {RPLL} \
CONFIG.PSU__CRL_APB__PCAP__ENABLE {1} \
CONFIG.PSU__CRL_APB__PICDEBUG_CTRL__ACT_FREQMHZ {1300} \
CONFIG.PSU__CRL_APB__PICDEBUG_CTRL__DIVISOR0 {32} \
CONFIG.PSU__CRL_APB__PICDEBUG_CTRL__FREQMHZ {1300} \
CONFIG.PSU__CRL_APB__PICDEBUG_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__PICDEBUG_REF_CTRL__ACT_FREQMHZ {1300} \
CONFIG.PSU__CRL_APB__PICDEBUG_REF_CTRL__DIVISOR0 {1} \
CONFIG.PSU__CRL_APB__PICDEBUG_REF_CTRL__FREQMHZ {1300} \
CONFIG.PSU__CRL_APB__PICDEBUG_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__PICDEBUG_TEMP_CTRL__ACT_FREQMHZ {300} \
CONFIG.PSU__CRL_APB__PICDEBUG_TEMP_CTRL__DIVISOR0 {1} \
CONFIG.PSU__CRL_APB__PICDEBUG_TEMP_CTRL__FREQMHZ {300} \
CONFIG.PSU__CRL_APB__PICDEBUG_TEMP_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__PL0_REF_CTRL__ACT_FREQMHZ {99.998999} \
CONFIG.PSU__CRL_APB__PL0_REF_CTRL__DIVISOR0 {15} \
CONFIG.PSU__CRL_APB__PL0_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__PL0_REF_CTRL__FREQMHZ {100} \
CONFIG.PSU__CRL_APB__PL0_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__PL1_REF_CTRL__ACT_FREQMHZ {299.997009} \
CONFIG.PSU__CRL_APB__PL1_REF_CTRL__DIVISOR0 {4} \
CONFIG.PSU__CRL_APB__PL1_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__PL1_REF_CTRL__FREQMHZ {200} \
CONFIG.PSU__CRL_APB__PL1_REF_CTRL__SRCSEL {RPLL} \
CONFIG.PSU__CRL_APB__PL2_REF_CTRL__ACT_FREQMHZ {324.997} \
CONFIG.PSU__CRL_APB__PL2_REF_CTRL__DIVISOR0 {4} \
CONFIG.PSU__CRL_APB__PL2_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__PL2_REF_CTRL__FREQMHZ {400} \
CONFIG.PSU__CRL_APB__PL2_REF_CTRL__SRCSEL {RPLL} \
CONFIG.PSU__CRL_APB__PL3_REF_CTRL__ACT_FREQMHZ {324.997} \
CONFIG.PSU__CRL_APB__PL3_REF_CTRL__DIVISOR0 {4} \
CONFIG.PSU__CRL_APB__PL3_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__PL3_REF_CTRL__FREQMHZ {400} \
CONFIG.PSU__CRL_APB__PL3_REF_CTRL__SRCSEL {RPLL} \
CONFIG.PSU__CRL_APB__QSPI_REF_CTRL__ACT_FREQMHZ {124.998749} \
CONFIG.PSU__CRL_APB__QSPI_REF_CTRL__DIVISOR0 {12} \
CONFIG.PSU__CRL_APB__QSPI_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__QSPI_REF_CTRL__FREQMHZ {300} \
CONFIG.PSU__CRL_APB__QSPI_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__RPLL_CTRL__DIV2 {1} \
CONFIG.PSU__CRL_APB__RPLL_CTRL__FBDIV {72} \
CONFIG.PSU__CRL_APB__RPLL_CTRL__FRACDATA {0.000} \
CONFIG.PSU__CRL_APB__RPLL_CTRL__FRACFREQ {27.138} \
CONFIG.PSU__CRL_APB__RPLL_CTRL__SRCSEL {PSS_REF_CLK} \
CONFIG.PSU__CRL_APB__RPLL_FRAC_CFG__ENABLED {0} \
CONFIG.PSU__CRL_APB__RPLL_TO_FPD_CTRL__DIVISOR0 {3} \
CONFIG.PSU__CRL_APB__RPLL_TO_FPD_CTRL__SRCSEL {RPLL} \
CONFIG.PSU__CRL_APB__SDIO0_REF_CTRL__ACT_FREQMHZ {185.712} \
CONFIG.PSU__CRL_APB__SDIO0_REF_CTRL__DIVISOR0 {6} \
CONFIG.PSU__CRL_APB__SDIO0_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__SDIO0_REF_CTRL__FREQMHZ {200} \
CONFIG.PSU__CRL_APB__SDIO0_REF_CTRL__SRCSEL {RPLL} \
CONFIG.PSU__CRL_APB__SDIO1_REF_CTRL__ACT_FREQMHZ {199.998006} \
CONFIG.PSU__CRL_APB__SDIO1_REF_CTRL__DIVISOR0 {6} \
CONFIG.PSU__CRL_APB__SDIO1_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__SDIO1_REF_CTRL__FREQMHZ {200} \
CONFIG.PSU__CRL_APB__SDIO1_REF_CTRL__SRCSEL {RPLL} \
CONFIG.PSU__CRL_APB__SPI0_REF_CTRL__ACT_FREQMHZ {185.712} \
CONFIG.PSU__CRL_APB__SPI0_REF_CTRL__DIVISOR0 {6} \
CONFIG.PSU__CRL_APB__SPI0_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__SPI0_REF_CTRL__FREQMHZ {100} \
CONFIG.PSU__CRL_APB__SPI0_REF_CTRL__SRCSEL {RPLL} \
CONFIG.PSU__CRL_APB__SPI1_REF_CTRL__ACT_FREQMHZ {199.998006} \
CONFIG.PSU__CRL_APB__SPI1_REF_CTRL__DIVISOR0 {6} \
CONFIG.PSU__CRL_APB__SPI1_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__SPI1_REF_CTRL__FREQMHZ {100} \
CONFIG.PSU__CRL_APB__SPI1_REF_CTRL__SRCSEL {RPLL} \
CONFIG.PSU__CRL_APB__TIMESTAMP_REF_CTRL__ACT_FREQMHZ {99.998999} \
CONFIG.PSU__CRL_APB__TIMESTAMP_REF_CTRL__DIVISOR0 {15} \
CONFIG.PSU__CRL_APB__TIMESTAMP_REF_CTRL__FREQMHZ {100} \
CONFIG.PSU__CRL_APB__TIMESTAMP_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__TIMESTAMP__ENABLE {1} \
CONFIG.PSU__CRL_APB__UART0_REF_CTRL__ACT_FREQMHZ {99.998999} \
CONFIG.PSU__CRL_APB__UART0_REF_CTRL__DIVISOR0 {15} \
CONFIG.PSU__CRL_APB__UART0_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__UART0_REF_CTRL__FREQMHZ {100} \
CONFIG.PSU__CRL_APB__UART0_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__UART1_REF_CTRL__ACT_FREQMHZ {99.998999} \
CONFIG.PSU__CRL_APB__UART1_REF_CTRL__DIVISOR0 {15} \
CONFIG.PSU__CRL_APB__UART1_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__UART1_REF_CTRL__FREQMHZ {100} \
CONFIG.PSU__CRL_APB__UART1_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__USB0_BUS_REF_CTRL__ACT_FREQMHZ {249.997498} \
CONFIG.PSU__CRL_APB__USB0_BUS_REF_CTRL__DIVISOR0 {6} \
CONFIG.PSU__CRL_APB__USB0_BUS_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__USB0_BUS_REF_CTRL__FREQMHZ {250} \
CONFIG.PSU__CRL_APB__USB0_BUS_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__USB1_BUS_REF_CTRL__ACT_FREQMHZ {249.997498} \
CONFIG.PSU__CRL_APB__USB1_BUS_REF_CTRL__DIVISOR0 {6} \
CONFIG.PSU__CRL_APB__USB1_BUS_REF_CTRL__DIVISOR1 {1} \
CONFIG.PSU__CRL_APB__USB1_BUS_REF_CTRL__FREQMHZ {250} \
CONFIG.PSU__CRL_APB__USB1_BUS_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__USB3_DUAL_REF_CTRL__ACT_FREQMHZ {19.999800} \
CONFIG.PSU__CRL_APB__USB3_DUAL_REF_CTRL__DIVISOR0 {5} \
CONFIG.PSU__CRL_APB__USB3_DUAL_REF_CTRL__DIVISOR1 {15} \
CONFIG.PSU__CRL_APB__USB3_DUAL_REF_CTRL__FREQMHZ {20} \
CONFIG.PSU__CRL_APB__USB3_DUAL_REF_CTRL__SRCSEL {IOPLL} \
CONFIG.PSU__CRL_APB__USB3__ENABLE {1} \
CONFIG.PSU__CRYSTAL__PERIPHERAL__FREQMHZ {33.333} \
CONFIG.PSU__CSU_COHERENCY {1} \
CONFIG.PSU__CSU__CSU_TAMPER_0__ENABLE {0} \
CONFIG.PSU__CSU__CSU_TAMPER_0__ERASE_BBRAM {0} \
CONFIG.PSU__CSU__CSU_TAMPER_0__RESPONSE {<Select>} \
CONFIG.PSU__CSU__CSU_TAMPER_10__ENABLE {0} \
CONFIG.PSU__CSU__CSU_TAMPER_10__ERASE_BBRAM {0} \
CONFIG.PSU__CSU__CSU_TAMPER_10__RESPONSE {<Select>} \
CONFIG.PSU__CSU__CSU_TAMPER_11__ENABLE {0} \
CONFIG.PSU__CSU__CSU_TAMPER_11__ERASE_BBRAM {0} \
CONFIG.PSU__CSU__CSU_TAMPER_11__RESPONSE {<Select>} \
CONFIG.PSU__CSU__CSU_TAMPER_12__ENABLE {0} \
CONFIG.PSU__CSU__CSU_TAMPER_12__ERASE_BBRAM {0} \
CONFIG.PSU__CSU__CSU_TAMPER_12__RESPONSE {<Select>} \
CONFIG.PSU__CSU__CSU_TAMPER_1__ENABLE {0} \
CONFIG.PSU__CSU__CSU_TAMPER_1__ERASE_BBRAM {0} \
CONFIG.PSU__CSU__CSU_TAMPER_1__RESPONSE {<Select>} \
CONFIG.PSU__CSU__CSU_TAMPER_2__ENABLE {0} \
CONFIG.PSU__CSU__CSU_TAMPER_2__ERASE_BBRAM {0} \
CONFIG.PSU__CSU__CSU_TAMPER_2__RESPONSE {<Select>} \
CONFIG.PSU__CSU__CSU_TAMPER_3__ENABLE {0} \
CONFIG.PSU__CSU__CSU_TAMPER_3__ERASE_BBRAM {0} \
CONFIG.PSU__CSU__CSU_TAMPER_3__RESPONSE {<Select>} \
CONFIG.PSU__CSU__CSU_TAMPER_4__ENABLE {0} \
CONFIG.PSU__CSU__CSU_TAMPER_4__ERASE_BBRAM {0} \
CONFIG.PSU__CSU__CSU_TAMPER_4__RESPONSE {<Select>} \
CONFIG.PSU__CSU__CSU_TAMPER_5__ENABLE {0} \
CONFIG.PSU__CSU__CSU_TAMPER_5__ERASE_BBRAM {0} \
CONFIG.PSU__CSU__CSU_TAMPER_5__RESPONSE {<Select>} \
CONFIG.PSU__CSU__CSU_TAMPER_6__ENABLE {0} \
CONFIG.PSU__CSU__CSU_TAMPER_6__ERASE_BBRAM {0} \
CONFIG.PSU__CSU__CSU_TAMPER_6__RESPONSE {<Select>} \
CONFIG.PSU__CSU__CSU_TAMPER_7__ENABLE {0} \
CONFIG.PSU__CSU__CSU_TAMPER_7__ERASE_BBRAM {0} \
CONFIG.PSU__CSU__CSU_TAMPER_7__RESPONSE {<Select>} \
CONFIG.PSU__CSU__CSU_TAMPER_8__ENABLE {0} \
CONFIG.PSU__CSU__CSU_TAMPER_8__ERASE_BBRAM {0} \
CONFIG.PSU__CSU__CSU_TAMPER_8__RESPONSE {<Select>} \
CONFIG.PSU__CSU__CSU_TAMPER_9__ENABLE {0} \
CONFIG.PSU__CSU__CSU_TAMPER_9__ERASE_BBRAM {0} \
CONFIG.PSU__CSU__CSU_TAMPER_9__RESPONSE {<Select>} \
CONFIG.PSU__CSU__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__CSU__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__DAP_COHERENCY {1} \
CONFIG.PSU__DDRC__ADDR_MIRROR {NA} \
CONFIG.PSU__DDRC__AL {0} \
CONFIG.PSU__DDRC__BANK_ADDR_COUNT {2} \
CONFIG.PSU__DDRC__BG_ADDR_COUNT {1} \
CONFIG.PSU__DDRC__BL {8} \
CONFIG.PSU__DDRC__BOARD_DELAY0 {1} \
CONFIG.PSU__DDRC__BOARD_DELAY1 {1} \
CONFIG.PSU__DDRC__BOARD_DELAY2 {1} \
CONFIG.PSU__DDRC__BOARD_DELAY3 {1} \
CONFIG.PSU__DDRC__BRC_MAPPING {0} \
CONFIG.PSU__DDRC__BUS_WIDTH {64 Bit} \
CONFIG.PSU__DDRC__CL {16} \
CONFIG.PSU__DDRC__CLOCK_0_LENGTH_MM {1} \
CONFIG.PSU__DDRC__CLOCK_0_PACKAGE_LENGTH {1} \
CONFIG.PSU__DDRC__CLOCK_0_PROPOGATION_DELAY {1} \
CONFIG.PSU__DDRC__CLOCK_1_LENGTH_MM {1} \
CONFIG.PSU__DDRC__CLOCK_1_PACKAGE_LENGTH {1} \
CONFIG.PSU__DDRC__CLOCK_1_PROPOGATION_DELAY {1} \
CONFIG.PSU__DDRC__CLOCK_2_LENGTH_MM {1} \
CONFIG.PSU__DDRC__CLOCK_2_PACKAGE_LENGTH {1} \
CONFIG.PSU__DDRC__CLOCK_2_PROPOGATION_DELAY {1} \
CONFIG.PSU__DDRC__CLOCK_3_LENGTH_MM {1} \
CONFIG.PSU__DDRC__CLOCK_3_PACKAGE_LENGTH {1} \
CONFIG.PSU__DDRC__CLOCK_3_PROPOGATION_DELAY {1} \
CONFIG.PSU__DDRC__CLOCK_STOP_EN {0} \
CONFIG.PSU__DDRC__COL_ADDR_COUNT {10} \
CONFIG.PSU__DDRC__CWL {16} \
CONFIG.PSU__DDRC__DATA_MASK {1} \
CONFIG.PSU__DDRC__DDR4_ADDR_MAPPING {0} \
CONFIG.PSU__DDRC__DDR4_CAL_MODE_ENABLE {0} \
CONFIG.PSU__DDRC__DDR4_CRC_CONTROL {0} \
CONFIG.PSU__DDRC__DDR4_MAXPWR_SAVING_EN {0} \
CONFIG.PSU__DDRC__DDR4_T_REF_MODE {0} \
CONFIG.PSU__DDRC__DDR4_T_REF_RANGE {0} \
CONFIG.PSU__DDRC__DEEP_PWR_DOWN_EN {0} \
CONFIG.PSU__DDRC__DERATE_INT_D {<Select>} \
CONFIG.PSU__DDRC__DEVICE_CAPACITY {8192 MBits} \
CONFIG.PSU__DDRC__DIMM_ADDR_MIRROR {0} \
CONFIG.PSU__DDRC__DQS_0_LENGTH_MM {1} \
CONFIG.PSU__DDRC__DQS_0_PACKAGE_LENGTH {1} \
CONFIG.PSU__DDRC__DQS_0_PROPOGATION_DELAY {1} \
CONFIG.PSU__DDRC__DQS_1_LENGTH_MM {1} \
CONFIG.PSU__DDRC__DQS_1_PACKAGE_LENGTH {1} \
CONFIG.PSU__DDRC__DQS_1_PROPOGATION_DELAY {1} \
CONFIG.PSU__DDRC__DQS_2_LENGTH_MM {1} \
CONFIG.PSU__DDRC__DQS_2_PACKAGE_LENGTH {1} \
CONFIG.PSU__DDRC__DQS_2_PROPOGATION_DELAY {1} \
CONFIG.PSU__DDRC__DQS_3_LENGTH_MM {1} \
CONFIG.PSU__DDRC__DQS_3_PACKAGE_LENGTH {1} \
CONFIG.PSU__DDRC__DQS_3_PROPOGATION_DELAY {1} \
CONFIG.PSU__DDRC__DQS_TO_CLK_DELAY_0 {0.00} \
CONFIG.PSU__DDRC__DQS_TO_CLK_DELAY_1 {0.05} \
CONFIG.PSU__DDRC__DQS_TO_CLK_DELAY_2 {0.10} \
CONFIG.PSU__DDRC__DQS_TO_CLK_DELAY_3 {0.15} \
CONFIG.PSU__DDRC__DQ_0_LENGTH_MM {1} \
CONFIG.PSU__DDRC__DQ_0_PACKAGE_LENGTH {1} \
CONFIG.PSU__DDRC__DQ_0_PROPOGATION_DELAY {1} \
CONFIG.PSU__DDRC__DQ_1_LENGTH_MM {1} \
CONFIG.PSU__DDRC__DQ_1_PACKAGE_LENGTH {1} \
CONFIG.PSU__DDRC__DQ_1_PROPOGATION_DELAY {1} \
CONFIG.PSU__DDRC__DQ_2_LENGTH_MM {1} \
CONFIG.PSU__DDRC__DQ_2_PACKAGE_LENGTH {1} \
CONFIG.PSU__DDRC__DQ_2_PROPOGATION_DELAY {1} \
CONFIG.PSU__DDRC__DQ_3_LENGTH_MM {1} \
CONFIG.PSU__DDRC__DQ_3_PACKAGE_LENGTH {1} \
CONFIG.PSU__DDRC__DQ_3_PROPOGATION_DELAY {1} \
CONFIG.PSU__DDRC__DRAM_WIDTH {16 Bits} \
CONFIG.PSU__DDRC__ECC {0} \
CONFIG.PSU__DDRC__ECC_SCRUB {0} \
CONFIG.PSU__DDRC__ENABLE {1} \
CONFIG.PSU__DDRC__EN_2ND_CLK {0} \
CONFIG.PSU__DDRC__FGRM {0} \
CONFIG.PSU__DDRC__FREQ_MHZ {1066.50} \
CONFIG.PSU__DDRC__HIGH_TEMP {<Select>} \
CONFIG.PSU__DDRC__LP_ASR {0} \
CONFIG.PSU__DDRC__MEMORY_TYPE {DDR 4} \
CONFIG.PSU__DDRC__PARITY_ENABLE {0} \
CONFIG.PSU__DDRC__PARTNO {<Select>} \
CONFIG.PSU__DDRC__PER_BANK_REFRESH {0} \
CONFIG.PSU__DDRC__PHY_DBI_MODE {0} \
CONFIG.PSU__DDRC__PLL_BYPASS {0} \
CONFIG.PSU__DDRC__PWR_DOWN_EN {0} \
CONFIG.PSU__DDRC__RANK_ADDR_COUNT {0} \
CONFIG.PSU__DDRC__RDIMM_INDICATOR {0} \
CONFIG.PSU__DDRC__RD_DBI_ENABLE {0} \
CONFIG.PSU__DDRC__ROW_ADDR_COUNT {16} \
CONFIG.PSU__DDRC__SELF_REF_ABORT {0} \
CONFIG.PSU__DDRC__SPEED_BIN {DDR4_2400P} \
CONFIG.PSU__DDRC__STATIC_RD_MODE {0} \
CONFIG.PSU__DDRC__TRAIN_DATA_EYE {1} \
CONFIG.PSU__DDRC__TRAIN_READ_GATE {1} \
CONFIG.PSU__DDRC__TRAIN_WRITE_LEVEL {1} \
CONFIG.PSU__DDRC__T_FAW {21.0} \
CONFIG.PSU__DDRC__T_RAS_MIN {32} \
CONFIG.PSU__DDRC__T_RC {45.32} \
CONFIG.PSU__DDRC__T_RCD {16} \
CONFIG.PSU__DDRC__T_RP {16} \
CONFIG.PSU__DDRC__UDIMM_INDICATOR {0} \
CONFIG.PSU__DDRC__USE_INTERNAL_VREF {0} \
CONFIG.PSU__DDRC__VIDEO_BUFFER_SIZE {0} \
CONFIG.PSU__DDRC__VREF {1} \
CONFIG.PSU__DDRC__WR_DBI_ENABLE {0} \
CONFIG.PSU__DDR_HIGH_ADDRESS_GUI_ENABLE {1} \
CONFIG.PSU__DDR__INTERFACE__FREQMHZ {600.000} \
CONFIG.PSU__DISPLAYPORT__LANE0__ENABLE {<Select>} \
CONFIG.PSU__DISPLAYPORT__LANE0__IO {<Select>} \
CONFIG.PSU__DISPLAYPORT__LANE1__ENABLE {<Select>} \
CONFIG.PSU__DISPLAYPORT__LANE1__IO {<Select>} \
CONFIG.PSU__DISPLAYPORT__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__DPAUX__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__DPAUX__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__DP__LANE_SEL {<Select>} \
CONFIG.PSU__ENET0__GRP_MDIO__ENABLE {0} \
CONFIG.PSU__ENET0__GRP_MDIO__IO {<Select>} \
CONFIG.PSU__ENET0__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__ENET0__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__ENET1__GRP_MDIO__ENABLE {0} \
CONFIG.PSU__ENET1__GRP_MDIO__IO {<Select>} \
CONFIG.PSU__ENET1__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__ENET1__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__ENET2__GRP_MDIO__ENABLE {0} \
CONFIG.PSU__ENET2__GRP_MDIO__IO {<Select>} \
CONFIG.PSU__ENET2__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__ENET2__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__ENET3__GRP_MDIO__ENABLE {0} \
CONFIG.PSU__ENET3__GRP_MDIO__IO {<Select>} \
CONFIG.PSU__ENET3__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__ENET3__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__FPGA_PL0_ENABLE {1} \
CONFIG.PSU__FPGA_PL1_ENABLE {1} \
CONFIG.PSU__FP__POWER__ON {1} \
CONFIG.PSU__GEM0_COHERENCY {1} \
CONFIG.PSU__GEM1_COHERENCY {0} \
CONFIG.PSU__GEM2_COHERENCY {1} \
CONFIG.PSU__GEM3_COHERENCY {1} \
CONFIG.PSU__GEM__TSU__ENABLE {0} \
CONFIG.PSU__GEM__TSU__IO {<Select>} \
CONFIG.PSU__GENERATE_SECURITY_REGISTERS {0} \
CONFIG.PSU__GEN_IPI_0__MASTER {APU} \
CONFIG.PSU__GEN_IPI_10__MASTER {S_AXI_LPD} \
CONFIG.PSU__GEN_IPI_1__MASTER {RPU} \
CONFIG.PSU__GEN_IPI_2__MASTER {RPU} \
CONFIG.PSU__GEN_IPI_3__MASTER {PMU} \
CONFIG.PSU__GEN_IPI_4__MASTER {PMU} \
CONFIG.PSU__GEN_IPI_5__MASTER {PMU} \
CONFIG.PSU__GEN_IPI_6__MASTER {PMU} \
CONFIG.PSU__GEN_IPI_7__MASTER {S_AXI_HP1_FPD} \
CONFIG.PSU__GEN_IPI_8__MASTER {S_AXI_HP2_FPD} \
CONFIG.PSU__GEN_IPI_9__MASTER {S_AXI_HP3_FPD} \
CONFIG.PSU__GPIO0_MIO__IO {<Select>} \
CONFIG.PSU__GPIO0_MIO__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__GPIO1_MIO__IO {<Select>} \
CONFIG.PSU__GPIO1_MIO__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__GPIO2_MIO__IO {<Select>} \
CONFIG.PSU__GPIO2_MIO__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__GPIO_EMIO__PERIPHERAL__ENABLE {1} \
CONFIG.PSU__GPIO_EMIO__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__GPIO_EMIO__WIDTH {[94:0]} \
CONFIG.PSU__GPU_PP0__POWER__ON {1} \
CONFIG.PSU__GPU_PP1__POWER__ON {1} \
CONFIG.PSU__GT_REF_CLK__FREQMHZ {33.333} \
CONFIG.PSU__GT__LANE0_REF_SEL {Ref Clk0} \
CONFIG.PSU__GT__LANE1_REF_SEL {Ref Clk0} \
CONFIG.PSU__GT__LANE2_REF_SEL {Ref Clk2} \
CONFIG.PSU__GT__LANE3_REF_SEL {Ref Clk1} \
CONFIG.PSU__GT__LINK_SPEED {<Select>} \
CONFIG.PSU__GT__REF_SEL0 {100} \
CONFIG.PSU__GT__REF_SEL1 {100} \
CONFIG.PSU__GT__REF_SEL2 {100} \
CONFIG.PSU__GT__REF_SEL3 {125} \
CONFIG.PSU__I2C0_LOOP_I2C1__ENABLE {0} \
CONFIG.PSU__I2C0__GRP_INT__ENABLE {0} \
CONFIG.PSU__I2C0__GRP_INT__IO {<Select>} \
CONFIG.PSU__I2C0__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__I2C0__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__I2C1__GRP_INT__ENABLE {0} \
CONFIG.PSU__I2C1__GRP_INT__IO {<Select>} \
CONFIG.PSU__I2C1__PERIPHERAL__ENABLE {1} \
CONFIG.PSU__I2C1__PERIPHERAL__IO {MIO 16 .. 17} \
CONFIG.PSU__L2_BANK0__POWER__ON {1} \
CONFIG.PSU__MAXIGP2__DATA_WIDTH {32} \
CONFIG.PSU__NAND_COHERENCY {1} \
CONFIG.PSU__NAND__CHIP_ENABLE__ENABLE {0} \
CONFIG.PSU__NAND__CHIP_ENABLE__IO {<Select>} \
CONFIG.PSU__NAND__DATA_STROBE__ENABLE {0} \
CONFIG.PSU__NAND__DATA_STROBE__IO {<Select>} \
CONFIG.PSU__NAND__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__NAND__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__NAND__READY_BUSY__ENABLE {0} \
CONFIG.PSU__NAND__READY_BUSY__IO {<Select>} \
CONFIG.PSU__OCM_BANK0__POWER__ON {1} \
CONFIG.PSU__OCM_BANK1__POWER__ON {1} \
CONFIG.PSU__OCM_BANK2__POWER__ON {1} \
CONFIG.PSU__OCM_BANK3__POWER__ON {1} \
CONFIG.PSU__OVERRIDE__BASIC_CLOCK {1} \
CONFIG.PSU__PCIE__ACS_VIOLATION {0} \
CONFIG.PSU__PCIE__AER_CAPABILITY {0} \
CONFIG.PSU__PCIE__ATOMICOP_EGRESS_BLOCKED {0} \
CONFIG.PSU__PCIE__BAR0_64BIT {0} \
CONFIG.PSU__PCIE__BAR0_ENABLE {<Select>} \
CONFIG.PSU__PCIE__BAR0_PREFETCHABLE {0} \
CONFIG.PSU__PCIE__BAR0_SCALE {<Select>} \
CONFIG.PSU__PCIE__BAR0_SIZE {<Select>} \
CONFIG.PSU__PCIE__BAR0_TYPE {<Select>} \
CONFIG.PSU__PCIE__BAR1_64BIT {0} \
CONFIG.PSU__PCIE__BAR1_ENABLE {<Select>} \
CONFIG.PSU__PCIE__BAR1_PREFETCHABLE {0} \
CONFIG.PSU__PCIE__BAR1_SCALE {<Select>} \
CONFIG.PSU__PCIE__BAR1_SIZE {<Select>} \
CONFIG.PSU__PCIE__BAR1_TYPE {<Select>} \
CONFIG.PSU__PCIE__BAR2_64BIT {0} \
CONFIG.PSU__PCIE__BAR2_ENABLE {<Select>} \
CONFIG.PSU__PCIE__BAR2_PREFETCHABLE {0} \
CONFIG.PSU__PCIE__BAR2_SCALE {<Select>} \
CONFIG.PSU__PCIE__BAR2_SIZE {<Select>} \
CONFIG.PSU__PCIE__BAR2_TYPE {<Select>} \
CONFIG.PSU__PCIE__BAR3_64BIT {0} \
CONFIG.PSU__PCIE__BAR3_ENABLE {<Select>} \
CONFIG.PSU__PCIE__BAR3_PREFETCHABLE {0} \
CONFIG.PSU__PCIE__BAR3_SCALE {<Select>} \
CONFIG.PSU__PCIE__BAR3_SIZE {<Select>} \
CONFIG.PSU__PCIE__BAR3_TYPE {<Select>} \
CONFIG.PSU__PCIE__BAR4_64BIT {0} \
CONFIG.PSU__PCIE__BAR4_ENABLE {<Select>} \
CONFIG.PSU__PCIE__BAR4_PREFETCHABLE {0} \
CONFIG.PSU__PCIE__BAR4_SCALE {<Select>} \
CONFIG.PSU__PCIE__BAR4_SIZE {<Select>} \
CONFIG.PSU__PCIE__BAR4_TYPE {<Select>} \
CONFIG.PSU__PCIE__BAR5_64BIT {0} \
CONFIG.PSU__PCIE__BAR5_ENABLE {<Select>} \
CONFIG.PSU__PCIE__BAR5_PREFETCHABLE {0} \
CONFIG.PSU__PCIE__BAR5_SCALE {<Select>} \
CONFIG.PSU__PCIE__BAR5_SIZE {<Select>} \
CONFIG.PSU__PCIE__BAR5_TYPE {<Select>} \
CONFIG.PSU__PCIE__BASE_CLASS_MENU {<Select>} \
CONFIG.PSU__PCIE__BRIDGE_BAR_INDICATOR {<Select>} \
CONFIG.PSU__PCIE__CAP_SLOT_IMPLEMENTED {<Select>} \
CONFIG.PSU__PCIE__CLASS_CODE_BASE {0x06} \
CONFIG.PSU__PCIE__CLASS_CODE_INTERFACE {0x0} \
CONFIG.PSU__PCIE__CLASS_CODE_SUB {0x4} \
CONFIG.PSU__PCIE__COMPLETER_ABORT {0} \
CONFIG.PSU__PCIE__COMPLTION_TIMEOUT {0} \
CONFIG.PSU__PCIE__CORRECTABLE_INT_ERR {0} \
CONFIG.PSU__PCIE__CRS_SW_VISIBILITY {0} \
CONFIG.PSU__PCIE__DEVICE_ID {0xD021} \
CONFIG.PSU__PCIE__DEVICE_PORT_TYPE {<Select>} \
CONFIG.PSU__PCIE__ECRC_CHECK {0} \
CONFIG.PSU__PCIE__ECRC_ERR {0} \
CONFIG.PSU__PCIE__ECRC_GEN {0} \
CONFIG.PSU__PCIE__EROM_ENABLE {0} \
CONFIG.PSU__PCIE__EROM_SCALE {<Select>} \
CONFIG.PSU__PCIE__EROM_SIZE {<Select>} \
CONFIG.PSU__PCIE__FLOW_CONTROL_ERR {0} \
CONFIG.PSU__PCIE__FLOW_CONTROL_PROTOCOL_ERR {0} \
CONFIG.PSU__PCIE__HEADER_LOG_OVERFLOW {0} \
CONFIG.PSU__PCIE__INTERFACE_WIDTH {<Select>} \
CONFIG.PSU__PCIE__INTX_GENERATION {0} \
CONFIG.PSU__PCIE__INTX_PIN {<Select>} \
CONFIG.PSU__PCIE__LANE0__ENABLE {<Select>} \
CONFIG.PSU__PCIE__LANE0__IO {<Select>} \
CONFIG.PSU__PCIE__LANE1__ENABLE {<Select>} \
CONFIG.PSU__PCIE__LANE1__IO {<Select>} \
CONFIG.PSU__PCIE__LANE2__ENABLE {<Select>} \
CONFIG.PSU__PCIE__LANE2__IO {<Select>} \
CONFIG.PSU__PCIE__LANE3__ENABLE {<Select>} \
CONFIG.PSU__PCIE__LANE3__IO {<Select>} \
CONFIG.PSU__PCIE__LEGACY_INTERRUPT {<Select>} \
CONFIG.PSU__PCIE__LINK_SPEED {<Select>} \
CONFIG.PSU__PCIE__MAXIMUM_LINK_WIDTH {<Select>} \
CONFIG.PSU__PCIE__MAX_PAYLOAD_SIZE {<Select>} \
CONFIG.PSU__PCIE__MC_BLOCKED_TLP {0} \
CONFIG.PSU__PCIE__MSIX_BAR_INDICATOR {<Select>} \
CONFIG.PSU__PCIE__MSIX_CAPABILITY {0} \
CONFIG.PSU__PCIE__MSIX_PBA_BAR_INDICATOR {<Select>} \
CONFIG.PSU__PCIE__MSIX_PBA_OFFSET {0} \
CONFIG.PSU__PCIE__MSIX_TABLE_OFFSET {0} \
CONFIG.PSU__PCIE__MSIX_TABLE_SIZE {0} \
CONFIG.PSU__PCIE__MSI_64BIT_ADDR_CAPABLE {0} \
CONFIG.PSU__PCIE__MSI_CAPABILITY {0} \
CONFIG.PSU__PCIE__MSI_MULTIPLE_MSG_CAPABLE {<Select>} \
CONFIG.PSU__PCIE__MULTIHEADER {0} \
CONFIG.PSU__PCIE__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__PCIE__PERIPHERAL__ENDPOINT_ENABLE {1} \
CONFIG.PSU__PCIE__PERIPHERAL__ENDPOINT_IO {<Select>} \
CONFIG.PSU__PCIE__PERIPHERAL__ROOTPORT_ENABLE {0} \
CONFIG.PSU__PCIE__PERIPHERAL__ROOTPORT_IO {<Select>} \
CONFIG.PSU__PCIE__PERM_ROOT_ERR_UPDATE {0} \
CONFIG.PSU__PCIE__RECEIVER_ERR {0} \
CONFIG.PSU__PCIE__RECEIVER_OVERFLOW {0} \
CONFIG.PSU__PCIE__REVISION_ID {0x0} \
CONFIG.PSU__PCIE__SUBSYSTEM_ID {0x7} \
CONFIG.PSU__PCIE__SUBSYSTEM_VENDOR_ID {0x10EE} \
CONFIG.PSU__PCIE__SUB_CLASS_INTERFACE_MENU {<Select>} \
CONFIG.PSU__PCIE__SURPRISE_DOWN {0} \
CONFIG.PSU__PCIE__TLP_PREFIX_BLOCKED {0} \
CONFIG.PSU__PCIE__UNCORRECTABL_INT_ERR {0} \
CONFIG.PSU__PCIE__USE_CLASS_CODE_LOOKUP_ASSISTANT {<Select>} \
CONFIG.PSU__PCIE__VENDOR_ID {0x10EE} \
CONFIG.PSU__PJTAG__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__PJTAG__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__PL_CLK1_BUF {true} \
CONFIG.PSU__PL__POWER__ON {1} \
CONFIG.PSU__PMU_COHERENCY {1} \
CONFIG.PSU__PMU__EMIO_GPI__ENABLE {<Select>} \
CONFIG.PSU__PMU__EMIO_GPO__ENABLE {<Select>} \
CONFIG.PSU__PMU__GPI0__ENABLE {<Select>} \
CONFIG.PSU__PMU__GPI0__IO {<Select>} \
CONFIG.PSU__PMU__GPI1__ENABLE {<Select>} \
CONFIG.PSU__PMU__GPI1__IO {<Select>} \
CONFIG.PSU__PMU__GPI2__ENABLE {<Select>} \
CONFIG.PSU__PMU__GPI2__IO {<Select>} \
CONFIG.PSU__PMU__GPI3__ENABLE {<Select>} \
CONFIG.PSU__PMU__GPI3__IO {<Select>} \
CONFIG.PSU__PMU__GPI4__ENABLE {<Select>} \
CONFIG.PSU__PMU__GPI4__IO {<Select>} \
CONFIG.PSU__PMU__GPI5__ENABLE {<Select>} \
CONFIG.PSU__PMU__GPI5__IO {<Select>} \
CONFIG.PSU__PMU__GPO0__ENABLE {<Select>} \
CONFIG.PSU__PMU__GPO0__IO {<Select>} \
CONFIG.PSU__PMU__GPO1__ENABLE {<Select>} \
CONFIG.PSU__PMU__GPO1__IO {<Select>} \
CONFIG.PSU__PMU__GPO2__ENABLE {<Select>} \
CONFIG.PSU__PMU__GPO2__IO {<Select>} \
CONFIG.PSU__PMU__GPO3__ENABLE {<Select>} \
CONFIG.PSU__PMU__GPO3__IO {<Select>} \
CONFIG.PSU__PMU__GPO4__ENABLE {<Select>} \
CONFIG.PSU__PMU__GPO4__IO {<Select>} \
CONFIG.PSU__PMU__GPO5__ENABLE {<Select>} \
CONFIG.PSU__PMU__GPO5__IO {<Select>} \
CONFIG.PSU__PMU__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__PMU__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__ANALOG {0.004} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__DDR {0.711} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__FPD {1.102} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__LPD {0.300} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__MIO {0.196} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__PLL {0.067} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__SERDES {0.114} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__TOTAL {2.493} \
CONFIG.PSU__POWER_SUMMARY__ONCHIP {3.940} \
CONFIG.PSU__POWER_SUMMARY__STATIC__ANALOG {0.034} \
CONFIG.PSU__POWER_SUMMARY__STATIC__DDR {0.000} \
CONFIG.PSU__POWER_SUMMARY__STATIC__FPD {1.785} \
CONFIG.PSU__POWER_SUMMARY__STATIC__LPD {0.366} \
CONFIG.PSU__POWER_SUMMARY__STATIC__MIO {0.000} \
CONFIG.PSU__POWER_SUMMARY__STATIC__PLL {0.003} \
CONFIG.PSU__POWER_SUMMARY__STATIC__SERDES {0.000} \
CONFIG.PSU__POWER_SUMMARY__STATIC__TOTAL {1.447} \
CONFIG.PSU__POWER__ACPU__VCCPSINTFP {0.440} \
CONFIG.PSU__POWER__AFI_FPD__VCCPSINTFP {0.113} \
CONFIG.PSU__POWER__AFI_LPD__VCCPSINTLP {0.028} \
CONFIG.PSU__POWER__APLL__VCCPSPLL {0.016} \
CONFIG.PSU__POWER__CAN0__VCCPSIO {0} \
CONFIG.PSU__POWER__CAN1__VCCPSIO {0.004} \
CONFIG.PSU__POWER__CSU__VCCPSIO {0} \
CONFIG.PSU__POWER__DDR__VCCPSDDR {0.711} \
CONFIG.PSU__POWER__DDR__VCCPSINTFP {0} \
CONFIG.PSU__POWER__DPAUX__VCCPSIO {0} \
CONFIG.PSU__POWER__DPLL__VCCPSPLL {0} \
CONFIG.PSU__POWER__DP__VCCPSGTA {0.000} \
CONFIG.PSU__POWER__DP__VCCPSINTFP {0.000} \
CONFIG.PSU__POWER__FPINT__VCCPSINTFP {0.232} \
CONFIG.PSU__POWER__GEM0__VCCPSINTLP {0.000} \
CONFIG.PSU__POWER__GEM0__VCCPSIO {0.034} \
CONFIG.PSU__POWER__GEM1__VCCPSINTLP {0.000} \
CONFIG.PSU__POWER__GEM1__VCCPSIO {0} \
CONFIG.PSU__POWER__GEM2__VCCPSINTLP {0.000} \
CONFIG.PSU__POWER__GEM2__VCCPSIO {0} \
CONFIG.PSU__POWER__GEM3__VCCPSINTLP {0.000} \
CONFIG.PSU__POWER__GEM3__VCCPSIO {0.034} \
CONFIG.PSU__POWER__GPIO0__VCCPSIO {0} \
CONFIG.PSU__POWER__GPIO1__VCCPSIO {0} \
CONFIG.PSU__POWER__GPIO2__VCCPSIO {0} \
CONFIG.PSU__POWER__GPU__VCCPSINTFP {0.318} \
CONFIG.PSU__POWER__IOPLL__VCCPSPLL {0.018} \
CONFIG.PSU__POWER__LPINT__VCCPSINTLP {0.115} \
CONFIG.PSU__POWER__NAND__VCCPSINTLP {0.000} \
CONFIG.PSU__POWER__NAND__VCCPSIO {0} \
CONFIG.PSU__POWER__PCIE__VCCPSGTA {0.000} \
CONFIG.PSU__POWER__PCIE__VCCPSINTFP {0.000} \
CONFIG.PSU__POWER__PJTAG__VCCPSIO {0} \
CONFIG.PSU__POWER__PMU__VCCPSIO {0} \
CONFIG.PSU__POWER__QSPI__VCCPSINTLP {0.002} \
CONFIG.PSU__POWER__QSPI__VCCPSIO {0.018} \
CONFIG.PSU__POWER__RPLL__VCCPSPLL {0.016} \
CONFIG.PSU__POWER__RPU__VCCPSINTLP {0.109} \
CONFIG.PSU__POWER__SATA__VCCPSGTA {0.000} \
CONFIG.PSU__POWER__SATA__VCCPSINTFP {0.000} \
CONFIG.PSU__POWER__SD0__VCCPSIO {0} \
CONFIG.PSU__POWER__SD1__VCCPSIO {0.047} \
CONFIG.PSU__POWER__SGMII__VCCPSGTA {0.000} \
CONFIG.PSU__POWER__SPI0__VCCPSIO {0.028} \
CONFIG.PSU__POWER__SPI1__VCCPSIO {0.028} \
CONFIG.PSU__POWER__TRACE__VCCPSIO {0} \
CONFIG.PSU__POWER__TTC0__VCCPSIO {0.002} \
CONFIG.PSU__POWER__TTC1__VCCPSIO {0} \
CONFIG.PSU__POWER__TTC2__VCCPSIO {0} \
CONFIG.PSU__POWER__TTC3__VCCPSIO {0} \
CONFIG.PSU__POWER__UART0__VCCPSIO {0.004} \
CONFIG.PSU__POWER__UART1__VCCPSIO {0.004} \
CONFIG.PSU__POWER__USB0__VCCPSINTLP {0.017} \
CONFIG.PSU__POWER__USB0__VCCPSIO {0.046} \
CONFIG.PSU__POWER__USB1__VCCPSINTLP {0.017} \
CONFIG.PSU__POWER__USB1__VCCPSIO {0.046} \
CONFIG.PSU__POWER__USB3__VCCPSGTA {0.114} \
CONFIG.PSU__POWER__VPLL__VCCPSPLL {0.017} \
CONFIG.PSU__POWER__WDT0__VCCPSIO {0} \
CONFIG.PSU__POWER__WDT1__VCCPSIO {0} \
CONFIG.PSU__PSS_ALT_REF_CLK__FREQMHZ {33.333} \
CONFIG.PSU__PSS_REF_CLK__FREQMHZ {33.333} \
CONFIG.PSU__QSPI_COHERENCY {1} \
CONFIG.PSU__QSPI__GRP_FBCLK__ENABLE {0} \
CONFIG.PSU__QSPI__GRP_FBCLK__IO {<Select>} \
CONFIG.PSU__QSPI__PERIPHERAL__DATA_MODE {x4} \
CONFIG.PSU__QSPI__PERIPHERAL__ENABLE {1} \
CONFIG.PSU__QSPI__PERIPHERAL__IO {MIO 0 .. 12} \
CONFIG.PSU__QSPI__PERIPHERAL__MODE {Dual Parallel} \
CONFIG.PSU__RPU_COHERENCY {0} \
CONFIG.PSU__RPU__POWER__ON {1} \
CONFIG.PSU__SATA__LANE0__ENABLE {<Select>} \
CONFIG.PSU__SATA__LANE0__IO {<Select>} \
CONFIG.PSU__SATA__LANE1__ENABLE {<Select>} \
CONFIG.PSU__SATA__LANE1__IO {<Select>} \
CONFIG.PSU__SATA__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__SD0_COHERENCY {1} \
CONFIG.PSU__SD0__DATA_TRANSFER_MODE {<Select>} \
CONFIG.PSU__SD0__GRP_CD__ENABLE {0} \
CONFIG.PSU__SD0__GRP_CD__IO {<Select>} \
CONFIG.PSU__SD0__GRP_POW__ENABLE {0} \
CONFIG.PSU__SD0__GRP_POW__IO {<Select>} \
CONFIG.PSU__SD0__GRP_WP__ENABLE {0} \
CONFIG.PSU__SD0__GRP_WP__IO {<Select>} \
CONFIG.PSU__SD0__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__SD0__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__SD0__RESET__ENABLE {0} \
CONFIG.PSU__SD0__SLOT_TYPE {<Select>} \
CONFIG.PSU__SD1_COHERENCY {1} \
CONFIG.PSU__SD1__DATA_TRANSFER_MODE {4Bit} \
CONFIG.PSU__SD1__GRP_CD__ENABLE {1} \
CONFIG.PSU__SD1__GRP_CD__IO {MIO 45} \
CONFIG.PSU__SD1__GRP_POW__ENABLE {0} \
CONFIG.PSU__SD1__GRP_POW__IO {<Select>} \
CONFIG.PSU__SD1__GRP_WP__ENABLE {1} \
CONFIG.PSU__SD1__GRP_WP__IO {MIO 44} \
CONFIG.PSU__SD1__PERIPHERAL__ENABLE {1} \
CONFIG.PSU__SD1__PERIPHERAL__IO {MIO 46 .. 51} \
CONFIG.PSU__SD1__RESET__ENABLE {<Select>} \
CONFIG.PSU__SD1__SLOT_TYPE {SD} \
CONFIG.PSU__SPI0_LOOP_SPI1__ENABLE {0} \
CONFIG.PSU__SPI0__GRP_SS0__ENABLE {0} \
CONFIG.PSU__SPI0__GRP_SS0__IO {<Select>} \
CONFIG.PSU__SPI0__GRP_SS1__ENABLE {0} \
CONFIG.PSU__SPI0__GRP_SS1__IO {<Select>} \
CONFIG.PSU__SPI0__GRP_SS2__ENABLE {0} \
CONFIG.PSU__SPI0__GRP_SS2__IO {<Select>} \
CONFIG.PSU__SPI0__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__SPI0__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__SPI1__GRP_SS0__ENABLE {1} \
CONFIG.PSU__SPI1__GRP_SS0__IO {MIO 21} \
CONFIG.PSU__SPI1__GRP_SS1__ENABLE {1} \
CONFIG.PSU__SPI1__GRP_SS1__IO {MIO 20} \
CONFIG.PSU__SPI1__GRP_SS2__ENABLE {1} \
CONFIG.PSU__SPI1__GRP_SS2__IO {MIO 19} \
CONFIG.PSU__SPI1__PERIPHERAL__ENABLE {1} \
CONFIG.PSU__SPI1__PERIPHERAL__IO {MIO 18 .. 23} \
CONFIG.PSU__SWDT0__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__SWDT0__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__SWDT1__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__SWDT1__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__TCM0A__POWER__ON {1} \
CONFIG.PSU__TCM0B__POWER__ON {1} \
CONFIG.PSU__TCM1A__POWER__ON {1} \
CONFIG.PSU__TCM1B__POWER__ON {1} \
CONFIG.PSU__TESTSCAN__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__TRACE__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__TRACE__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__TRACE__WIDTH {<Select>} \
CONFIG.PSU__TRISTATE__INVERTED {0} \
CONFIG.PSU__TTC0__PERIPHERAL__ENABLE {1} \
CONFIG.PSU__TTC0__PERIPHERAL__IO {EMIO} \
CONFIG.PSU__TTC1__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__TTC1__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__TTC2__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__TTC2__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__TTC3__PERIPHERAL__ENABLE {0} \
CONFIG.PSU__TTC3__PERIPHERAL__IO {<Select>} \
CONFIG.PSU__UART0_LOOP_UART1__ENABLE {0} \
CONFIG.PSU__UART0__BAUD_RATE {115200} \
CONFIG.PSU__UART0__MODEM__ENABLE {0} \
CONFIG.PSU__UART0__PERIPHERAL__ENABLE {1} \
CONFIG.PSU__UART0__PERIPHERAL__IO {MIO 14 .. 15} \
CONFIG.PSU__UART1__BAUD_RATE {115200} \
CONFIG.PSU__UART1__MODEM__ENABLE {0} \
CONFIG.PSU__UART1__PERIPHERAL__ENABLE {1} \
CONFIG.PSU__UART1__PERIPHERAL__IO {MIO 24 .. 25} \
CONFIG.PSU__USB0_COHERENCY {1} \
CONFIG.PSU__USB0__PERIPHERAL__ENABLE {1} \
CONFIG.PSU__USB0__PERIPHERAL__IO {MIO 52 .. 63} \
CONFIG.PSU__USB1_COHERENCY {1} \
CONFIG.PSU__USB1__PERIPHERAL__ENABLE {1} \
CONFIG.PSU__USB1__PERIPHERAL__IO {MIO 64 .. 75} \
CONFIG.PSU__USB3_0__PERIPHERAL__ENABLE {1} \
CONFIG.PSU__USB3_0__PERIPHERAL__IO {GT Lane2} \
CONFIG.PSU__USB3_1__PERIPHERAL__ENABLE {1} \
CONFIG.PSU__USB3_1__PERIPHERAL__IO {GT Lane3} \
CONFIG.PSU__USE__FABRIC__RST {1} \
CONFIG.PSU__USE__IRQ0 {1} \
CONFIG.PSU__USE__IRQ1 {1} \
CONFIG.PSU__USE__M_AXI_GP2 {1} \
CONFIG.PSU__USE__S_AXI_GP3 {1} \
CONFIG.PSU__USE__S_AXI_GP4 {1} \
CONFIG.PSU__VIDEO_REF_CLK__FREQMHZ {33.333} \
 ] $sys_ps8

  # Need to retain value_src of defaults
  set_property -dict [ list \
CONFIG.PSU_BANK_0_IO_STANDARD.VALUE_SRC {DEFAULT} \
CONFIG.PSU_BANK_1_IO_STANDARD.VALUE_SRC {DEFAULT} \
CONFIG.PSU_BANK_2_IO_STANDARD.VALUE_SRC {DEFAULT} \
CONFIG.PSU_DDR_RAM_HIGHADDR.VALUE_SRC {DEFAULT} \
CONFIG.PSU_DDR_RAM_HIGHADDR_OFFSET.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_0_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_0_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_0_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_0_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_0_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_10_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_10_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_10_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_10_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_10_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_11_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_11_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_11_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_11_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_11_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_12_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_12_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_12_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_12_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_12_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_13_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_13_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_13_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_13_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_13_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_14_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_14_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_14_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_14_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_14_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_15_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_15_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_15_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_15_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_15_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_16_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_16_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_16_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_16_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_16_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_17_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_17_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_17_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_17_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_17_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_18_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_18_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_18_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_18_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_18_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_19_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_19_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_19_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_19_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_19_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_1_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_1_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_1_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_1_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_1_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_20_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_20_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_20_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_20_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_20_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_21_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_21_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_21_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_21_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_21_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_22_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_22_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_22_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_22_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_22_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_23_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_23_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_23_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_23_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_23_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_24_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_24_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_24_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_24_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_24_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_25_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_25_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_25_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_25_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_25_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_26_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_26_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_26_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_26_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_26_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_27_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_27_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_27_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_27_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_27_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_28_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_28_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_28_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_28_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_28_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_29_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_29_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_29_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_29_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_29_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_2_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_2_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_2_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_2_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_2_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_30_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_30_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_30_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_30_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_30_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_31_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_31_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_31_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_31_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_31_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_32_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_32_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_32_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_32_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_32_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_33_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_33_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_33_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_33_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_33_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_34_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_34_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_34_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_34_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_34_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_35_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_35_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_35_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_35_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_35_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_36_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_36_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_36_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_36_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_36_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_37_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_37_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_37_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_37_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_37_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_38_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_38_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_38_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_38_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_38_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_39_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_39_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_39_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_39_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_39_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_3_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_3_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_3_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_3_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_3_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_40_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_40_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_40_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_40_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_40_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_41_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_41_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_41_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_41_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_41_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_42_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_42_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_42_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_42_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_42_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_43_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_43_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_43_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_43_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_43_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_44_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_44_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_44_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_44_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_44_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_45_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_45_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_45_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_45_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_45_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_46_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_46_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_46_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_46_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_46_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_47_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_47_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_47_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_47_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_47_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_48_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_48_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_48_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_48_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_48_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_49_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_49_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_49_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_49_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_49_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_4_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_4_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_4_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_4_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_4_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_50_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_50_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_50_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_50_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_50_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_51_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_51_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_51_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_51_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_51_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_52_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_52_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_52_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_52_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_52_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_53_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_53_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_53_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_53_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_53_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_54_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_54_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_54_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_54_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_54_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_55_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_55_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_55_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_55_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_55_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_56_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_56_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_56_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_56_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_56_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_57_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_57_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_57_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_57_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_57_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_58_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_58_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_58_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_58_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_58_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_59_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_59_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_59_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_59_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_59_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_5_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_5_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_5_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_5_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_5_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_60_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_60_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_60_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_60_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_60_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_61_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_61_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_61_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_61_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_61_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_62_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_62_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_62_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_62_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_62_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_63_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_63_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_63_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_63_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_63_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_64_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_64_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_64_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_64_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_64_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_65_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_65_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_65_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_65_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_65_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_66_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_66_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_66_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_66_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_66_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_67_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_67_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_67_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_67_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_67_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_68_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_68_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_68_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_68_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_68_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_69_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_69_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_69_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_69_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_69_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_6_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_6_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_6_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_6_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_6_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_70_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_70_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_70_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_70_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_70_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_71_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_71_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_71_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_71_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_71_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_72_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_72_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_72_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_72_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_72_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_73_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_73_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_73_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_73_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_73_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_74_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_74_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_74_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_74_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_74_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_75_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_75_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_75_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_75_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_75_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_76_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_76_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_76_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_76_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_76_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_77_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_77_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_77_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_77_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_77_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_7_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_7_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_7_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_7_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_7_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_8_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_8_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_8_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_8_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_8_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_9_DIRECTION.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_9_DRIVE_STRENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_9_INPUT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_9_PULLUPDOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_9_SLEW.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_TREE_PERIPHERALS.VALUE_SRC {DEFAULT} \
CONFIG.PSU_MIO_TREE_SIGNALS.VALUE_SRC {DEFAULT} \
CONFIG.PSU_PACKAGE_DDR_BOARD_DELAY3.VALUE_SRC {DEFAULT} \
CONFIG.PSU_PRESET_BANK0_VOLTAGE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_PRESET_BANK1_VOLTAGE.VALUE_SRC {DEFAULT} \
CONFIG.PSU_UIPARAM_GENERATE_SUMMARY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ACPU0__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ACPU1__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ACPU2__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ACPU3__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ACT_DDR_FREQ_MHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ADMA_COHERENCY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__AUX_REF_CLK__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CAN0_LOOP_CAN1__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CAN0__GRP_CLK__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CAN0__GRP_CLK__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CAN0__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CAN0__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CAN1__GRP_CLK__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CAN1__GRP_CLK__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CPU_CPU_6X4X_MAX_RANGE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CPU_R5__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__ACPU_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__ACPU_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__ACPU__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI0_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI0_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI0_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI0_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI0_REF__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI1_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI1_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI1_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI1_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI1_REF__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI2_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI2_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI2_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI2_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI2_REF__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI3_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI3_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI3_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI3_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI3_REF__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI4_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI4_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI4_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI4_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI4_REF__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI5_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI5_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI5_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI5_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__AFI5_REF__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__APLL_CTRL__FRACDATA.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__APLL_CTRL__FRACFREQ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__APLL_FRAC_CFG__ENABLED.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__APM_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__APM_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__APM_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__APM_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DBG_FPD_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DBG_FPD_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DBG_TRACE_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DBG_TRACE_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DBG_TSTMP_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DBG_TSTMP_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DDR_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DFT125_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DFT125_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DFT250_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DFT250_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DFT270_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DFT270_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DFT300_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DFT300_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DPDMA_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DPDMA_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DPLL_CTRL__FRACDATA.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DPLL_CTRL__FRACFREQ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DPLL_FRAC_CFG__ENABLED.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DP_AUDIO_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DP_AUDIO_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DP_STC_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DP_STC_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DP_VIDEO_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__DP_VIDEO_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__GDMA_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__GDMA_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__GPU_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__GPU_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__GPU__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__GTGREF0_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__GTGREF0_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__PCIE_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__PCIE_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__SATA_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__SATA_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__TOPSW_LSBUS_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__TOPSW_LSBUS_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__TOPSW_MAIN_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__TOPSW_MAIN_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRF_APB__VPLL_CTRL__FRACFREQ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__ADMA_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__ADMA_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__AFI6_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__AFI6_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__AFI6_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__AFI6_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__AFI6__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__AMS_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__AMS_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__AMS_REF_CTRL__DIVISOR1.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__AMS_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__AMS_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__AMS__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__CAN0_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__CAN0_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__CAN1_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__CAN1_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__CPU_R5_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__CPU_R5_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__CSU_PLL_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__CSU_PLL_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__DBG_LPD_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__DBG_LPD_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__DEBUG_R5_ATCLK_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__DEBUG_R5_ATCLK_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__DEBUG_R5_ATCLK_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__DEBUG_R5_ATCLK_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__DLL_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__DLL_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__DLL_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__GEM0_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__GEM0_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__GEM1_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__GEM1_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__GEM2_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__GEM2_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__GEM3_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__GEM3_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__GEM_TSU_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__GEM_TSU_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__I2C0_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__I2C0_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__I2C1_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__I2C1_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__IOPLL_CTRL__FRACDATA.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__IOPLL_CTRL__FRACFREQ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__IOPLL_FRAC_CFG__ENABLED.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__IOU_SWITCH_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__IOU_SWITCH_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__LPD_LSBUS_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__LPD_LSBUS_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__LPD_SWITCH_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__LPD_SWITCH_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__NAND_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__NAND_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__OCM_MAIN_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__OCM_MAIN_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__OCM_MAIN_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__OCM_MAIN_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__OCM_MAIN__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PCAP_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PCAP_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PICDEBUG_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PICDEBUG_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PICDEBUG_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PICDEBUG_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PICDEBUG_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PICDEBUG_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PICDEBUG_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PICDEBUG_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PICDEBUG_TEMP_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PICDEBUG_TEMP_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PICDEBUG_TEMP_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PICDEBUG_TEMP_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PL0_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PL1_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PL1_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PL1_REF_CTRL__DIVISOR1.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PL1_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PL2_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PL2_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PL2_REF_CTRL__DIVISOR1.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PL2_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PL2_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PL3_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PL3_REF_CTRL__DIVISOR0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PL3_REF_CTRL__DIVISOR1.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PL3_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__PL3_REF_CTRL__SRCSEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__QSPI_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__QSPI_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__RPLL_CTRL__FRACDATA.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__RPLL_CTRL__FRACFREQ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__RPLL_FRAC_CFG__ENABLED.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__SDIO0_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__SDIO0_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__SDIO1_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__SDIO1_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__SPI0_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__SPI1_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__TIMESTAMP_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__TIMESTAMP_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__UART0_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__UART0_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__UART1_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__UART1_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__USB0_BUS_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__USB0_BUS_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__USB1_BUS_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__USB1_BUS_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__USB3_DUAL_REF_CTRL__ACT_FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRL_APB__USB3_DUAL_REF_CTRL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CRYSTAL__PERIPHERAL__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU_COHERENCY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_0__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_0__ERASE_BBRAM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_0__RESPONSE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_10__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_10__ERASE_BBRAM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_10__RESPONSE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_11__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_11__ERASE_BBRAM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_11__RESPONSE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_12__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_12__ERASE_BBRAM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_12__RESPONSE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_1__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_1__ERASE_BBRAM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_1__RESPONSE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_2__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_2__ERASE_BBRAM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_2__RESPONSE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_3__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_3__ERASE_BBRAM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_3__RESPONSE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_4__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_4__ERASE_BBRAM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_4__RESPONSE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_5__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_5__ERASE_BBRAM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_5__RESPONSE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_6__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_6__ERASE_BBRAM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_6__RESPONSE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_7__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_7__ERASE_BBRAM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_7__RESPONSE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_8__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_8__ERASE_BBRAM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_8__RESPONSE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_9__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_9__ERASE_BBRAM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__CSU_TAMPER_9__RESPONSE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__CSU__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DAP_COHERENCY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__BOARD_DELAY0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__BOARD_DELAY1.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__BOARD_DELAY2.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__BOARD_DELAY3.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__CLOCK_0_LENGTH_MM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__CLOCK_0_PACKAGE_LENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__CLOCK_0_PROPOGATION_DELAY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__CLOCK_1_LENGTH_MM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__CLOCK_1_PACKAGE_LENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__CLOCK_1_PROPOGATION_DELAY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__CLOCK_2_LENGTH_MM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__CLOCK_2_PACKAGE_LENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__CLOCK_2_PROPOGATION_DELAY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__CLOCK_3_LENGTH_MM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__CLOCK_3_PACKAGE_LENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__CLOCK_3_PROPOGATION_DELAY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DDR4_MAXPWR_SAVING_EN.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DEEP_PWR_DOWN_EN.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DERATE_INT_D.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_0_LENGTH_MM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_0_PACKAGE_LENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_0_PROPOGATION_DELAY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_1_LENGTH_MM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_1_PACKAGE_LENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_1_PROPOGATION_DELAY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_2_LENGTH_MM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_2_PACKAGE_LENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_2_PROPOGATION_DELAY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_3_LENGTH_MM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_3_PACKAGE_LENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_3_PROPOGATION_DELAY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_TO_CLK_DELAY_0.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_TO_CLK_DELAY_1.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_TO_CLK_DELAY_2.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQS_TO_CLK_DELAY_3.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQ_0_LENGTH_MM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQ_0_PACKAGE_LENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQ_0_PROPOGATION_DELAY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQ_1_LENGTH_MM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQ_1_PACKAGE_LENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQ_1_PROPOGATION_DELAY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQ_2_LENGTH_MM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQ_2_PACKAGE_LENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQ_2_PROPOGATION_DELAY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQ_3_LENGTH_MM.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQ_3_PACKAGE_LENGTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__DQ_3_PROPOGATION_DELAY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__ECC_SCRUB.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__EN_2ND_CLK.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__HIGH_TEMP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__PARTNO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__PLL_BYPASS.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__PWR_DOWN_EN.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__USE_INTERNAL_VREF.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDRC__VIDEO_BUFFER_SIZE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDR_HIGH_ADDRESS_GUI_ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DDR__INTERFACE__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DISPLAYPORT__LANE0__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DISPLAYPORT__LANE0__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DISPLAYPORT__LANE1__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DISPLAYPORT__LANE1__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DISPLAYPORT__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DPAUX__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DPAUX__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__DP__LANE_SEL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ENET0__GRP_MDIO__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ENET0__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ENET1__GRP_MDIO__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ENET1__GRP_MDIO__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ENET1__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ENET1__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ENET2__GRP_MDIO__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ENET2__GRP_MDIO__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ENET2__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__ENET2__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__FP__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEM0_COHERENCY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEM1_COHERENCY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEM2_COHERENCY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEM3_COHERENCY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEM__TSU__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEM__TSU__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GENERATE_SECURITY_REGISTERS.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEN_IPI_0__MASTER.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEN_IPI_10__MASTER.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEN_IPI_1__MASTER.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEN_IPI_2__MASTER.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEN_IPI_3__MASTER.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEN_IPI_4__MASTER.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEN_IPI_5__MASTER.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEN_IPI_6__MASTER.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEN_IPI_7__MASTER.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEN_IPI_8__MASTER.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GEN_IPI_9__MASTER.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GPIO2_MIO__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GPIO2_MIO__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GPIO_EMIO__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GPIO_EMIO__WIDTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GPU_PP0__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GPU_PP1__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GT_REF_CLK__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__GT__LINK_SPEED.VALUE_SRC {DEFAULT} \
CONFIG.PSU__I2C0_LOOP_I2C1__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__I2C0__GRP_INT__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__I2C0__GRP_INT__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__I2C1__GRP_INT__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__I2C1__GRP_INT__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__L2_BANK0__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__NAND_COHERENCY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__NAND__CHIP_ENABLE__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__NAND__CHIP_ENABLE__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__NAND__DATA_STROBE__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__NAND__DATA_STROBE__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__NAND__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__NAND__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__NAND__READY_BUSY__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__NAND__READY_BUSY__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__OCM_BANK0__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__OCM_BANK1__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__OCM_BANK2__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__OCM_BANK3__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__ACS_VIOLATION.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__AER_CAPABILITY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__ATOMICOP_EGRESS_BLOCKED.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR0_64BIT.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR0_PREFETCHABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR0_SCALE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR0_SIZE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR0_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR1_64BIT.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR1_ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR1_PREFETCHABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR1_SCALE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR1_SIZE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR1_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR2_64BIT.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR2_ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR2_PREFETCHABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR2_SCALE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR2_SIZE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR2_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR3_64BIT.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR3_ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR3_PREFETCHABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR3_SCALE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR3_SIZE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR3_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR4_64BIT.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR4_ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR4_PREFETCHABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR4_SCALE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR4_SIZE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR4_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR5_64BIT.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR5_ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR5_PREFETCHABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR5_SCALE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR5_SIZE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BAR5_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BASE_CLASS_MENU.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__BRIDGE_BAR_INDICATOR.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__CAP_SLOT_IMPLEMENTED.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__CLASS_CODE_INTERFACE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__COMPLETER_ABORT.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__COMPLTION_TIMEOUT.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__CORRECTABLE_INT_ERR.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__ECRC_CHECK.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__ECRC_ERR.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__ECRC_GEN.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__EROM_ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__EROM_SCALE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__EROM_SIZE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__FLOW_CONTROL_ERR.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__FLOW_CONTROL_PROTOCOL_ERR.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__HEADER_LOG_OVERFLOW.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__INTERFACE_WIDTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__INTX_GENERATION.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__INTX_PIN.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__LANE1__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__LANE2__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__LANE2__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__LANE3__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__LANE3__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__LEGACY_INTERRUPT.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__MAX_PAYLOAD_SIZE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__MC_BLOCKED_TLP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__MSIX_BAR_INDICATOR.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__MSIX_CAPABILITY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__MSIX_PBA_BAR_INDICATOR.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__MSIX_PBA_OFFSET.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__MSIX_TABLE_OFFSET.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__MSIX_TABLE_SIZE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__MSI_64BIT_ADDR_CAPABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__MSI_CAPABILITY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__MSI_MULTIPLE_MSG_CAPABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__MULTIHEADER.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__PERIPHERAL__ENDPOINT_ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__PERIPHERAL__ENDPOINT_IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__PERIPHERAL__ROOTPORT_ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__PERM_ROOT_ERR_UPDATE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__RECEIVER_ERR.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__RECEIVER_OVERFLOW.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__REVISION_ID.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__SUBSYSTEM_ID.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__SUBSYSTEM_VENDOR_ID.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__SUB_CLASS_INTERFACE_MENU.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__SURPRISE_DOWN.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__TLP_PREFIX_BLOCKED.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__UNCORRECTABL_INT_ERR.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__USE_CLASS_CODE_LOOKUP_ASSISTANT.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PCIE__VENDOR_ID.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PJTAG__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PJTAG__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PL_CLK1_BUF.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PL__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU_COHERENCY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__EMIO_GPI__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__EMIO_GPO__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPI0__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPI0__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPI1__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPI1__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPI2__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPI2__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPI3__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPI3__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPI4__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPI4__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPI5__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPI5__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPO0__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPO0__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPO1__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPO1__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPO2__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPO2__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPO3__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPO3__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPO4__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPO4__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPO5__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__GPO5__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PMU__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__ANALOG.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__DDR.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__FPD.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__LPD.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__MIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__PLL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__SERDES.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__DYNAMIC__TOTAL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__ONCHIP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__STATIC__ANALOG.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__STATIC__DDR.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__STATIC__FPD.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__STATIC__LPD.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__STATIC__MIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__STATIC__PLL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__STATIC__SERDES.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER_SUMMARY__STATIC__TOTAL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__ACPU__VCCPSINTFP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__AFI_FPD__VCCPSINTFP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__AFI_LPD__VCCPSINTLP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__APLL__VCCPSPLL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__CAN0__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__CAN1__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__CSU__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__DDR__VCCPSDDR.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__DDR__VCCPSINTFP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__DPAUX__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__DPLL__VCCPSPLL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__DP__VCCPSGTA.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__DP__VCCPSINTFP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__FPINT__VCCPSINTFP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__GEM0__VCCPSINTLP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__GEM0__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__GEM1__VCCPSINTLP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__GEM1__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__GEM2__VCCPSINTLP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__GEM2__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__GEM3__VCCPSINTLP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__GEM3__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__GPIO0__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__GPIO1__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__GPIO2__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__GPU__VCCPSINTFP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__IOPLL__VCCPSPLL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__LPINT__VCCPSINTLP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__NAND__VCCPSINTLP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__NAND__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__PCIE__VCCPSGTA.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__PCIE__VCCPSINTFP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__PJTAG__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__PMU__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__QSPI__VCCPSINTLP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__QSPI__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__RPLL__VCCPSPLL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__RPU__VCCPSINTLP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__SATA__VCCPSGTA.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__SATA__VCCPSINTFP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__SD0__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__SD1__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__SGMII__VCCPSGTA.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__SPI0__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__SPI1__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__TRACE__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__TTC0__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__TTC1__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__TTC2__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__TTC3__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__UART0__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__UART1__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__USB0__VCCPSINTLP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__USB0__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__USB1__VCCPSINTLP.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__USB1__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__USB3__VCCPSGTA.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__VPLL__VCCPSPLL.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__WDT0__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__POWER__WDT1__VCCPSIO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__PSS_ALT_REF_CLK__FREQMHZ.VALUE_SRC {DEFAULT} \
CONFIG.PSU__QSPI_COHERENCY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__RPU_COHERENCY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__RPU__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SATA__LANE0__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SATA__LANE0__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SD0_COHERENCY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SD0__DATA_TRANSFER_MODE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SD0__GRP_CD__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SD0__GRP_CD__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SD0__GRP_POW__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SD0__GRP_POW__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SD0__GRP_WP__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SD0__GRP_WP__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SD0__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SD0__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SD0__RESET__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SD0__SLOT_TYPE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SD1_COHERENCY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SD1__RESET__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SPI0_LOOP_SPI1__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SPI0__GRP_SS0__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SPI0__GRP_SS0__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SPI0__GRP_SS1__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SPI0__GRP_SS1__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SPI0__GRP_SS2__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SPI0__GRP_SS2__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SWDT0__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SWDT0__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SWDT1__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__SWDT1__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__TCM0A__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__TCM0B__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__TCM1A__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__TCM1B__POWER__ON.VALUE_SRC {DEFAULT} \
CONFIG.PSU__TESTSCAN__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__TRACE__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__TRACE__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__TRACE__WIDTH.VALUE_SRC {DEFAULT} \
CONFIG.PSU__TRISTATE__INVERTED.VALUE_SRC {DEFAULT} \
CONFIG.PSU__TTC1__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__TTC1__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__TTC2__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__TTC2__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__TTC3__PERIPHERAL__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__TTC3__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__UART0_LOOP_UART1__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__UART0__BAUD_RATE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__UART0__MODEM__ENABLE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__UART1__BAUD_RATE.VALUE_SRC {DEFAULT} \
CONFIG.PSU__USB0_COHERENCY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__USB1_COHERENCY.VALUE_SRC {DEFAULT} \
CONFIG.PSU__USB1__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__USB3_1__PERIPHERAL__IO.VALUE_SRC {DEFAULT} \
CONFIG.PSU__USE__FABRIC__RST.VALUE_SRC {DEFAULT} \
CONFIG.PSU__VIDEO_REF_CLK__FREQMHZ.VALUE_SRC {DEFAULT} \
 ] $sys_ps8

  # Create instance: sys_rstgen, and set properties
  set sys_rstgen [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 sys_rstgen ]
  set_property -dict [ list \
CONFIG.C_EXT_RST_WIDTH {1} \
 ] $sys_rstgen

  # Create instance: system_management_wiz_0, and set properties
  set system_management_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:system_management_wiz:1.3 system_management_wiz_0 ]

  # Create instance: util_ad9361_adc_fifo, and set properties
  set util_ad9361_adc_fifo [ create_bd_cell -type ip -vlnv analog.com:user:util_wfifo:1.0 util_ad9361_adc_fifo ]
  set_property -dict [ list \
CONFIG.DIN_ADDRESS_WIDTH {4} \
CONFIG.DIN_DATA_WIDTH {16} \
CONFIG.DOUT_DATA_WIDTH {16} \
CONFIG.NUM_OF_CHANNELS {4} \
 ] $util_ad9361_adc_fifo

  # Create instance: util_ad9361_adc_pack, and set properties
  set util_ad9361_adc_pack [ create_bd_cell -type ip -vlnv analog.com:user:util_cpack:1.0 util_ad9361_adc_pack ]
  set_property -dict [ list \
CONFIG.CHANNEL_DATA_WIDTH {16} \
CONFIG.NUM_OF_CHANNELS {4} \
 ] $util_ad9361_adc_pack

  # Create instance: util_ad9361_dac_upack, and set properties
  set util_ad9361_dac_upack [ create_bd_cell -type ip -vlnv analog.com:user:util_upack:1.0 util_ad9361_dac_upack ]
  set_property -dict [ list \
CONFIG.CHANNEL_DATA_WIDTH {16} \
CONFIG.NUM_OF_CHANNELS {4} \
 ] $util_ad9361_dac_upack

  # Create instance: util_ad9361_tdd_sync, and set properties
  set util_ad9361_tdd_sync [ create_bd_cell -type ip -vlnv analog.com:user:util_tdd_sync:1.0 util_ad9361_tdd_sync ]
  set_property -dict [ list \
CONFIG.TDD_SYNC_PERIOD {10000000} \
 ] $util_ad9361_tdd_sync

  # Create interface connections
  connect_bd_intf_net -intf_net S00_AXI_1 [get_bd_intf_pins axi_cpu_interconnect/S00_AXI] [get_bd_intf_pins sys_ps8/M_AXI_HPM0_LPD]
  connect_bd_intf_net -intf_net S00_AXI_2 [get_bd_intf_pins axi_ad9361_adc_dma/m_dest_axi] [get_bd_intf_pins axi_hp1_interconnect/S00_AXI]
  connect_bd_intf_net -intf_net S00_AXI_3 [get_bd_intf_pins axi_ad9361_dac_dma/m_src_axi] [get_bd_intf_pins axi_hp2_interconnect/S00_AXI]
  connect_bd_intf_net -intf_net axi_cpu_interconnect_M00_AXI [get_bd_intf_pins axi_ad9361/s_axi] [get_bd_intf_pins axi_cpu_interconnect/M00_AXI]
  connect_bd_intf_net -intf_net axi_cpu_interconnect_M01_AXI [get_bd_intf_pins axi_ad9361_adc_dma/s_axi] [get_bd_intf_pins axi_cpu_interconnect/M01_AXI]
  connect_bd_intf_net -intf_net axi_cpu_interconnect_M02_AXI [get_bd_intf_pins axi_ad9361_dac_dma/s_axi] [get_bd_intf_pins axi_cpu_interconnect/M02_AXI]
  connect_bd_intf_net -intf_net axi_cpu_interconnect_M03_AXI [get_bd_intf_pins axi_cpu_interconnect/M03_AXI] [get_bd_intf_pins system_management_wiz_0/S_AXI_LITE]
  connect_bd_intf_net -intf_net axi_hp1_interconnect_M00_AXI [get_bd_intf_pins axi_hp1_interconnect/M00_AXI] [get_bd_intf_pins sys_ps8/S_AXI_HP1_FPD]
  connect_bd_intf_net -intf_net axi_hp2_interconnect_M00_AXI [get_bd_intf_pins axi_hp2_interconnect/M00_AXI] [get_bd_intf_pins sys_ps8/S_AXI_HP2_FPD]

  # Create port connections
  connect_bd_net -net axi_ad9361_adc_data_i0 [get_bd_pins axi_ad9361/adc_data_i0] [get_bd_pins util_ad9361_adc_fifo/din_data_0]
  connect_bd_net -net axi_ad9361_adc_data_i1 [get_bd_pins axi_ad9361/adc_data_i1] [get_bd_pins util_ad9361_adc_fifo/din_data_2]
  connect_bd_net -net axi_ad9361_adc_data_q0 [get_bd_pins axi_ad9361/adc_data_q0] [get_bd_pins util_ad9361_adc_fifo/din_data_1]
  connect_bd_net -net axi_ad9361_adc_data_q1 [get_bd_pins axi_ad9361/adc_data_q1] [get_bd_pins util_ad9361_adc_fifo/din_data_3]
  connect_bd_net -net axi_ad9361_adc_dma_fifo_wr_overflow [get_bd_pins axi_ad9361_adc_dma/fifo_wr_overflow] [get_bd_pins util_ad9361_adc_fifo/dout_ovf]
  connect_bd_net -net axi_ad9361_adc_dma_irq [get_bd_pins axi_ad9361_adc_dma/irq] [get_bd_pins sys_concat_intc_1/In5]
  connect_bd_net -net axi_ad9361_adc_enable_i0 [get_bd_pins axi_ad9361/adc_enable_i0] [get_bd_pins util_ad9361_adc_fifo/din_enable_0]
  connect_bd_net -net axi_ad9361_adc_enable_i1 [get_bd_pins axi_ad9361/adc_enable_i1] [get_bd_pins util_ad9361_adc_fifo/din_enable_2]
  connect_bd_net -net axi_ad9361_adc_enable_q0 [get_bd_pins axi_ad9361/adc_enable_q0] [get_bd_pins util_ad9361_adc_fifo/din_enable_1]
  connect_bd_net -net axi_ad9361_adc_enable_q1 [get_bd_pins axi_ad9361/adc_enable_q1] [get_bd_pins util_ad9361_adc_fifo/din_enable_3]
  connect_bd_net -net axi_ad9361_adc_r1_mode [get_bd_pins axi_ad9361/adc_r1_mode] [get_bd_pins concat_logic/In0]
  connect_bd_net -net axi_ad9361_adc_valid_i0 [get_bd_pins axi_ad9361/adc_valid_i0] [get_bd_pins util_ad9361_adc_fifo/din_valid_0]
  connect_bd_net -net axi_ad9361_adc_valid_i1 [get_bd_pins axi_ad9361/adc_valid_i1] [get_bd_pins util_ad9361_adc_fifo/din_valid_2]
  connect_bd_net -net axi_ad9361_adc_valid_q0 [get_bd_pins axi_ad9361/adc_valid_q0] [get_bd_pins util_ad9361_adc_fifo/din_valid_1]
  connect_bd_net -net axi_ad9361_adc_valid_q1 [get_bd_pins axi_ad9361/adc_valid_q1] [get_bd_pins util_ad9361_adc_fifo/din_valid_3]
  connect_bd_net -net axi_ad9361_clk [get_bd_pins axi_ad9361/clk] [get_bd_pins axi_ad9361/l_clk] [get_bd_pins clkdiv/clk] [get_bd_pins dac_fifo/dout_clk] [get_bd_pins util_ad9361_adc_fifo/din_clk]
  connect_bd_net -net axi_ad9361_dac_dma_fifo_rd_dout [get_bd_pins axi_ad9361_dac_dma/fifo_rd_dout] [get_bd_pins util_ad9361_dac_upack/dac_data]
  connect_bd_net -net axi_ad9361_dac_dma_irq [get_bd_pins axi_ad9361_dac_dma/irq] [get_bd_pins sys_concat_intc_1/In4]
  connect_bd_net -net axi_ad9361_dac_enable_i0 [get_bd_pins axi_ad9361/dac_enable_i0] [get_bd_pins dac_fifo/dout_enable_0]
  connect_bd_net -net axi_ad9361_dac_enable_i1 [get_bd_pins axi_ad9361/dac_enable_i1] [get_bd_pins dac_fifo/dout_enable_2]
  connect_bd_net -net axi_ad9361_dac_enable_q0 [get_bd_pins axi_ad9361/dac_enable_q0] [get_bd_pins dac_fifo/dout_enable_1]
  connect_bd_net -net axi_ad9361_dac_enable_q1 [get_bd_pins axi_ad9361/dac_enable_q1] [get_bd_pins dac_fifo/dout_enable_3]
  connect_bd_net -net axi_ad9361_dac_r1_mode [get_bd_pins axi_ad9361/dac_r1_mode] [get_bd_pins concat_logic/In1]
  connect_bd_net -net axi_ad9361_dac_valid_i0 [get_bd_pins axi_ad9361/dac_valid_i0] [get_bd_pins dac_fifo/dout_valid_0]
  connect_bd_net -net axi_ad9361_dac_valid_i1 [get_bd_pins axi_ad9361/dac_valid_i1] [get_bd_pins dac_fifo/dout_valid_2]
  connect_bd_net -net axi_ad9361_dac_valid_q0 [get_bd_pins axi_ad9361/dac_valid_q0] [get_bd_pins dac_fifo/dout_valid_1]
  connect_bd_net -net axi_ad9361_dac_valid_q1 [get_bd_pins axi_ad9361/dac_valid_q1] [get_bd_pins dac_fifo/dout_valid_3]
  connect_bd_net -net axi_ad9361_enable [get_bd_ports enable] [get_bd_pins axi_ad9361/enable]
  connect_bd_net -net axi_ad9361_rst [get_bd_pins axi_ad9361/rst] [get_bd_pins dac_fifo/dout_rst] [get_bd_pins util_ad9361_adc_fifo/din_rst]
  connect_bd_net -net axi_ad9361_tdd_sync_cntr [get_bd_ports tdd_sync_t] [get_bd_pins axi_ad9361/tdd_sync_cntr] [get_bd_pins util_ad9361_tdd_sync/sync_mode]
  connect_bd_net -net axi_ad9361_tx_clk_out_n [get_bd_ports tx_clk_out_n] [get_bd_pins axi_ad9361/tx_clk_out_n]
  connect_bd_net -net axi_ad9361_tx_clk_out_p [get_bd_ports tx_clk_out_p] [get_bd_pins axi_ad9361/tx_clk_out_p]
  connect_bd_net -net axi_ad9361_tx_data_out_n [get_bd_ports tx_data_out_n] [get_bd_pins axi_ad9361/tx_data_out_n]
  connect_bd_net -net axi_ad9361_tx_data_out_p [get_bd_ports tx_data_out_p] [get_bd_pins axi_ad9361/tx_data_out_p]
  connect_bd_net -net axi_ad9361_tx_frame_out_n [get_bd_ports tx_frame_out_n] [get_bd_pins axi_ad9361/tx_frame_out_n]
  connect_bd_net -net axi_ad9361_tx_frame_out_p [get_bd_ports tx_frame_out_p] [get_bd_pins axi_ad9361/tx_frame_out_p]
  connect_bd_net -net axi_ad9361_txnrx [get_bd_ports txnrx] [get_bd_pins axi_ad9361/txnrx]
  connect_bd_net -net clkdiv_clk_out [get_bd_pins axi_ad9361_adc_dma/fifo_wr_clk] [get_bd_pins axi_ad9361_dac_dma/fifo_rd_clk] [get_bd_pins clkdiv/clk_out] [get_bd_pins clkdiv_reset/slowest_sync_clk] [get_bd_pins dac_fifo/din_clk] [get_bd_pins util_ad9361_adc_fifo/dout_clk] [get_bd_pins util_ad9361_adc_pack/adc_clk] [get_bd_pins util_ad9361_dac_upack/dac_clk]
  connect_bd_net -net clkdiv_reset_peripheral_aresetn [get_bd_pins clkdiv_reset/peripheral_aresetn] [get_bd_pins dac_fifo/din_rstn] [get_bd_pins util_ad9361_adc_fifo/dout_rstn]
  connect_bd_net -net clkdiv_reset_peripheral_reset [get_bd_pins clkdiv_reset/peripheral_reset] [get_bd_pins util_ad9361_adc_pack/adc_rst]
  connect_bd_net -net clkdiv_sel_logic_Res [get_bd_pins clkdiv/clk_sel] [get_bd_pins clkdiv_sel_logic/Res]
  connect_bd_net -net concat_logic_dout [get_bd_pins clkdiv_sel_logic/Op1] [get_bd_pins concat_logic/dout]
  connect_bd_net -net dac_fifo_din_enable_0 [get_bd_pins dac_fifo/din_enable_0] [get_bd_pins util_ad9361_dac_upack/dac_enable_0]
  connect_bd_net -net dac_fifo_din_enable_1 [get_bd_pins dac_fifo/din_enable_1] [get_bd_pins util_ad9361_dac_upack/dac_enable_1]
  connect_bd_net -net dac_fifo_din_enable_2 [get_bd_pins dac_fifo/din_enable_2] [get_bd_pins util_ad9361_dac_upack/dac_enable_2]
  connect_bd_net -net dac_fifo_din_enable_3 [get_bd_pins dac_fifo/din_enable_3] [get_bd_pins util_ad9361_dac_upack/dac_enable_3]
  connect_bd_net -net dac_fifo_din_valid_0 [get_bd_pins dac_fifo/din_valid_0] [get_bd_pins util_ad9361_dac_upack/dac_valid_0]
  connect_bd_net -net dac_fifo_din_valid_1 [get_bd_pins dac_fifo/din_valid_1] [get_bd_pins util_ad9361_dac_upack/dac_valid_1]
  connect_bd_net -net dac_fifo_din_valid_2 [get_bd_pins dac_fifo/din_valid_2] [get_bd_pins util_ad9361_dac_upack/dac_valid_2]
  connect_bd_net -net dac_fifo_din_valid_3 [get_bd_pins dac_fifo/din_valid_3] [get_bd_pins util_ad9361_dac_upack/dac_valid_3]
  connect_bd_net -net dac_fifo_dout_data_0 [get_bd_pins axi_ad9361/dac_data_i0] [get_bd_pins dac_fifo/dout_data_0]
  connect_bd_net -net dac_fifo_dout_data_1 [get_bd_pins axi_ad9361/dac_data_q0] [get_bd_pins dac_fifo/dout_data_1]
  connect_bd_net -net dac_fifo_dout_data_2 [get_bd_pins axi_ad9361/dac_data_i1] [get_bd_pins dac_fifo/dout_data_2]
  connect_bd_net -net dac_fifo_dout_data_3 [get_bd_pins axi_ad9361/dac_data_q1] [get_bd_pins dac_fifo/dout_data_3]
  connect_bd_net -net dac_fifo_dout_unf [get_bd_pins axi_ad9361/dac_dunf] [get_bd_pins dac_fifo/dout_unf]
  connect_bd_net -net gpio_i_1 [get_bd_ports gpio_i] [get_bd_pins sys_ps8/emio_gpio_i]
  connect_bd_net -net ps_intr_00_1 [get_bd_ports ps_intr_00] [get_bd_pins sys_concat_intc_0/In0]
  connect_bd_net -net ps_intr_01_1 [get_bd_ports ps_intr_01] [get_bd_pins sys_concat_intc_0/In1]
  connect_bd_net -net ps_intr_02_1 [get_bd_ports ps_intr_02] [get_bd_pins sys_concat_intc_0/In2]
  connect_bd_net -net ps_intr_03_1 [get_bd_ports ps_intr_03] [get_bd_pins sys_concat_intc_0/In3]
  connect_bd_net -net ps_intr_04_1 [get_bd_ports ps_intr_04] [get_bd_pins sys_concat_intc_0/In4]
  connect_bd_net -net ps_intr_05_1 [get_bd_ports ps_intr_05] [get_bd_pins sys_concat_intc_0/In5]
  connect_bd_net -net ps_intr_06_1 [get_bd_ports ps_intr_06] [get_bd_pins sys_concat_intc_0/In6]
  connect_bd_net -net ps_intr_07_1 [get_bd_ports ps_intr_07] [get_bd_pins sys_concat_intc_0/In7]
  connect_bd_net -net ps_intr_08_1 [get_bd_ports ps_intr_08] [get_bd_pins sys_concat_intc_1/In0]
  connect_bd_net -net ps_intr_09_1 [get_bd_ports ps_intr_09] [get_bd_pins sys_concat_intc_1/In1]
  connect_bd_net -net ps_intr_10_1 [get_bd_ports ps_intr_10] [get_bd_pins sys_concat_intc_1/In2]
  connect_bd_net -net ps_intr_11_1 [get_bd_ports ps_intr_11] [get_bd_pins sys_concat_intc_1/In3]
  connect_bd_net -net ps_intr_14_1 [get_bd_ports ps_intr_14] [get_bd_pins sys_concat_intc_1/In6]
  connect_bd_net -net ps_intr_15_1 [get_bd_ports ps_intr_15] [get_bd_pins sys_concat_intc_1/In7]
  connect_bd_net -net rx_clk_in_n_1 [get_bd_ports rx_clk_in_n] [get_bd_pins axi_ad9361/rx_clk_in_n]
  connect_bd_net -net rx_clk_in_p_1 [get_bd_ports rx_clk_in_p] [get_bd_pins axi_ad9361/rx_clk_in_p]
  connect_bd_net -net rx_data_in_n_1 [get_bd_ports rx_data_in_n] [get_bd_pins axi_ad9361/rx_data_in_n]
  connect_bd_net -net rx_data_in_p_1 [get_bd_ports rx_data_in_p] [get_bd_pins axi_ad9361/rx_data_in_p]
  connect_bd_net -net rx_frame_in_n_1 [get_bd_ports rx_frame_in_n] [get_bd_pins axi_ad9361/rx_frame_in_n]
  connect_bd_net -net rx_frame_in_p_1 [get_bd_ports rx_frame_in_p] [get_bd_pins axi_ad9361/rx_frame_in_p]
  connect_bd_net -net sys_200m_clk [get_bd_pins axi_ad9361/delay_clk] [get_bd_pins sys_ps8/pl_clk1]
  connect_bd_net -net sys_concat_intc_0_dout [get_bd_pins sys_concat_intc_0/dout] [get_bd_pins sys_ps8/pl_ps_irq0]
  connect_bd_net -net sys_concat_intc_1_dout [get_bd_pins sys_concat_intc_1/dout] [get_bd_pins sys_ps8/pl_ps_irq1]
  connect_bd_net -net sys_cpu_clk [get_bd_pins axi_ad9361/s_axi_aclk] [get_bd_pins axi_ad9361_adc_dma/m_dest_axi_aclk] [get_bd_pins axi_ad9361_adc_dma/s_axi_aclk] [get_bd_pins axi_ad9361_dac_dma/m_src_axi_aclk] [get_bd_pins axi_ad9361_dac_dma/s_axi_aclk] [get_bd_pins axi_cpu_interconnect/ACLK] [get_bd_pins axi_cpu_interconnect/M00_ACLK] [get_bd_pins axi_cpu_interconnect/M01_ACLK] [get_bd_pins axi_cpu_interconnect/M02_ACLK] [get_bd_pins axi_cpu_interconnect/M03_ACLK] [get_bd_pins axi_cpu_interconnect/S00_ACLK] [get_bd_pins axi_hp1_interconnect/ACLK] [get_bd_pins axi_hp1_interconnect/M00_ACLK] [get_bd_pins axi_hp1_interconnect/S00_ACLK] [get_bd_pins axi_hp2_interconnect/ACLK] [get_bd_pins axi_hp2_interconnect/M00_ACLK] [get_bd_pins axi_hp2_interconnect/S00_ACLK] [get_bd_pins sys_ps8/maxihpm0_lpd_aclk] [get_bd_pins sys_ps8/pl_clk0] [get_bd_pins sys_ps8/saxihp1_fpd_aclk] [get_bd_pins sys_ps8/saxihp2_fpd_aclk] [get_bd_pins sys_rstgen/slowest_sync_clk] [get_bd_pins system_management_wiz_0/s_axi_aclk] [get_bd_pins util_ad9361_tdd_sync/clk]
  connect_bd_net -net sys_cpu_reset [get_bd_pins sys_rstgen/peripheral_reset]
  connect_bd_net -net sys_cpu_resetn [get_bd_pins axi_ad9361/s_axi_aresetn] [get_bd_pins axi_ad9361_adc_dma/m_dest_axi_aresetn] [get_bd_pins axi_ad9361_adc_dma/s_axi_aresetn] [get_bd_pins axi_ad9361_dac_dma/m_src_axi_aresetn] [get_bd_pins axi_ad9361_dac_dma/s_axi_aresetn] [get_bd_pins axi_cpu_interconnect/ARESETN] [get_bd_pins axi_cpu_interconnect/M00_ARESETN] [get_bd_pins axi_cpu_interconnect/M01_ARESETN] [get_bd_pins axi_cpu_interconnect/M02_ARESETN] [get_bd_pins axi_cpu_interconnect/M03_ARESETN] [get_bd_pins axi_cpu_interconnect/S00_ARESETN] [get_bd_pins axi_hp1_interconnect/ARESETN] [get_bd_pins axi_hp1_interconnect/M00_ARESETN] [get_bd_pins axi_hp1_interconnect/S00_ARESETN] [get_bd_pins axi_hp2_interconnect/ARESETN] [get_bd_pins axi_hp2_interconnect/M00_ARESETN] [get_bd_pins axi_hp2_interconnect/S00_ARESETN] [get_bd_pins clkdiv_reset/ext_reset_in] [get_bd_pins sys_rstgen/peripheral_aresetn] [get_bd_pins system_management_wiz_0/s_axi_aresetn] [get_bd_pins util_ad9361_tdd_sync/rstn]
  connect_bd_net -net sys_ps8_emio_gpio_o [get_bd_ports gpio_o] [get_bd_pins sys_ps8/emio_gpio_o]
  connect_bd_net -net sys_ps8_pl_resetn0 [get_bd_pins sys_ps8/pl_resetn0] [get_bd_pins sys_rstgen/ext_reset_in]
  connect_bd_net -net tdd_sync_i_1 [get_bd_ports tdd_sync_i] [get_bd_pins util_ad9361_tdd_sync/sync_in]
  connect_bd_net -net up_enable_1 [get_bd_ports up_enable] [get_bd_pins axi_ad9361/up_enable]
  connect_bd_net -net up_txnrx_1 [get_bd_ports up_txnrx] [get_bd_pins axi_ad9361/up_txnrx]
  connect_bd_net -net util_ad9361_adc_fifo_din_ovf [get_bd_pins axi_ad9361/adc_dovf] [get_bd_pins util_ad9361_adc_fifo/din_ovf]
  connect_bd_net -net util_ad9361_adc_fifo_dout_data_0 [get_bd_pins util_ad9361_adc_fifo/dout_data_0] [get_bd_pins util_ad9361_adc_pack/adc_data_0]
  connect_bd_net -net util_ad9361_adc_fifo_dout_data_1 [get_bd_pins util_ad9361_adc_fifo/dout_data_1] [get_bd_pins util_ad9361_adc_pack/adc_data_1]
  connect_bd_net -net util_ad9361_adc_fifo_dout_data_2 [get_bd_pins util_ad9361_adc_fifo/dout_data_2] [get_bd_pins util_ad9361_adc_pack/adc_data_2]
  connect_bd_net -net util_ad9361_adc_fifo_dout_data_3 [get_bd_pins util_ad9361_adc_fifo/dout_data_3] [get_bd_pins util_ad9361_adc_pack/adc_data_3]
  connect_bd_net -net util_ad9361_adc_fifo_dout_enable_0 [get_bd_pins util_ad9361_adc_fifo/dout_enable_0] [get_bd_pins util_ad9361_adc_pack/adc_enable_0]
  connect_bd_net -net util_ad9361_adc_fifo_dout_enable_1 [get_bd_pins util_ad9361_adc_fifo/dout_enable_1] [get_bd_pins util_ad9361_adc_pack/adc_enable_1]
  connect_bd_net -net util_ad9361_adc_fifo_dout_enable_2 [get_bd_pins util_ad9361_adc_fifo/dout_enable_2] [get_bd_pins util_ad9361_adc_pack/adc_enable_2]
  connect_bd_net -net util_ad9361_adc_fifo_dout_enable_3 [get_bd_pins util_ad9361_adc_fifo/dout_enable_3] [get_bd_pins util_ad9361_adc_pack/adc_enable_3]
  connect_bd_net -net util_ad9361_adc_fifo_dout_valid_0 [get_bd_pins util_ad9361_adc_fifo/dout_valid_0] [get_bd_pins util_ad9361_adc_pack/adc_valid_0]
  connect_bd_net -net util_ad9361_adc_fifo_dout_valid_1 [get_bd_pins util_ad9361_adc_fifo/dout_valid_1] [get_bd_pins util_ad9361_adc_pack/adc_valid_1]
  connect_bd_net -net util_ad9361_adc_fifo_dout_valid_2 [get_bd_pins util_ad9361_adc_fifo/dout_valid_2] [get_bd_pins util_ad9361_adc_pack/adc_valid_2]
  connect_bd_net -net util_ad9361_adc_fifo_dout_valid_3 [get_bd_pins util_ad9361_adc_fifo/dout_valid_3] [get_bd_pins util_ad9361_adc_pack/adc_valid_3]
  connect_bd_net -net util_ad9361_adc_pack_adc_data [get_bd_pins axi_ad9361_adc_dma/fifo_wr_din] [get_bd_pins util_ad9361_adc_pack/adc_data]
  connect_bd_net -net util_ad9361_adc_pack_adc_sync [get_bd_pins axi_ad9361_adc_dma/fifo_wr_sync] [get_bd_pins util_ad9361_adc_pack/adc_sync]
  connect_bd_net -net util_ad9361_adc_pack_adc_valid [get_bd_pins axi_ad9361_adc_dma/fifo_wr_en] [get_bd_pins util_ad9361_adc_pack/adc_valid]
  connect_bd_net -net util_ad9361_dac_upack_dac_data_0 [get_bd_pins dac_fifo/din_data_0] [get_bd_pins util_ad9361_dac_upack/dac_data_0]
  connect_bd_net -net util_ad9361_dac_upack_dac_data_1 [get_bd_pins dac_fifo/din_data_1] [get_bd_pins util_ad9361_dac_upack/dac_data_1]
  connect_bd_net -net util_ad9361_dac_upack_dac_data_2 [get_bd_pins dac_fifo/din_data_2] [get_bd_pins util_ad9361_dac_upack/dac_data_2]
  connect_bd_net -net util_ad9361_dac_upack_dac_data_3 [get_bd_pins dac_fifo/din_data_3] [get_bd_pins util_ad9361_dac_upack/dac_data_3]
  connect_bd_net -net util_ad9361_dac_upack_dac_valid [get_bd_pins axi_ad9361_dac_dma/fifo_rd_en] [get_bd_pins util_ad9361_dac_upack/dac_valid]
  connect_bd_net -net util_ad9361_tdd_sync_sync_out [get_bd_ports tdd_sync_o] [get_bd_pins axi_ad9361/tdd_sync] [get_bd_pins util_ad9361_tdd_sync/sync_out]

  # Create address segments
  create_bd_addr_seg -range 0x80000000 -offset 0x00000000 [get_bd_addr_spaces axi_ad9361_adc_dma/m_dest_axi] [get_bd_addr_segs sys_ps8/SAXIGP3/HP1_DDR_LOW] SEG_sys_ps8_HP1_DDR_LOW
  create_bd_addr_seg -range 0x01000000 -offset 0xFF000000 [get_bd_addr_spaces axi_ad9361_adc_dma/m_dest_axi] [get_bd_addr_segs sys_ps8/SAXIGP3/HP1_LPS_OCM] SEG_sys_ps8_HP1_LPS_OCM
  create_bd_addr_seg -range 0x20000000 -offset 0xC0000000 [get_bd_addr_spaces axi_ad9361_adc_dma/m_dest_axi] [get_bd_addr_segs sys_ps8/SAXIGP3/HP1_QSPI] SEG_sys_ps8_HP1_QSPI
  create_bd_addr_seg -range 0x80000000 -offset 0x00000000 [get_bd_addr_spaces axi_ad9361_dac_dma/m_src_axi] [get_bd_addr_segs sys_ps8/SAXIGP4/HP2_DDR_LOW] SEG_sys_ps8_HP2_DDR_LOW
  create_bd_addr_seg -range 0x01000000 -offset 0xFF000000 [get_bd_addr_spaces axi_ad9361_dac_dma/m_src_axi] [get_bd_addr_segs sys_ps8/SAXIGP4/HP2_LPS_OCM] SEG_sys_ps8_HP2_LPS_OCM
  create_bd_addr_seg -range 0x20000000 -offset 0xC0000000 [get_bd_addr_spaces axi_ad9361_dac_dma/m_src_axi] [get_bd_addr_segs sys_ps8/SAXIGP4/HP2_QSPI] SEG_sys_ps8_HP2_QSPI
  create_bd_addr_seg -range 0x00010000 -offset 0x99020000 [get_bd_addr_spaces sys_ps8/Data] [get_bd_addr_segs axi_ad9361/s_axi/axi_lite] SEG_data_axi_ad9361
  create_bd_addr_seg -range 0x00010000 -offset 0x9C400000 [get_bd_addr_spaces sys_ps8/Data] [get_bd_addr_segs axi_ad9361_adc_dma/s_axi/axi_lite] SEG_data_axi_ad9361_adc_dma
  create_bd_addr_seg -range 0x00010000 -offset 0x9C420000 [get_bd_addr_spaces sys_ps8/Data] [get_bd_addr_segs axi_ad9361_dac_dma/s_axi/axi_lite] SEG_data_axi_ad9361_dac_dma
  create_bd_addr_seg -range 0x00010000 -offset 0x80000000 [get_bd_addr_spaces sys_ps8/Data] [get_bd_addr_segs system_management_wiz_0/S_AXI_LITE/Reg] SEG_system_management_wiz_0_Reg

  # Perform GUI Layout
  regenerate_bd_layout -layout_string {
   guistr: "# # String gsaved with Nlview 6.5.12  2016-01-29 bk=1.3547 VDI=39 GEI=35 GUI=JA:1.6
#  -string -flagsOSRD
preplace port enable -pg 1 -y 190 -defaultsOSRD
preplace port rx_clk_in_p -pg 1 -y 1420 -defaultsOSRD
preplace port ps_intr_10 -pg 1 -y 2070 -defaultsOSRD
preplace port ps_intr_11 -pg 1 -y 2090 -defaultsOSRD
preplace port tdd_sync_o -pg 1 -y 1040 -defaultsOSRD
preplace port up_txnrx -pg 1 -y 500 -defaultsOSRD
preplace port tx_frame_out_n -pg 1 -y 130 -defaultsOSRD
preplace port rx_frame_in_n -pg 1 -y 1460 -defaultsOSRD
preplace port ps_intr_00 -pg 1 -y 2250 -defaultsOSRD
preplace port up_enable -pg 1 -y 480 -defaultsOSRD
preplace port tx_clk_out_n -pg 1 -y 90 -defaultsOSRD
preplace port ps_intr_01 -pg 1 -y 2270 -defaultsOSRD
preplace port tx_frame_out_p -pg 1 -y 110 -defaultsOSRD
preplace port rx_frame_in_p -pg 1 -y 460 -defaultsOSRD
preplace port ps_intr_02 -pg 1 -y 2290 -defaultsOSRD
preplace port ps_intr_14 -pg 1 -y 2150 -defaultsOSRD
preplace port tx_clk_out_p -pg 1 -y 70 -defaultsOSRD
preplace port ps_intr_03 -pg 1 -y 2310 -defaultsOSRD
preplace port ps_intr_15 -pg 1 -y 2170 -defaultsOSRD
preplace port ps_intr_04 -pg 1 -y 2330 -defaultsOSRD
preplace port tdd_sync_t -pg 1 -y 250 -defaultsOSRD
preplace port ps_intr_05 -pg 1 -y 2350 -defaultsOSRD
preplace port ps_intr_06 -pg 1 -y 2370 -defaultsOSRD
preplace port tdd_sync_i -pg 1 -y 1020 -defaultsOSRD
preplace port ps_intr_07 -pg 1 -y 2390 -defaultsOSRD
preplace port ps_intr_08 -pg 1 -y 2030 -defaultsOSRD
preplace port rx_clk_in_n -pg 1 -y 1380 -defaultsOSRD
preplace port ps_intr_09 -pg 1 -y 2050 -defaultsOSRD
preplace port txnrx -pg 1 -y 210 -defaultsOSRD
preplace portBus rx_data_in_p -pg 1 -y 1440 -defaultsOSRD
preplace portBus gpio_o -pg 1 -y 1710 -defaultsOSRD
preplace portBus tx_data_out_n -pg 1 -y 170 -defaultsOSRD
preplace portBus gpio_i -pg 1 -y 2010 -defaultsOSRD
preplace portBus tx_data_out_p -pg 1 -y 150 -defaultsOSRD
preplace portBus rx_data_in_n -pg 1 -y 1400 -defaultsOSRD
preplace inst util_ad9361_tdd_sync -pg 1 -lvl 10 -y 1120 -defaultsOSRD
preplace inst axi_hp2_interconnect -pg 1 -lvl 8 -y 1630 -defaultsOSRD
preplace inst axi_ad9361 -pg 1 -lvl 11 -y 420 -defaultsOSRD
preplace inst sys_concat_intc_0 -pg 1 -lvl 8 -y 2320 -defaultsOSRD
preplace inst axi_ad9361_adc_dma -pg 1 -lvl 7 -y 1160 -defaultsOSRD
preplace inst sys_concat_intc_1 -pg 1 -lvl 8 -y 2100 -defaultsOSRD
preplace inst sys_rstgen -pg 1 -lvl 3 -y 1650 -defaultsOSRD
preplace inst clkdiv_reset -pg 1 -lvl 4 -y 1110 -defaultsOSRD
preplace inst axi_ad9361_dac_dma -pg 1 -lvl 7 -y 1880 -defaultsOSRD
preplace inst util_ad9361_adc_fifo -pg 1 -lvl 5 -y 260 -defaultsOSRD
preplace inst dac_fifo -pg 1 -lvl 5 -y 800 -defaultsOSRD
preplace inst concat_logic -pg 1 -lvl 1 -y 1340 -defaultsOSRD
preplace inst util_ad9361_dac_upack -pg 1 -lvl 6 -y 1200 -defaultsOSRD
preplace inst util_ad9361_adc_pack -pg 1 -lvl 6 -y 250 -defaultsOSRD
preplace inst axi_cpu_interconnect -pg 1 -lvl 10 -y 1440 -defaultsOSRD
preplace inst system_management_wiz_0 -pg 1 -lvl 10 -y 2160 -defaultsOSRD
preplace inst sys_ps8 -pg 1 -lvl 9 -y 1730 -defaultsOSRD
preplace inst clkdiv -pg 1 -lvl 3 -y 1260 -defaultsOSRD
preplace inst axi_hp1_interconnect -pg 1 -lvl 8 -y 1410 -defaultsOSRD
preplace inst clkdiv_sel_logic -pg 1 -lvl 2 -y 1340 -defaultsOSRD
preplace netloc S00_AXI_2 1 7 1 2860
preplace netloc axi_ad9361_dac_valid_i1 1 4 8 1400 1020 NJ 1000 NJ 1000 NJ 1000 NJ 1000 NJ 1000 NJ 1000 4680
preplace netloc clkdiv_sel_logic_Res 1 2 1 500
preplace netloc sys_concat_intc_0_dout 1 8 1 3190
preplace netloc S00_AXI_3 1 7 1 2860
preplace netloc axi_ad9361_dac_enable_i0 1 4 8 1390 1010 NJ 970 NJ 970 NJ 970 NJ 970 NJ 970 NJ 970 4700
preplace netloc axi_ad9361_dac_enable_i1 1 4 8 1370 1350 NJ 1350 NJ 1320 NJ 1240 NJ 1240 NJ 1240 NJ 1240 4720
preplace netloc util_ad9361_adc_fifo_dout_enable_0 1 5 1 N
preplace netloc ps_intr_06_1 1 0 8 NJ 2370 NJ 2370 NJ 2370 NJ 2370 NJ 2370 NJ 2370 NJ 2370 NJ
preplace netloc util_ad9361_adc_fifo_dout_enable_1 1 5 1 N
preplace netloc axi_ad9361_adc_data_q0 1 4 8 1280 -110 NJ -110 NJ -110 NJ -110 NJ -110 NJ -110 NJ -110 4760
preplace netloc up_enable_1 1 0 11 NJ 490 NJ 490 NJ 490 NJ 490 NJ 590 NJ 590 NJ 590 NJ 590 NJ 590 NJ 590 NJ
preplace netloc util_ad9361_adc_fifo_dout_enable_2 1 5 1 N
preplace netloc axi_ad9361_adc_data_q1 1 4 8 1260 -100 NJ -100 NJ -100 NJ -100 NJ -100 NJ -100 NJ -100 4780
preplace netloc ps_intr_02_1 1 0 8 NJ 2290 NJ 2290 NJ 2290 NJ 2290 NJ 2290 NJ 2290 NJ 2290 NJ
preplace netloc util_ad9361_adc_fifo_dout_valid_0 1 5 1 N
preplace netloc util_ad9361_adc_fifo_dout_enable_3 1 5 1 N
preplace netloc util_ad9361_adc_fifo_dout_valid_1 1 5 1 N
preplace netloc ps_intr_01_1 1 0 8 NJ 2270 NJ 2270 NJ 2270 NJ 2270 NJ 2270 NJ 2270 NJ 2270 NJ
preplace netloc sys_ps8_emio_gpio_o 1 9 3 NJ 1710 NJ 1710 NJ
preplace netloc axi_ad9361_adc_valid_q0 1 4 8 1390 -20 NJ -20 NJ -20 NJ -20 NJ -20 NJ -20 NJ -20 4680
preplace netloc axi_ad9361_dac_valid_q0 1 4 8 1360 1340 NJ 1380 NJ 1380 NJ 1260 NJ 1260 NJ 1200 NJ 1200 4710
preplace netloc axi_ad9361_adc_dma_fifo_wr_overflow 1 4 3 1310 1030 NJ 1030 NJ
preplace netloc util_ad9361_adc_fifo_dout_valid_2 1 5 1 N
preplace netloc axi_ad9361_tx_frame_out_n 1 11 1 NJ
preplace netloc ps_intr_09_1 1 0 8 NJ 2050 NJ 2050 NJ 2050 NJ 2050 NJ 2050 NJ 2050 NJ 2050 NJ
preplace netloc axi_ad9361_adc_valid_q1 1 4 8 1350 -10 NJ -10 NJ -10 NJ -10 NJ -10 NJ -10 NJ -10 4720
preplace netloc axi_ad9361_dac_valid_q1 1 4 8 1350 1400 NJ 1400 NJ 1400 NJ 1040 NJ 1040 NJ 1040 NJ 1040 4660
preplace netloc clkdiv_clk_out 1 3 4 870 660 1280 550 1920 1010 2350
preplace netloc util_ad9361_adc_fifo_dout_valid_3 1 5 1 N
preplace netloc sys_ps8_pl_resetn0 1 2 8 490 1740 NJ 1740 NJ 1740 NJ 1740 NJ 1740 NJ 1860 NJ 1860 3810
preplace netloc axi_ad9361_tx_frame_out_p 1 11 1 NJ
preplace netloc axi_ad9361_dac_dma_fifo_rd_dout 1 5 2 1920 1860 N
preplace netloc util_ad9361_dac_upack_dac_data_0 1 4 3 1320 1040 NJ 1040 2340
preplace netloc util_ad9361_dac_upack_dac_valid 1 6 1 2370
preplace netloc rx_frame_in_n_1 1 0 11 NJ 500 NJ 500 NJ 500 NJ 500 NJ 500 NJ 500 NJ 260 NJ 260 NJ 260 NJ 260 NJ
preplace netloc ps_intr_05_1 1 0 8 NJ 2350 NJ 2350 NJ 2350 NJ 2350 NJ 2350 NJ 2350 NJ 2350 NJ
preplace netloc util_ad9361_tdd_sync_sync_out 1 10 2 4230 1050 NJ
preplace netloc util_ad9361_dac_upack_dac_data_1 1 4 3 1330 1050 NJ 1050 2330
preplace netloc dac_fifo_dout_unf 1 5 6 1850 530 NJ 530 NJ 530 NJ 530 NJ 530 NJ
preplace netloc util_ad9361_dac_upack_dac_data_2 1 4 3 1270 1430 NJ 1430 2340
preplace netloc clkdiv_reset_peripheral_reset 1 4 2 NJ 50 1920
preplace netloc util_ad9361_adc_fifo_din_ovf 1 5 6 NJ 70 NJ 70 NJ 70 NJ 70 NJ 70 4240
preplace netloc ps_intr_07_1 1 0 8 NJ 2390 NJ 2390 NJ 2390 NJ 2390 NJ 2390 NJ 2390 NJ 2390 NJ
preplace netloc util_ad9361_dac_upack_dac_data_3 1 4 3 1280 1440 NJ 1440 2330
preplace netloc ps_intr_00_1 1 0 8 NJ 2250 NJ 2250 NJ 2250 NJ 2250 NJ 2250 NJ 2250 NJ 2250 NJ
preplace netloc axi_cpu_interconnect_M01_AXI 1 6 5 2430 1310 NJ 1290 NJ 1290 NJ 1260 4190
preplace netloc rx_clk_in_p_1 1 0 11 NJ 460 NJ 460 NJ 460 NJ 460 NJ 490 NJ 490 NJ 200 NJ 200 NJ 200 NJ 200 NJ
preplace netloc dac_fifo_dout_data_0 1 5 6 1760 430 NJ 430 NJ 430 NJ 430 NJ 430 NJ
preplace netloc dac_fifo_din_valid_0 1 5 1 1860
preplace netloc tdd_sync_i_1 1 0 10 NJ 1020 NJ 1020 NJ 1020 NJ 1020 NJ 1420 NJ 1420 NJ 1420 NJ 1270 NJ 1270 NJ
preplace netloc dac_fifo_dout_data_1 1 5 6 1790 460 NJ 460 NJ 460 NJ 460 NJ 460 NJ
preplace netloc dac_fifo_din_valid_1 1 5 1 1820
preplace netloc rx_clk_in_n_1 1 0 11 NJ 450 NJ 450 NJ 450 NJ 450 NJ 480 NJ 450 NJ 220 NJ 220 NJ 220 NJ 220 NJ
preplace netloc dac_fifo_dout_data_2 1 5 6 1810 480 NJ 480 NJ 480 NJ 480 NJ 480 NJ
preplace netloc dac_fifo_din_valid_2 1 5 1 1780
preplace netloc dac_fifo_dout_data_3 1 5 6 1830 520 NJ 500 NJ 500 NJ 500 NJ 500 NJ
preplace netloc dac_fifo_din_valid_3 1 5 1 1750
preplace netloc axi_ad9361_dac_r1_mode 1 0 12 30 1270 NJ 1270 NJ 1330 NJ 1330 NJ 1330 NJ 830 NJ 830 NJ 830 NJ 830 NJ 830 NJ 830 4650
preplace netloc axi_ad9361_rst 1 4 8 1340 0 NJ 0 NJ 0 NJ 0 NJ 0 NJ 0 NJ 0 4660
preplace netloc ps_intr_08_1 1 0 8 NJ 2030 NJ 2030 NJ 2030 NJ 2030 NJ 2030 NJ 2030 NJ 2030 NJ
preplace netloc axi_ad9361_tx_clk_out_n 1 11 1 NJ
preplace netloc axi_ad9361_adc_r1_mode 1 0 12 40 1280 NJ 1290 NJ 1320 NJ 1320 NJ 1320 NJ 820 NJ 820 NJ 820 NJ 820 NJ 820 NJ 820 4690
preplace netloc axi_ad9361_tx_clk_out_p 1 11 1 NJ
preplace netloc axi_ad9361_adc_enable_i0 1 4 8 1380 -90 NJ -90 NJ -90 NJ -90 NJ -90 NJ -90 NJ -90 4690
preplace netloc axi_ad9361_adc_dma_irq 1 7 1 2820
preplace netloc axi_ad9361_adc_enable_i1 1 4 8 1310 -80 NJ -80 NJ -80 NJ -80 NJ -80 NJ -80 NJ -80 4730
preplace netloc axi_cpu_interconnect_M03_AXI 1 9 2 3890 1630 4190
preplace netloc gpio_i_1 1 0 10 NJ 2010 NJ 2010 NJ 2010 NJ 2010 NJ 2010 NJ 2010 NJ 2010 NJ 1970 NJ 1970 3820
preplace netloc ps_intr_04_1 1 0 8 NJ 2330 NJ 2330 NJ 2330 NJ 2330 NJ 2330 NJ 2330 NJ 2330 NJ
preplace netloc ps_intr_10_1 1 0 8 NJ 2070 NJ 2070 NJ 2070 NJ 2070 NJ 2070 NJ 2070 NJ 2070 NJ
preplace netloc axi_ad9361_tdd_sync_cntr 1 9 3 3880 10 NJ 10 4790
preplace netloc util_ad9361_adc_pack_adc_sync 1 6 1 2390
preplace netloc axi_ad9361_txnrx 1 11 1 NJ
preplace netloc axi_cpu_interconnect_M00_AXI 1 10 1 4220
preplace netloc axi_ad9361_enable 1 11 1 NJ
preplace netloc dac_fifo_din_enable_0 1 5 1 1870
preplace netloc up_txnrx_1 1 0 11 NJ 510 NJ 510 NJ 510 NJ 510 NJ 540 NJ 540 NJ 540 NJ 540 NJ 540 NJ 540 NJ
preplace netloc axi_hp1_interconnect_M00_AXI 1 8 1 3210
preplace netloc dac_fifo_din_enable_1 1 5 1 1840
preplace netloc ps_intr_03_1 1 0 8 NJ 2310 NJ 2310 NJ 2310 NJ 2310 NJ 2310 NJ 2310 NJ 2310 NJ
preplace netloc dac_fifo_din_enable_2 1 5 1 1800
preplace netloc sys_200m_clk 1 9 2 NJ 360 N
preplace netloc ps_intr_11_1 1 0 8 NJ 2090 NJ 2090 NJ 2090 NJ 2090 NJ 2090 NJ 2090 NJ 2090 NJ
preplace netloc dac_fifo_din_enable_3 1 5 1 1770
preplace netloc axi_ad9361_adc_enable_q0 1 4 8 1370 -70 NJ -70 NJ -70 NJ -70 NJ -70 NJ -70 NJ -70 4700
preplace netloc ps_intr_15_1 1 0 8 NJ 2170 NJ 2170 NJ 2170 NJ 2170 NJ 2170 NJ 2170 NJ 2170 NJ
preplace netloc axi_ad9361_dac_enable_q0 1 4 8 1340 1360 NJ 1360 NJ 1330 NJ 1250 NJ 1250 NJ 1250 NJ 1250 4740
preplace netloc axi_ad9361_adc_enable_q1 1 4 8 1300 -60 NJ -60 NJ -60 NJ -60 NJ -60 NJ -60 NJ -60 4740
preplace netloc axi_ad9361_dac_enable_q1 1 4 8 1380 1370 NJ 1370 NJ 1370 NJ 1030 NJ 1030 NJ 1030 NJ 1030 4670
preplace netloc util_ad9361_adc_pack_adc_data 1 6 1 2410
preplace netloc rx_frame_in_p_1 1 0 11 NJ 480 NJ 480 NJ 480 NJ 480 NJ 510 NJ 510 NJ 240 NJ 240 NJ 240 NJ 240 NJ
preplace netloc axi_ad9361_tx_data_out_n 1 11 1 NJ
preplace netloc axi_ad9361_clk 1 2 10 490 800 NJ 800 1250 30 NJ 30 NJ 30 NJ 30 NJ 30 NJ 30 4250 20 4650
preplace netloc axi_cpu_interconnect_M02_AXI 1 6 5 2430 1750 NJ 1750 NJ 1600 NJ 1620 4230
preplace netloc axi_ad9361_tx_data_out_p 1 11 1 NJ
preplace netloc rx_data_in_n_1 1 0 11 NJ 470 NJ 470 NJ 470 NJ 470 NJ 470 NJ 470 NJ 300 NJ 300 NJ 300 NJ 300 NJ
preplace netloc concat_logic_dout 1 1 1 NJ
preplace netloc util_ad9361_adc_pack_adc_valid 1 6 1 2420
preplace netloc ps_intr_14_1 1 0 8 NJ 2150 NJ 2150 NJ 2150 NJ 2150 NJ 2150 NJ 2150 NJ 2150 NJ
preplace netloc sys_cpu_reset 1 3 1 N
preplace netloc util_ad9361_adc_fifo_dout_data_0 1 5 1 N
preplace netloc util_ad9361_adc_fifo_dout_data_1 1 5 1 N
preplace netloc clkdiv_reset_peripheral_aresetn 1 4 1 1260
preplace netloc util_ad9361_adc_fifo_dout_data_2 1 5 1 N
preplace netloc axi_ad9361_adc_data_i0 1 4 8 1290 -140 NJ -140 NJ -140 NJ -140 NJ -140 NJ -140 NJ -140 4750
preplace netloc sys_cpu_clk 1 2 9 490 1560 NJ 1560 NJ 1560 NJ 1560 2410 1410 2880 1760 3180 1580 3860 560 N
preplace netloc sys_concat_intc_1_dout 1 8 1 3210
preplace netloc util_ad9361_adc_fifo_dout_data_3 1 5 1 N
preplace netloc sys_cpu_resetn 1 3 8 880 1410 NJ 1410 NJ 1410 2380 1340 2870 1280 NJ 1280 3870 580 N
preplace netloc axi_ad9361_adc_data_i1 1 4 8 1270 -120 NJ -120 NJ -120 NJ -120 NJ -120 NJ -120 NJ -120 4770
preplace netloc axi_hp2_interconnect_M00_AXI 1 8 1 3200
preplace netloc axi_ad9361_adc_valid_i0 1 4 8 1400 -50 NJ -50 NJ -50 NJ -50 NJ -50 NJ -50 NJ -50 4670
preplace netloc rx_data_in_p_1 1 0 11 NJ 560 NJ 560 NJ 560 NJ 560 NJ 560 NJ 560 NJ 280 NJ 280 NJ 280 NJ 280 NJ
preplace netloc S00_AXI_1 1 9 1 3880
preplace netloc axi_ad9361_dac_dma_irq 1 7 1 2780
preplace netloc axi_ad9361_dac_valid_i0 1 4 8 1300 1390 NJ 1390 NJ 1390 NJ 1210 NJ 1210 NJ 1210 NJ 1210 4730
preplace netloc axi_ad9361_adc_valid_i1 1 4 8 1360 -40 NJ -40 NJ -40 NJ -40 NJ -40 NJ -40 NJ -40 4710
levelinfo -pg 1 -60 140 380 700 1050 1610 2200 2620 3030 3510 4040 4480 4890 -top -230 -bot 3130
",
}

  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


